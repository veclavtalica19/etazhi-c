cmake_minimum_required(VERSION 3.19)

include(FetchContent)

# todo: Strip .eh_frame and .eh_frame_hdr sections.

set(IT_BACKEND "SDL2" CACHE STRING "Backend to be based on")
option(IT_DEBUGSYM "Produce debug symbols" ON)
option(IT_SANITIZE "Use sanitizers" OFF)
option(IT_STATIC "Try locally building dependencies, instead of relying on shared linking" OFF)
option(IT_LTO "Use link time optimizations" OFF)
option(IT_DEFAULT_TO_STATIC "Defines ilotawa interface accordingly" OFF)
option(IT_FORCE_32_BIT "Force compilation of 32bit executable on 64bit platforms" OFF)

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Debug")
endif()

function(it_download_file file url hash)
    FetchContent_Declare(${file}
        URL ${url}
        URL_HASH SHA256=${hash}
        DOWNLOAD_NO_EXTRACT false
        TLS_VERIFY true)
    FetchContent_MakeAvailable(${file})
endfunction()

function(it_populate_target_debug_symbols target)
    target_compile_options(${target} PRIVATE -g3 -ggdb)
endfunction()

function(it_populate_target_options target)
    # todo: GCC/MSVC variants
    # todo: Use -ffast-math on lofi builds.
    # http://ptspts.blogspot.com/2013/12/how-to-make-smaller-c-and-c-binaries.html
    target_compile_options(${target} PRIVATE
        -pipe -fvisibility=hidden -fno-ident
        -fmerge-all-constants -fno-math-errno -fno-stack-protector -fno-asynchronous-unwind-tables -Qn
        -Weverything -Wno-extra-semi-stmt -Wno-c++98-compat -Wno-padded -Wno-declaration-after-statement
        -Wno-unreachable-code-return -Wno-switch-enum -Wno-covered-switch-default -Wno-bad-function-cast
        -Wno-unused-macros -Wno-assume -Wno-ignored-attributes
        $<$<CONFIG:DEBUG>:-O0;-ftrapv>
        $<$<CONFIG:RELEASE>:-Oz;-msse2;-march=native;-ffunction-sections;-fdata-sections>
        $<$<BOOL:${IT_LTO}>:-flto;-fuse-linker-plugin>
        $<$<BOOL:${IT_FORCE_32_BIT}>:-m32>)
    target_link_options(${target} PRIVATE
        $<$<CONFIG:RELEASE>:-Wl,--gc-sections> -Wl,-z,norelro -Wl,--hash-style=gnu -Wl,--build-id=none)
    target_compile_definitions(${target} PRIVATE $<$<CONFIG:RELEASE>:NDEBUG> $<$<CONFIG:DEBUG>:_FORTIFY_SOURCE=2>)

    if(${IT_DEBUGSYM})
        it_populate_target_debug_symbols(${target})
    endif()

    if(${IT_SANITIZE})
        message(STATUS "Sanitizers are on for " $<TARGET_NAME:${target}>)
        # todo: Figure out how to link to UCRT reliably
        # note: On windows address sanitizer only works under UCRT
        if(CMAKE_SYSTEM_NAME MATCHES "Windows")
            target_link_libraries(${target} PRIVATE ucrt)
        endif()
        target_compile_options(${target} PRIVATE -fsanitize=address -fsanitize=undefined)
        target_link_options(${target} PRIVATE -fsanitize=address -fsanitize=undefined)
    endif()
endfunction()

function(it_resolve_ilotawa_shared_library target)
    add_custom_command(TARGET ${target} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
            $<TARGET_FILE:ilotawa_shared>
            $<TARGET_FILE_DIR:${target}>)
    # todo: Move SDL2 shared object to folder of executable, at least on windows.
endfunction()

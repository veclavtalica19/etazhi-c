cmake_minimum_required(VERSION 3.19)

project(kalama_project LANGUAGES C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS OFF)

set(IT_ILOTAWA_PATH ${CMAKE_SOURCE_DIR} CACHE STRING "ilotawa base directory")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${IT_ILOTAWA_PATH})
include(Shared)

file(GLOB_RECURSE IT_SRCS ${CMAKE_CURRENT_SOURCE_DIR}/src/*.c)
add_library(kalama STATIC ${IT_SRCS} ${IT_BACKEND_SRCS})
it_populate_target_options(kalama)
target_link_libraries(kalama PRIVATE tomolipu)

target_include_directories(kalama PRIVATE $<PATH:GET_PARENT_PATH, ${CMAKE_CURRENT_SOURCE_DIR}>)

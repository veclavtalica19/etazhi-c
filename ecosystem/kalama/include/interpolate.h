#pragma once

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/fixed.h>

enum it_interpolation { it_interpolation_nearest, it_interpolation_linear, it_interpolation_count };

IT_API IT_PURE it_fixed it_interpolate_linearly(it_fixed low, it_fixed high, it_fixed step);

// todo: Would buffer of steps make more sense?
IT_API void it_interpolate_linearly_fill_buffer(it_buffer *restrict result,
    it_buffer const *restrict lows,
    it_buffer const *restrict highs,
    it_fixed step);

IT_API it_fixed it_interpolate_genericly(
    enum it_interpolation method, it_fixed low, it_fixed high, it_fixed step);

IT_API it_fixed it_interpolate_genericly_fill_buffer(enum it_interpolation method,
    it_buffer *restrict result,
    it_buffer const *restrict lows,
    it_buffer const *restrict highs,
    it_fixed step);

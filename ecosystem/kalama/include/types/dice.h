#pragma once

#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/map.h>

// @brief Xorshift32 RNG.
//
typedef struct it_dice {
    struct {
        uint32_t state;

    } private;
} it_dice;

IT_API IT_PURE it_dice it_dice_init(uint32_t seed);

// @brief Roll a dice with desired result being less than 'sides'.
//        If 0 sides passed - any uint32_t value is considered to be valid.
//
IT_API uint32_t it_dice_roll(it_dice *dice, uint32_t sides);

// @brief Roll a dice, output will be in range [0, 1]
//
IT_API it_fixed it_dice_clamp(it_dice *dice);

// @brief Fill dimensional map with values from subsequent calls to 'it_dice_roll',
//        filling rows first, then columns.
//
IT_API void it_dice_roll_fill_map(it_dice dice, it_map *result, uint32_t sides);

// @brief Fill dimensional map with values from subsequent calls to 'it_dice_it_dice_clamp',
//        filling rows first, then columns.
//
IT_API void it_dice_clamp_fill_map(it_dice dice, it_map *result);

#pragma once

#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/fixed.h>
#include <tomolipu/include/types/map.h>

#include <kalama/include/interpolate.h>
#include <kalama/include/types/dice.h>

typedef struct it_value_noise {
    struct {
        it_map permutations;
        enum it_interpolation interpolation;
    } private;
} it_value_noise;

IT_API IT_PURE it_value_noise it_value_noise_from_permutations(
    it_map permutations, enum it_interpolation method);

IT_API IT_PURE it_fixed it_value_noise_at(
    it_value_noise const *value_noise, it_vector2_uint16_t position, it_fixed frequency);

IT_API void it_value_noise_fill_map(
    it_value_noise const *value_noise, it_map *result, it_fixed frequency);

IT_API void it_value_noise_free(it_value_noise *result);

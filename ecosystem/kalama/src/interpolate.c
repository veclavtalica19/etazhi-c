#define IT_FIXED_INLINED
#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/fixed.h>

#include <kalama/include/interpolate.h>

it_fixed it_interpolate_linearly(it_fixed const low, it_fixed const high, it_fixed const step) {
    it_test(it_fixed_get_whole(step) == 0);

    it_fixed const step_m = it_fixed_sub(it_fixed_from_whole(1), step);
    return it_fixed_add(it_fixed_mul(low, step_m), it_fixed_mul(high, step));
}

void it_interpolate_linearly_fill_buffer(it_buffer *restrict const result,
    it_buffer const *restrict const lows,
    it_buffer const *restrict const highs,
    it_fixed const step) {
    // todo: whole == 1 and fraction == 0 case is not covered here.
    it_test(it_fixed_get_whole(step) == 0);
    it_test(it_buffer_is_initialized(result) and it_buffer_is_initialized(lows)
        and it_buffer_is_initialized(highs));
    it_test(it_buffer_get_length(result) == it_buffer_get_length(lows)
        and it_buffer_get_length(lows) == it_buffer_get_length(highs));

    it_buffer_iterator result_it = it_buffer_iterator_init(result, it_flag_access_writable);
    it_buffer_iterator lows_it = it_buffer_iterator_init(lows, it_flag_access_readable);
    it_buffer_iterator highs_it = it_buffer_iterator_init(highs, it_flag_access_readable);

    while (it_buffer_iterator_pump(&result_it)) {
        (void)it_buffer_iterator_pump(&lows_it);
        (void)it_buffer_iterator_pump(&highs_it);

        *it_align_cast(result_it.current, it_fixed *)
            = it_interpolate_linearly(*it_align_cast(lows_it.current, it_fixed *),
                *it_align_cast(highs_it.current, it_fixed *), step);
    }
}

it_fixed it_interpolate_genericly(enum it_interpolation const method,
    it_fixed const low,
    it_fixed const high,
    it_fixed const step) {
    it_test(method >= 0 and method < it_interpolation_count);

    switch (method) {
    case it_interpolation_nearest:
        return it_fixed_get_fraction(step) <= (uint16_t)(it_fixed_denominator / 2) ? low : high;
    case it_interpolation_linear:
        return it_interpolate_linearly(low, high, step);
    default:
        it_ensure(false);
    }

    it_unreachable();
    return (it_fixed) { 0 };
}

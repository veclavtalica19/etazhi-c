#include <stddef.h>
#include <stdint.h>

#include <tomolipu/include/definitions.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/fixed.h>

#include <kalama/include/types/dice.h>

it_dice it_dice_init(uint32_t const seed) {
    it_test(seed != 0);

    return (it_dice) { .private = { .state = seed } };
}

static uint32_t roll(it_dice *const dice) {
    it_test(dice != NULL);
    it_test(dice->private.state != 0);

    uint32_t state = dice->private.state;
    state ^= state << 13;
    state ^= state >> 17;
    state ^= state << 5;

    return dice->private.state = state;
}

// https://en.wikipedia.org/wiki/Xorshift
// https://stackoverflow.com/questions/10984974/why-do-people-say-there-is-modulo-bias-when-using-a-random-number-generator
uint32_t it_dice_roll(it_dice *const dice, uint32_t const sides) {
    it_test(dice != NULL);

    roll(dice);

    if (sides != 0) {
        uint32_t const compare = UINT32_MAX - (((UINT32_MAX % sides) + 1) % sides);
        while (dice->private.state > compare)
            roll(dice);
        return dice->private.state % sides;
    } else
        return dice->private.state;
}

it_fixed it_dice_clamp(it_dice *const dice) {
    it_test(dice != NULL);

    return it_fixed_const_init(
        it_sign_plus, 0, (uint16_t)it_dice_roll(dice, it_fixed_denominator + 1));
}

void it_dice_roll_fill_map(it_dice const dice, it_map *const result, uint32_t const sides) {
    it_test(result != NULL);

    it_dice m_dice = dice;
    for (it_map_iterator it = it_map_iterator_init(result); it_map_iterator_pump(&it);) {
        uint32_t const roll = it_dice_roll(&m_dice, sides);
        it_map_set(result, it.current, &roll);
    }
}

void it_dice_clamp_fill_map(it_dice const dice, it_map *const result) {
    it_test(result != NULL);

    it_dice m_dice = dice;
    for (it_map_iterator it = it_map_iterator_init(result); it_map_iterator_pump(&it);) {
        it_fixed const roll = it_dice_clamp(&m_dice);
        it_map_set(result, it.current, &roll);
    }
}

#define IT_FIXED_INLINED
#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/integer_ops.h>
#include <tomolipu/include/memory.h>
#include <tomolipu/include/types/fixed.h>
#include <tomolipu/include/types/map.h>
#include <tomolipu/include/types/vector2.h>

#include <kalama/include/interpolate.h>
#include <kalama/include/types/dice.h>
#include <kalama/include/types/value_noise.h>

// https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/procedural-patterns-noise-part-1/creating-simple-1D-noise.html

it_value_noise it_value_noise_from_permutations(
    it_map permutations, enum it_interpolation const method) {
    it_test(method >= 0 and method < it_interpolation_count);

    return (
        it_value_noise) { .private = { .permutations = permutations, .interpolation = method } };
}

it_fixed it_value_noise_at(it_value_noise const *const value_noise,
    it_vector2_uint16_t const position,
    it_fixed const frequency) {
    it_test(value_noise != NULL);

    it_vector2_uint16_t const dims = it_map_get_dimensions(&value_noise->private.permutations);
    it_fixed const x
        = it_fixed_mul(it_fixed_from_whole(it_int16_t_from_uint16_t(position.x)), frequency);
    it_fixed const y
        = it_fixed_mul(it_fixed_from_whole(it_int16_t_from_uint16_t(position.y)), frequency);

    it_fixed const a = it_interpolate_genericly(value_noise->private.interpolation,
        *it_align_cast(it_map_get(&value_noise->private.permutations,
                           (it_vector2_uint16_t) {
                               .x = (uint16_t)((uint32_t)it_fixed_get_whole(x) % dims.width),
                               .y = (uint16_t)((uint32_t)it_fixed_get_whole(y) % dims.height) }),
            it_fixed *),
        *it_align_cast(it_map_get(&value_noise->private.permutations,
                           (it_vector2_uint16_t) {
                               .x = (uint16_t)(((uint32_t)it_fixed_get_whole(x) + 1) % dims.width),
                               .y = (uint16_t)((uint32_t)it_fixed_get_whole(y) % dims.height) }),
            it_fixed *),
        it_fixed_trim(x));

    it_fixed const b = it_interpolate_genericly(value_noise->private.interpolation,
        *it_align_cast(
            it_map_get(&value_noise->private.permutations,
                (it_vector2_uint16_t) {
                    .x = (uint16_t)((uint32_t)it_fixed_get_whole(x) % dims.width),
                    .y = (uint16_t)(((uint32_t)it_fixed_get_whole(y) + 1) % dims.height) }),
            it_fixed *),
        *it_align_cast(
            it_map_get(&value_noise->private.permutations,
                (it_vector2_uint16_t) {
                    .x = (uint16_t)(((uint32_t)it_fixed_get_whole(x) + 1) % dims.width),
                    .y = (uint16_t)(((uint32_t)it_fixed_get_whole(y) + 1) % dims.height) }),
            it_fixed *),
        it_fixed_trim(x));

    return it_interpolate_genericly(value_noise->private.interpolation, a, b, it_fixed_trim(y));
}

void it_value_noise_fill_map(
    it_value_noise const *const value_noise, it_map *const result, it_fixed const frequency) {
    it_test(value_noise != NULL);
    it_test(result != NULL);

    for (it_map_iterator it = it_map_iterator_init(result); it_map_iterator_pump(&it);) {
        it_fixed const value = it_value_noise_at(value_noise, it.current, frequency);
        it_map_set(result, it.current, &value);
    }
}

void it_value_noise_free(it_value_noise *result) {
    it_test(result != NULL);

    it_map_free(&result->private.permutations);
    result->private.interpolation = 0;
}

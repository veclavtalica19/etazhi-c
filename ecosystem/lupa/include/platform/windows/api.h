#pragma once

#if !defined(_WIN64) && !defined(_X86_)
#define _X86_
#elif defined(_WIN64) && !defined(_AMD64_)
#define _AMD64_
#endif
#include <windef.h>
#include <Winbase.h>

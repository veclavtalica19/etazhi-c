#pragma once

#if defined(_WIN32)
#define IT_PLATFORM_WINDOWS
#elif defined(__linux)
#define IT_PLATFORM_LINUX
#define IT_PLATFORM_UNIX
#define IT_PLATFORM_POSIX
#else
#warning Unknown target.
#endif

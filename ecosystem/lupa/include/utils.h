#pragma once

#include <stddef.h>

#include <tomolipu/include/compiler.h>

// @brief Get size of null terminated string, provided strictly to help with constructing proper
//        string.
//
// @param[in] cstring ASCII or UTF8 string with terminating null byte.
//
// @param[in] max_length Known limit in which string should be in. If 0 passed - some arbitrary
//            number of chars is deemed acceptable.
//
IT_INTERNAL IT_PURE size_t it_cstring_length(char const *cstring, size_t max_length);

#include <stdint.h>
#include <stdlib.h>

#include <SDL2/SDL_main.h>

#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/managed_buffer.h>
#include <tomolipu/include/types/string.h>

#include <lupa/include/target.h>
#include <lupa/include/utils.h>

// todo: Switch console codepage to unicode under windows, at least when stdout logging is used.

#if defined(IT_PLATFORM_WINDOWS)
// https://learn.microsoft.com/en-us/troubleshoot/windows-client/shell-experience/command-line-string-limitation
#define argument_length_limit (8191U)
#elif defined(IT_PLATFORM_POSIX)
#include <unistd.h>
#else
#error Unknown platform to infer the maximum size of cli argument.
#endif

extern void it_entry(it_managed_buffer arguments);

int main(int argc, char **argv) {
    it_managed_buffer command_line_arguments
        = it_managed_buffer_init(it_typeinfo_init(it_string), (it_destructor)&it_string_free);

#if defined(IT_PLATFORM_POSIX)
    // todo: This is quite weak, and also doesn't actually provide length of a single parameter,
    //       but rather whole environment. We need to subtract from it each iteration to get proper
    //       limit.
    long argument_length_limit = sysconf(_SC_ARG_MAX);
    it_ensure(argument_length_limit != -1);
#endif

    for (int i = 1; i < argc; ++i) {
        size_t length = it_cstring_length(argv[i], (size_t)argument_length_limit);
        it_string argument = it_string_from_utf8(it_buffer_from_sequence(argv[i],
            it_convert(uint32_t, length), it_typeinfo_init(char), it_flag_access_readable));
        it_ensure(it_managed_buffer_push(&command_line_arguments, &argument));
    }

    it_entry(command_line_arguments);

    return EXIT_SUCCESS;
}

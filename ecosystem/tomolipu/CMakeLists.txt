cmake_minimum_required(VERSION 3.19)

project(tomolipu_project LANGUAGES C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS OFF)

# todo: Ideally should be fully separate.
set(IT_ILOTAWA_PATH ${CMAKE_SOURCE_DIR} CACHE STRING "ilotawa base directory")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${IT_ILOTAWA_PATH})
include(Shared)

# todo: Generic platform and backend dispatch for ecosystem.
if(IT_BACKEND MATCHES "SDL2")
    file(GLOB IT_BACKEND_SRCS ${CMAKE_CURRENT_SOURCE_DIR}/src/backend/sdl2/*.c)
endif()

file(GLOB_RECURSE IT_SRCS ${CMAKE_CURRENT_SOURCE_DIR}/src/*.c)
add_library(tomolipu STATIC ${IT_SRCS} ${IT_BACKEND_SRCS})
it_populate_target_options(tomolipu)

target_include_directories(tomolipu PRIVATE $<PATH:GET_PARENT_PATH, ${CMAKE_CURRENT_SOURCE_DIR}>)
# todo: For proper behavior of precompiling headers inline and extern implementations should be segregated.
# target_precompile_headers(tomolipu PRIVATE
#     include/interfaces/math.h
#     include/integer_ops.h
#     include/types/fixed.h
#     include/types/vector2.h)

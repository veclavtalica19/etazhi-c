/**
 * @brief      Target specific definitions.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

#include <tomolipu/include/compiler.h>

// todo: Figure out a way of inferring this, probably by -D passing.
#define IT_ARCH_HAS_FPU

#if defined(IT_COMPILER_CLANG) || defined(IT_COMPILER_GCC)

#if __SIZEOF_INT__ == 4
#define IT_ARCH_WORD_SIZE_32
#elif __SIZEOF_INT__ == 8
#define IT_ARCH_WORD_SIZE_64
#else
#error Word size unhandled.
#endif

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define IT_ARCH_LITTLE_ENDIAN
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define IT_ARCH_BIG_ENDIAN
#else
#error Byte order unhandled.
#endif

#endif // defined(IT_COMPILER_CLANG) || defined(IT_COMPILER_GCC)

/**
 * @brief      Generalization of compiler specific things and building pipeline.
 *
 * @author     Veclav Talica
 * @date       2023
 */

#pragma once

// todo: Better name for this header.

// todo: Introduce more robust way of checking pointer validity.
//       It should check for null, alignment and paging limitations (for example, no more than 48
//       bits can be valid on x86_64 and arm64)

#include <iso646.h>

#if defined(NDEBUG) && !defined(_DEBUG)
#define IT_BUILD_RELEASE
#else
#define IT_BUILD_DEBUG
#endif

#if defined(_MSC_VER)
#define IT_COMPILER_MSVC
#endif

#if defined(__clang__)
#define IT_COMPILER_CLANG
#endif

#if defined(__GNUC__)
#define IT_COMPILER_GCC
#endif

#if defined(IT_COMPILER_MSVC)

#include <stdint.h>

#define it_assume(p_expr)            __assume(p_expr)
#define it_unreachable()             __assume(0)
#define it_is_aligned(p_expr, bytes) (((uintptr_t)(void const *)(p_expr)) % (bytes) == 0)
#define it_align_cast(p_expr, type)  ((type)(void *)(p_expr))
// Note: Qualifiers are ignored.
#define it_compare_types_impl_(left, right) _Generic(((left) { 0 }), right : true, default : false)
#define it_compare_types(left, right)                                                              \
    (it_compare_types_impl_(left, right) and it_compare_types_impl_(right, left))
#define IT_API      __declspec(dllexport)
#define IT_INTERNAL __declspec(dllimport)
#define IT_PURE     __declspec(noalias)
#define IT_LEAF
#define IT_NO_NULL_RETURN
#define IT_FLAG_ENUMERATOR
#define IT_FALLTHROUGH ((void)0)

#elif defined(IT_COMPILER_CLANG) || defined(IT_COMPILER_GCC)

#define it_unreachable()             __builtin_unreachable()
#define it_is_aligned(p_expr, bytes) __builtin_is_aligned(p_expr, bytes)
#define it_align_cast(p_expr, type)  ((type)__builtin_assume_aligned(p_expr, _Alignof(type)))
// Note: Qualifiers are ignored.
#define it_compare_types(left, right)                                                              \
    __builtin_types_compatible_p(__typeof__(left), __typeof__(right))
#define it_likely(p_expr)   __builtin_expect((p_expr), 1)
#define it_unlikely(p_expr) __builtin_expect((p_expr), 0)
#define IT_API              __attribute__((visibility("default"))) __attribute__((warn_unused_result))
#define IT_INTERNAL         __attribute__((visibility("hidden"))) __attribute__((warn_unused_result))
#define IT_PURE             __attribute__((pure))
#define IT_LEAF             __attribute__((leaf))
#define IT_NO_NULL_RETURN   __attribute__((returns_nonnull))
#define IT_FLAG_ENUMERATOR  __attribute__((flag_enum))
#define IT_FALLTHROUGH      __attribute__((fallthrough))

#if defined(IT_COMPILER_CLANG)
#define it_assume(p_expr) (__builtin_assume(p_expr), p_expr)
#elif defined(IT_COMPILER_GCC)
#define it_assume(p_expr) (__builtin_expect((p_expr), 1) ? (void)0 : __builtin_unreachable())
#endif

#else
#warning Unknown compiler identity.

#include <stdint.h>
#include <tomolipu/include/testing.h>

#define it_assume(p_expr)
#define it_unreachable()                    it_ensure(false)
#define it_is_aligned(p_expr, bytes)        (((uintptr_t)(void const *)(p_expr)) % (bytes) == 0)
#define it_align_cast(p_expr, type)         ((type)(void *)(p_expr))
// Note: Qualifiers are ignored.
#define it_compare_types_impl_(left, right) _Generic(((left) { 0 }), right : true, default : false)
#define it_compare_types(left, right)                                                              \
    (it_compare_types_impl_(left, right) and it_compare_types_impl_(right, left))
#define it_likely(p_expr)   (p_expr)
#define it_unlikely(p_expr) (p_expr)
#define IT_API
#define IT_INTERNAL
#define IT_PURE
#define IT_LEAF
#define IT_NO_NULL_RETURN
#define IT_FLAG_ENUMERATOR
#define IT_FALLTHROUGH ((void)0)

#endif // defined(IT_COMPILER_...)

#define it_is_power_of_two(p_expr) (((p_expr bitand (p_expr - 1)) == 0) and ((p_expr != 0)))
#define it_alignof(p_expr)         _Alignof(p_expr)
#define it_static_assert(p_expr)   _Static_assert(p_expr, "Static assertion failed")
#define it_stringify(pasting)      #pasting

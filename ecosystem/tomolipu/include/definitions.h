#pragma once

#include <tomolipu/include/compiler.h>

enum IT_FLAG_ENUMERATOR it_flag {

    // @brief Specifies that resource is not dynamically allocated and its lifetime is independent
    // 	      from ilotawa.
    //
    it_flag_static = 1 << 0,
    it_flag_access_writable = 1 << 1,
    it_flag_access_readable = 1 << 2,
    it_flag_limit = 1 << 3
};

enum it_sign { it_sign_plus, it_sign_minus };

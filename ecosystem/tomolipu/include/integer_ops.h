/**
 * @brief      Safe and robust operations over sized integers.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

#include <tomolipu/include/compiler.h>

#if defined(IT_INTEGER_OPS_INLINED)

#include <tomolipu/include/testing.h>

#define IT_INTEGER_OPS_COMPILE
#define IT_INTEGER_OPS_ATTRIBUTES static inline
#define it_integer_ops_trap_      it_test

#else

#define IT_INTEGER_OPS_ATTRIBUTES IT_API

#endif // !defined(IT_INTEGER_OPS_INLINED)

#include <tomolipu/include/private/integer_ops_implementation.h>

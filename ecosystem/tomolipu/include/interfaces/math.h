/**
 * @brief      Type generics over mathematic functions.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

// note: Non inlined ops are used unless inlined version header was included previously.
#include <tomolipu/include/integer_ops.h>
#include <tomolipu/include/types/fixed.h>

// todo: Automatic type conversions for op parameters?
// todo: Generic integer range methods.

// clang-format off

#define it_math_generic_dispatch_for_integer_function_(p_function)                                 \
		int8_t   : it_int8_t_##p_function,                                                         \
		int16_t  : it_int16_t_##p_function,                                                        \
		int32_t  : it_int32_t_##p_function,                                                        \
		int64_t  : it_int64_t_##p_function,                                                        \
		uint8_t  : it_uint8_t_##p_function,                                                        \
		uint16_t : it_uint16_t_##p_function,                                                       \
		uint32_t : it_uint32_t_##p_function,                                                       \
		uint64_t : it_uint64_t_##p_function

#define it_math_generic_dispatch_for_append_function_(p_function, ...)                             \
        int8_t   : p_function##_int8_t(__VA_ARGS__),                                               \
        int16_t  : p_function##_int16_t(__VA_ARGS__),                                              \
        int32_t  : p_function##_int32_t(__VA_ARGS__),                                              \
        int64_t  : p_function##_int64_t(__VA_ARGS__),                                              \
        uint8_t  : p_function##_uint8_t(__VA_ARGS__),                                              \
        uint16_t : p_function##_uint16_t(__VA_ARGS__),                                             \
        uint32_t : p_function##_uint32_t(__VA_ARGS__),                                             \
        uint64_t : p_function##_uint64_t(__VA_ARGS__)

// clang-format on

#define it_add(p_type, p_a, p_b)                                                                   \
    _Generic(((p_type) { 0 }), it_math_generic_dispatch_for_integer_function_(add))(p_a, p_b)

#define it_add_sat(p_type, p_a, p_b)                                                               \
    _Generic(((p_type) { 0 }), it_math_generic_dispatch_for_integer_function_(add_sat))(p_a, p_b)

#define it_sub(p_type, p_a, p_b)                                                                   \
    _Generic(((p_type) { 0 }), it_math_generic_dispatch_for_integer_function_(sub))(p_a, p_b)

#define it_mul(p_type, p_a, p_b)                                                                   \
    _Generic(((p_type) { 0 }), it_math_generic_dispatch_for_integer_function_(mul))(p_a, p_b)

#define it_div(p_type, p_a, p_b)                                                                   \
    _Generic(((p_type) { 0 }), it_math_generic_dispatch_for_integer_function_(div))(p_a, p_b)

#define it_inc(p_value)                                                                            \
    _Generic((p_value), it_math_generic_dispatch_for_integer_function_(add))(p_value, 1)

#define it_dec(p_value)                                                                            \
    _Generic((p_value), it_math_generic_dispatch_for_integer_function_(sub))(p_value, 1)

#define it_clamp(p_type, p_value, p_low, p_high)                                                   \
    _Generic(((p_type) { 0 }), it_math_generic_dispatch_for_integer_function_(clamp))(             \
        p_value, p_low, p_high)

#define it_convert(p_type, p_value)                                                                \
    _Generic(((p_type) { 0 }), int8_t                                                              \
             : (_Generic((p_value),                                                                \
                   it_math_generic_dispatch_for_append_function_(it_int8_t_from, p_value))),       \
             int16_t                                                                               \
             : (_Generic((p_value),                                                                \
                   it_math_generic_dispatch_for_append_function_(it_int16_t_from, p_value))),      \
             int32_t                                                                               \
             : (_Generic((p_value),                                                                \
                   it_math_generic_dispatch_for_append_function_(it_int32_t_from, p_value))),      \
             int64_t                                                                               \
             : (_Generic((p_value),                                                                \
                   it_math_generic_dispatch_for_append_function_(it_int64_t_from, p_value))),      \
             uint8_t                                                                               \
             : (_Generic((p_value),                                                                \
                   it_math_generic_dispatch_for_append_function_(it_uint8_t_from, p_value))),      \
             uint16_t                                                                              \
             : (_Generic((p_value),                                                                \
                   it_math_generic_dispatch_for_append_function_(it_uint16_t_from, p_value))),     \
             uint32_t                                                                              \
             : (_Generic((p_value),                                                                \
                   it_math_generic_dispatch_for_append_function_(it_uint32_t_from, p_value))),     \
             uint64_t                                                                              \
             : (_Generic((p_value),                                                                \
                 it_math_generic_dispatch_for_append_function_(it_uint64_t_from, p_value))))

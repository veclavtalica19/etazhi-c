/**
 * @brief      Reflection typedefs and utilities.
 *
 * @author     Veclav Talica
 * @date       2022
 */

// todo: Rework.

#pragma once

#include <stdalign.h>
#include <stddef.h>
#include <stdint.h>

#include <tomolipu/include/testing.h>

typedef struct it_typeinfo {
    uint16_t size;
    uint16_t alignment;
} it_typeinfo;

enum it_ctype {
    // Basic (numerical) types
    it_ctype_invalid,
    it_ctype_char,
    it_ctype_signed_char,
    it_ctype_unsigned_char,
    it_ctype_short,
    it_ctype_unsigned_short,
    it_ctype_int,
    it_ctype_unsigned_int,
    it_ctype_long,
    it_ctype_unsigned_long,
    it_ctype_long_long,
    it_ctype_unsigned_long_long,
    it_ctype_float,
    it_ctype_double,
    it_ctype_long_double,
    // C99 bool
    it_ctype_bool,
    // Special types
    it_ctype_char_pointer_to_null_string,

    it_ctype_count
};

#define it_typeinfo_init(type)                                                                     \
    (it_typeinfo) { .size = sizeof(type), .alignment = alignof(type) }

static inline IT_PURE size_t it_ctype_alignof(enum it_ctype type) {
#define CASE(ct, t)                                                                                \
    case it_ctype_##ct:                                                                            \
        return alignof(t)
    switch (type) {
        CASE(char, char);
        CASE(signed_char, signed char);
        CASE(unsigned_char, unsigned char);
        CASE(short, short);
        CASE(unsigned_short, unsigned short);
        CASE(int, int);
        CASE(unsigned_int, unsigned int);
        CASE(long, long);
        CASE(unsigned_long, unsigned long);
        CASE(long_long, long long);
        CASE(unsigned_long_long, unsigned long long);
        CASE(float, float);
        CASE(double, double);
        CASE(long_double, long double);
        CASE(bool, bool);
    default:
        it_ensure(false);
    }
    return 0;
#undef CASE
}

static inline IT_PURE size_t it_ctype_sizeof(enum it_ctype type) {
#define CASE(ct, t)                                                                                \
    case it_ctype_##ct:                                                                            \
        return sizeof(t)
    switch (type) {
        CASE(char, char);
        CASE(signed_char, signed char);
        CASE(unsigned_char, unsigned char);
        CASE(short, short);
        CASE(unsigned_short, unsigned short);
        CASE(int, int);
        CASE(unsigned_int, unsigned int);
        CASE(long, long);
        CASE(unsigned_long, unsigned long);
        CASE(long_long, long long);
        CASE(unsigned_long_long, unsigned long long);
        CASE(float, float);
        CASE(double, double);
        CASE(long_double, long double);
        CASE(bool, bool);
    default:
        it_ensure(false);
    }
    return 0;
#undef CASE
}

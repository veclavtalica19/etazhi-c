#pragma once

#include <iso646.h>
#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/definitions.h>
#include <tomolipu/include/reflection.h>

// @brief Generic low-level buffer of allocated or non-allocated data.
//
typedef struct it_buffer {
    struct {
        void *data;
        uint32_t length;
        uint32_t capacity;
        it_typeinfo typeinfo;
        enum it_flag flags;
    } private;
} it_buffer;

typedef struct it_buffer_iterator {
    void *current;

    struct {
        it_typeinfo const typeinfo;
        uint32_t left;
    } private;
} it_buffer_iterator;

IT_API IT_PURE it_buffer it_buffer_init(it_typeinfo typeinfo);

IT_API IT_PURE it_buffer it_buffer_from_sequence(
    void *sequence, uint32_t sequence_length, it_typeinfo sequence_typeinfo, enum it_flag flags);

IT_API IT_PURE uint32_t it_buffer_get_length(it_buffer const *buffer);

IT_API IT_PURE it_typeinfo it_buffer_get_typeinfo(it_buffer const *buffer);

IT_API IT_PURE enum it_flag it_buffer_get_flags(it_buffer const *buffer);

IT_API bool it_buffer_ensure_capacity(it_buffer *buffer, uint32_t capacity);

IT_API bool it_buffer_grow_by_item_count(it_buffer *buffer, uint32_t item_count);

IT_API bool it_buffer_grow_to_length(it_buffer *buffer, uint32_t length);

// @brief Append new item to buffer.
//
// @warning Ownership of item is transferred to buffer.
//
IT_API bool it_buffer_push(it_buffer *restrict buffer, void const *restrict item);

// @brief Set item at index, rewriting previous state.
//
// @warning Ownership of item is transferred to buffer.
// @warning Item should not be a resource handler, as it would leak.
//
IT_API void it_buffer_set(
    it_buffer const *restrict buffer, uint32_t index, void const *restrict item);

IT_API IT_PURE void *it_buffer_get(it_buffer const *buffer, uint32_t index) IT_NO_NULL_RETURN;

// @brief Place 'other' to the end of 'buffer'
//
IT_API bool it_buffer_append(it_buffer *buffer, it_buffer other);

IT_API void it_buffer_free(it_buffer *buffer);

// todo: it_buffer_delete(it_buffer *buffer, uint32_t index);

IT_API IT_PURE it_buffer it_buffer_make_view(it_buffer const *buffer, enum it_flag flags);

IT_API IT_PURE it_buffer it_buffer_make_subview(
    it_buffer const *buffer, uint32_t start, uint32_t length, enum it_flag flags);

IT_API void it_buffer_shrink(it_buffer *buffer);

IT_API IT_PURE bool it_buffer_check_equality(
    it_buffer const *restrict buffer, it_buffer const *restrict other);

IT_API IT_PURE bool it_buffer_is_initialized(it_buffer const *buffer);

// @brief Initialize iterator over buffer with write and/or write access.
//
IT_API IT_PURE it_buffer_iterator it_buffer_iterator_init(
    it_buffer const *buffer, enum it_flag flags);

IT_API bool it_buffer_iterator_pump(it_buffer_iterator *it);

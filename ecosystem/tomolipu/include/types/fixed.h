/**
 * @brief      Portable and simple fixed point fixed implementation.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 * Format is S15.16, picked for interoperability with OpenGL ES fixed point.
 *
 */

#pragma once

#include <stdint.h>

#include <tomolipu/include/arch.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/definitions.h>

enum { it_fixed_denominator = UINT16_MAX + 1 };

typedef struct it_fixed {
    struct {
        union {
            int32_t value;

            // todo: Should be ordered based on endianess.
            struct {

#if defined(IT_ARCH_LITTLE_ENDIAN)
                uint16_t fraction;
                int16_t whole; /* Rounded towards negative */

#elif defined(IT_ARCH_BIG_ENDIAN)
                int16_t whole; /* Rounded towards negative */
                uint16_t fraction;
#endif
            } parts;
        };
    } private;
} it_fixed;

#if defined(IT_FIXED_INLINED)

#include <tomolipu/include/testing.h>

#define IT_FIXED_COMPILE
#define IT_FIXED_ATTRIBUTES static inline
#define it_fixed_trap_      it_test

#else

#include <tomolipu/include/testing.h>

#define IT_FIXED_ATTRIBUTES IT_API
#define it_fixed_trap_      it_ensure

#endif // !defined(IT_FIXED_INLINED)

#include <tomolipu/include/types/private/fixed_implementation.h>

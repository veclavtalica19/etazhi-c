#pragma once

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/buffer.h>

// todo: As this uses callbacks and not polling - we might move it to private.
// todo: Rename to 'resource_buffer'.

// todo: Move somewhere else.
typedef void (*it_destructor)(void *item);

// @brief Wrapper over buffer that allows automatic destructors.
//
// @warning Do not mutate length of buffer field, otherwise destructors will not be called properly.
//
// todo: Encrypt stored function pointer. It's rather easy to get arbitrary code execution going
//       when you have them just stored and modifiable.
//       https://wiki.sei.cmu.edu/confluence/display/c/WIN04-C.+Consider+encrypting+function+pointers
typedef struct it_managed_buffer {
    it_buffer buffer;
    it_destructor destructor;
} it_managed_buffer;

IT_API IT_PURE it_managed_buffer it_managed_buffer_init(
    it_typeinfo typeinfo, it_destructor destructor);

IT_API bool it_managed_buffer_ensure_capacity(it_managed_buffer *buffer, uint32_t capacity);

// @brief Append new item to buffer.
//
// @warning Ownership of item is transferred to buffer.
//
IT_API bool it_managed_buffer_push(it_managed_buffer *buffer, void const *item);

IT_API IT_PURE void *it_managed_buffer_get(
    it_managed_buffer const *buffer, uint32_t index) IT_NO_NULL_RETURN;

IT_API void it_managed_buffer_free(it_managed_buffer *buffer);

// todo: it_managed_buffer_delete(it_managed_buffer *buffer, uint32_t index);
// Problem with this is that we can't later infer what's freed in dotted buffer.
// We probably need to implement it over generational buffer, or allocate parallel buffer with
// booleans. (But using bitsets is preferable)

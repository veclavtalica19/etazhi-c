/**
 * @brief      Generic 2D map.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

#include <stdint.h>

#include <tomolipu/include/reflection.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/vector2.h>

typedef struct it_map {

    struct {
        it_vector2_uint16_t dimensions;
        it_buffer cells;
    } private;
} it_map;

// todo: Make it a generic 'grid' iterator.
//
typedef struct it_map_iterator {
    it_vector2_uint16_t current;

    struct {
        it_vector2_uint16_t const target;
        bool was_pumped;
    } private;
} it_map_iterator;

IT_API IT_PURE it_map it_map_init(it_typeinfo typeinfo);

IT_API IT_PURE it_vector2_uint16_t it_map_get_dimensions(it_map const *map);

IT_API bool it_map_resize(it_map *result, it_vector2_uint16_t dimensions);

IT_API void it_map_set(it_map *map, it_vector2_uint16_t coordinate, void const *item);

IT_API IT_PURE void *it_map_get(
    it_map const *map, it_vector2_uint16_t coordinate) IT_NO_NULL_RETURN;

IT_API void it_map_shrink(it_map *map);

IT_API void it_map_free(it_map *map);

IT_API IT_PURE it_map_iterator it_map_iterator_init(it_map const *map);

IT_API IT_PURE it_map_iterator it_map_iterator_init_with_subarea(
    it_map const *map, it_vector2_uint16_t origin, it_vector2_uint16_t extend);

IT_API bool it_map_iterator_pump(it_map_iterator *it);

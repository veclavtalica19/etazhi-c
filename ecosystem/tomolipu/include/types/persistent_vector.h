#pragma once

#include <iso646.h>
#include <stdbool.h>
#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/definitions.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/types/buffer.h>

// todo: Rename to 'persistent_buffer' to follow the same convention as 'buffer' and 'managed
//       buffer' follow.
// todo: We could store both items and spots in single allocation, where spots are acting like stack
//       at the back. New structure over buffer would be appreciated for that.
// todo: Spots could use type smaller than full uint32_t when addressing doesn't require full range.
//
typedef struct it_persistent_vector {
    it_buffer items;
    it_buffer spots; /* Vector of places that are free for repopulation */
} it_persistent_vector;

typedef struct it_persistent_vector_iterator {
    void *current;
    it_buffer_iterator item_walker;
    it_buffer const *const spots;
} it_persistent_vector_iterator;

IT_API IT_PURE it_persistent_vector it_persistent_vector_init(it_typeinfo);

IT_API IT_PURE it_persistent_vector it_persistent_vector_from_buffer(it_buffer);

// @brief Returns key to newly allocated slot
//
// todo: Make it so 0 key is reserved for invalid items? Would play nicely with reliance on
// zero-initialization as default state.
IT_API bool it_persistent_vector_add(it_persistent_vector *vector, uint32_t *result);

// warn: Returned pointer is only valid until vector mutation
IT_API IT_PURE void *it_persistent_vector_get(
    it_persistent_vector const *vector, uint32_t key) IT_NO_NULL_RETURN;

IT_API bool it_persistent_vector_delete(it_persistent_vector *vector, uint32_t key);

// todo: ability to reduce memory footprint
// void it_persistent_vector_shrink(it_persistent_vector );

IT_API void it_persistent_vector_free(it_persistent_vector *vector);

IT_API IT_PURE it_persistent_vector_iterator it_persistent_vector_iterator_init(
    it_persistent_vector const *vector, enum it_flag flags);

IT_API bool it_persistent_vector_iterator_pump(it_persistent_vector_iterator *it);

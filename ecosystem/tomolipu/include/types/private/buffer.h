#pragma once

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/buffer.h>

IT_INTERNAL IT_PURE void *it_buffer_get_end_pointer(it_buffer const *buffer) IT_NO_NULL_RETURN;

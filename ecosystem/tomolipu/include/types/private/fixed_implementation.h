#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <tomolipu/include/arch.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/fixed.h>

// https://booksite.elsevier.com/samplechapters/9781558607989/9781558607989.PDF

// @warning Parameters should not be side effecting.
//
// todo: Negatiion of zero without fractional part should result in same value as positive zero
//       without fractional part.
//
#define it_fixed_const_init(p_sign, p_whole, p_fraction)                                           \
    ((it_fixed) { .private                                                                         \
        = { .parts = { .whole = ((p_sign) == it_sign_plus) ? (int16_t)(p_whole)                    \
                                                           : (int16_t) ~(int32_t)(p_whole),        \
                .fraction = ((p_sign) == it_sign_plus) ? (uint16_t)(p_fraction)                    \
                                                       : (uint16_t) ~(uint32_t)(p_fraction) } } })

IT_FIXED_ATTRIBUTES IT_PURE it_fixed it_fixed_init(
    enum it_sign sign, uint16_t whole, uint16_t portion);

IT_FIXED_ATTRIBUTES IT_PURE it_fixed it_fixed_from_whole(int16_t whole);

IT_FIXED_ATTRIBUTES IT_PURE int16_t it_fixed_get_whole(it_fixed a);

IT_FIXED_ATTRIBUTES IT_PURE uint16_t it_fixed_get_fraction(it_fixed a);

IT_FIXED_ATTRIBUTES IT_PURE it_fixed it_fixed_trim(it_fixed a);

IT_FIXED_ATTRIBUTES IT_PURE it_fixed it_fixed_add(it_fixed a, it_fixed b);

IT_FIXED_ATTRIBUTES IT_PURE it_fixed it_fixed_sub(it_fixed a, it_fixed b);

IT_FIXED_ATTRIBUTES IT_PURE it_fixed it_fixed_mul(it_fixed a, it_fixed b);

IT_FIXED_ATTRIBUTES IT_PURE it_fixed it_fixed_div(it_fixed a, it_fixed b);

IT_FIXED_ATTRIBUTES IT_PURE bool it_fixed_is_zero(it_fixed a);

#if defined(IT_ARCH_HAS_FPU)

// todo: Rather have separate 'IT_ARCH_HAS_FLOAT' and 'IT_ARCH_HAS_DOUBLE'.

IT_FIXED_ATTRIBUTES IT_PURE float it_fixed_to_float(it_fixed a);

IT_FIXED_ATTRIBUTES IT_PURE double it_fixed_to_double(it_fixed a);

#endif // defined(IT_ARCH_HAS_FPU)

#if defined(IT_FIXED_COMPILE)

#include <tomolipu/include/integer_ops.h>

IT_FIXED_ATTRIBUTES it_fixed it_fixed_init(
    enum it_sign const sign, uint16_t const whole, uint16_t const portion) {
    it_fixed_trap_(sign == it_sign_plus or sign == it_sign_minus);
    return it_fixed_const_init(sign, whole, portion);
}

IT_FIXED_ATTRIBUTES it_fixed it_fixed_from_whole(int16_t const whole) {
    return (it_fixed) { .private = { .parts = { .whole = whole } } };
}

IT_FIXED_ATTRIBUTES int16_t it_fixed_get_whole(it_fixed const a) { return a.private.parts.whole; }

IT_FIXED_ATTRIBUTES uint16_t it_fixed_get_fraction(it_fixed const a) {
    return a.private.parts.fraction;
}

IT_FIXED_ATTRIBUTES it_fixed it_fixed_trim(it_fixed const a) {
    return (it_fixed) { .private = { .parts = { .fraction = a.private.parts.fraction } } };
}

IT_FIXED_ATTRIBUTES it_fixed it_fixed_add(it_fixed const a, it_fixed const b) {
    return (it_fixed) { it_int32_t_add(a.private.value, b.private.value) };
}

IT_FIXED_ATTRIBUTES it_fixed it_fixed_sub(it_fixed const a, it_fixed const b) {
    return (it_fixed) { it_int32_t_sub(a.private.value, b.private.value) };
}

// todo: 32bit word version.
// todo: Catch overflow.
IT_FIXED_ATTRIBUTES it_fixed it_fixed_mul(it_fixed const a, it_fixed const b) {
    return (it_fixed) { (
        int32_t)((uint64_t)it_int64_t_mul(a.private.value, b.private.value) >> (uint64_t)16) };
}

IT_FIXED_ATTRIBUTES it_fixed it_fixed_div(it_fixed const a, it_fixed const b) {
    return (
        it_fixed) { (int32_t)(it_int32_t_div(a.private.value, b.private.value) << (uint32_t)16) };
}

// todo: Test if it holds.
IT_FIXED_ATTRIBUTES bool it_fixed_is_zero(it_fixed const a) {
    return a.private.value == (int32_t)0;
}

#if defined(IT_ARCH_HAS_FPU)

IT_FIXED_ATTRIBUTES float it_fixed_to_float(it_fixed const a) {
    return (float)a.private.parts.whole
        + (float)a.private.parts.fraction * (1.0f / (float)it_fixed_denominator);
}

// todo: Can use more imprecise form of `(double)a.private.value / (double)it_fixed_denominator)`
// when it's
//       not as relevant.
//
IT_FIXED_ATTRIBUTES double it_fixed_to_double(it_fixed const a) {
    return (double)a.private.parts.whole
        + (double)a.private.parts.fraction * (1.0 / (double)it_fixed_denominator);
}

#endif // defined(IT_ARCH_HAS_FPU)

#endif // defined(IT_FIXED_COMPILE)

#undef IT_FIXED_COMPILE
#undef IT_FIXED_ATTRIBUTES
#undef it_fixed_trap_

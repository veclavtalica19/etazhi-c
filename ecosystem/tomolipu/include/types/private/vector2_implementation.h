#pragma once

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/vector2.h>

#if defined(IT_VECTOR2_COMPILE)

#include <tomolipu/include/interfaces/math.h>

#define it_vector2_implement_binary_operation(p_name, p_operation, p_type, p_typename)             \
    IT_VECTOR2_ATTRIBUTES it_vector2_##p_typename it_vector2_##p_typename##_##p_name(              \
        it_vector2_##p_typename a, it_vector2_##p_typename b);                                     \
    IT_VECTOR2_ATTRIBUTES it_vector2_##p_typename it_vector2_##p_typename##_##p_name(              \
        it_vector2_##p_typename a, it_vector2_##p_typename b) {                                    \
        return (it_vector2_##p_typename) { .x = it_##p_typename##_##p_operation(a.x, b.x),         \
            .y = it_##p_typename##_##p_operation(a.y, b.y) };                                      \
    }

#else

#define it_vector2_implement_binary_operation(p_name, p_operation, p_type, p_typename)             \
    IT_API IT_PURE it_vector2_##p_typename it_vector2_##p_typename##_##p_name(                     \
        it_vector2_##p_typename a, it_vector2_##p_typename b);

#endif // defined(IT_VECTOR2_COMPILE)

// clang-format off

#define it_vector2_implement_math_operations(p_typename)                                           \
    it_vector2_implement_binary_operation(add, add, p_typename, p_typename)                        \
    it_vector2_implement_binary_operation(add_sat, add_sat, p_typename, p_typename)                \
    it_vector2_implement_binary_operation(sub, sub, p_typename, p_typename)                        \
    it_vector2_implement_binary_operation(mul, mul, p_typename, p_typename)                        \
    it_vector2_implement_binary_operation(div, div, p_typename, p_typename)

it_vector2_implement_math_operations(uint8_t)
it_vector2_implement_math_operations(uint16_t)
it_vector2_implement_math_operations(uint32_t)
it_vector2_implement_math_operations(uint64_t)
it_vector2_implement_math_operations(int8_t)
it_vector2_implement_math_operations(int16_t)
it_vector2_implement_math_operations(int32_t)
it_vector2_implement_math_operations(int64_t)
it_vector2_implement_binary_operation(add, add, it_fixed, fixed)
it_vector2_implement_binary_operation(sub, sub, it_fixed, fixed)
it_vector2_implement_binary_operation(mul, mul, it_fixed, fixed)
it_vector2_implement_binary_operation(div, div, it_fixed, fixed)

#define it_vector2_generic_dispatch_for_function_from_expr_(function, expr)                        \
    _Generic(((expr).x),                                                                           \
        int8_t   : function##_int8_t,                                                              \
        int16_t  : function##_int16_t,                                                             \
        int32_t  : function##_int32_t,                                                             \
        int64_t  : function##_int64_t,                                                             \
        uint8_t  : function##_uint8_t,                                                             \
        uint16_t : function##_uint16_t,                                                            \
        uint32_t : function##_uint32_t,                                                            \
        uint64_t : function##_uint64_t,                                                            \
        it_fixed : function##_fixed)

// clang-format on

#define it_vector2_add(a, b)                                                                       \
    it_vector2_generic_dispatch_for_function_from_expr_(it_vector2_add, a)(a, b)

#define it_vector2_add_sat(a, b)                                                                   \
    it_vector2_generic_dispatch_for_function_from_expr_(it_vector2_add_sat, a)(a, b)

#define it_vector2_sub(a, b)                                                                       \
    it_vector2_generic_dispatch_for_function_from_expr_(it_vector2_add_sub, a)(a, b)

#define it_vector2_mul(a, b)                                                                       \
    it_vector2_generic_dispatch_for_function_from_expr_(it_vector2_add_mul, a)(a, b)

#define it_vector2_div(a, b)                                                                       \
    it_vector2_generic_dispatch_for_function_from_expr_(it_vector2_add_div, a)(a, b)

#undef it_vector2_implement_for_type
#undef it_vector2_implement_binary_operation
#undef it_vector2_implement_math_operations

#undef IT_VECTOR2_COMPILE
#undef IT_VECTOR2_ATTRIBUTES
#undef it_vector2_trap_

/**
 * @brief      Varying type buffer.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/types/buffer.h>

typedef struct it_varying_buffer {
    struct {
        it_buffer buffer;
    } private;
} it_varying_buffer;

typedef struct it_varying_buffer_iterator {
    void *current;

    struct {
        uint32_t bytes_left;
        it_typeinfo current_item_typeinfo;
        enum it_flag flags;
    } private;
} it_varying_buffer_iterator;

IT_API IT_PURE it_varying_buffer it_varying_buffer_init(void);

// @brief Push item after current.
//
// @warning It will truncate items that were stored after current beforehand.
//
IT_API bool it_varying_buffer_push(it_varying_buffer *restrict varying_buffer,
    it_varying_buffer_iterator *restrict it,
    it_typeinfo item_typeinfo,
    void const *restrict item);

IT_API void it_varying_buffer_free(it_varying_buffer *varying_buffer);

IT_API void it_varying_buffer_shrink(it_varying_buffer *varying_buffer);

IT_API IT_PURE it_varying_buffer_iterator it_varying_buffer_iterator_init(
    it_varying_buffer const *varying_buffer, enum it_flag flags);

IT_API bool it_varying_buffer_iterator_pump(it_varying_buffer_iterator *it,
    it_varying_buffer const *varying_buffer,
    it_typeinfo next_item_typeinfo);

IT_API IT_PURE bool it_varying_buffer_iterator_is_at_end(it_varying_buffer_iterator const *it);

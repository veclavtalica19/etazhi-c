#include <SDL2/SDL_assert.h>
#include <SDL2/SDL_log.h>

#include <tomolipu/include/testing.h>

void it_ensure_expression(
    bool status, char const *filename, uint32_t line, char const *expression) {
    if (not status) {
        SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "%s:%i: Assertion failed for: %s", filename,
            line, expression);
        SDL_TriggerBreakpoint();
    }
}

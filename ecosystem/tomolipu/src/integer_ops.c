#include <tomolipu/include/compiler.h>
#include <tomolipu/include/testing.h>

#define IT_INTEGER_OPS_COMPILE
#define IT_INTEGER_OPS_ATTRIBUTES IT_API
#define it_integer_ops_trap_      it_ensure

#include <tomolipu/include/integer_ops.h>
#include <tomolipu/include/private/integer_ops_implementation.h>

// clang-format on

#include <iso646.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/private/memory.h>
#include <tomolipu/include/testing.h>

it_memory_stats stats;

bool it_memory_is_byte_sequence_equal_to(
    void const *restrict sequence, size_t bytes, uint8_t value) {
    it_test(sequence != NULL);

    uint8_t const *casted_sequence = sequence;
    for (; bytes--; casted_sequence++) {
        if (*casted_sequence != value)
            return false;
    }

    return true;
}

it_memory_stats it_memory_get_stats(void) { return stats; }

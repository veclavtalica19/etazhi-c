#include <tomolipu/include/compiler.h>
#include <tomolipu/include/testing.h>

#define IT_FIXED_COMPILE
#define IT_FIXED_ATTRIBUTES IT_API
#define it_fixed_trap_      it_ensure

#include <tomolipu/include/types/fixed.h>
#include <tomolipu/include/types/private/fixed_implementation.h>

// clang-format on

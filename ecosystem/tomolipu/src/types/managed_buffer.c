#include <iso646.h>
#include <stdbool.h>
#include <stdint.h>

#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/managed_buffer.h>

it_managed_buffer it_managed_buffer_init(it_typeinfo typeinfo, it_destructor destructor) {
    it_test(destructor != NULL);
    return (it_managed_buffer) { .buffer = it_buffer_init(typeinfo), .destructor = destructor };
}

bool it_managed_buffer_ensure_capacity(it_managed_buffer *buffer, uint32_t capacity) {
    return it_buffer_ensure_capacity(&buffer->buffer, capacity);
}

bool it_managed_buffer_push(it_managed_buffer *buffer, void const *item) {
    return it_buffer_push(&buffer->buffer, item);
}

void *it_managed_buffer_get(it_managed_buffer const *buffer, uint32_t index) {
    return it_buffer_get(&buffer->buffer, index);
}

void it_managed_buffer_free(it_managed_buffer *buffer) {
    it_test(buffer->destructor != NULL);
    for (uint32_t i = 0; i < buffer->buffer.private.length; ++i) {
        buffer->destructor(it_managed_buffer_get(buffer, i));
    }
}

// todo: it_managed_buffer_delete(it_managed_buffer *buffer, uint32_t index);

#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/map.h>
#include <tomolipu/include/types/vector2.h>

it_map it_map_init(it_typeinfo typeinfo) {
    return (it_map) { .private = {
                          .cells = it_buffer_init(typeinfo),
                      } };
}

it_vector2_uint16_t it_map_get_dimensions(it_map const *const map) {
    it_test(map != NULL);

    return map->private.dimensions;
}

bool it_map_resize(it_map *const result, it_vector2_uint16_t dimensions) {
    it_test(result != NULL);
    it_test(dimensions.width != 0 and dimensions.height != 0);

    uint32_t const new_area = (uint32_t)dimensions.width * (uint32_t)dimensions.height;
    uint32_t const old_area
        = (uint32_t)result->private.dimensions.width * (uint32_t)result->private.dimensions.height;

    if ((new_area > old_area) and (dimensions.width == result->private.dimensions.width)) {
        // Trivially extend.

        if (not it_buffer_grow_to_length(&result->private.cells, new_area))
            return false;

    } else if ((new_area < old_area) or (new_area > old_area)) {
        // Truncate or extend while rearranging.

        it_buffer cells = it_buffer_init(result->private.cells.private.typeinfo);

        if (not it_buffer_grow_to_length(&cells, new_area))
            return false;

        // todo: We could copy whole lines.
        // Preserve original cells.
        uint32_t index = 0;
        for (it_map_iterator it = it_map_iterator_init(result); it_map_iterator_pump(&it);) {
            if (it.current.x >= dimensions.width)
                continue;
            if (it.current.y >= dimensions.height)
                break;

            it_buffer_set(&cells, index, it_map_get(result, it.current));

            index++;
        }

        it_map_free(result);

        *result = (it_map) { .private = { .cells = cells, .dimensions = dimensions } };
    }

    return true;
}

void it_map_set(it_map *const map, it_vector2_uint16_t coordinate, void const *const item) {
    it_test(map != NULL and item != NULL);
    it_test(coordinate.x < map->private.dimensions.width);
    it_test(coordinate.y < map->private.dimensions.height);

    it_buffer_set(&map->private.cells,
        it_add(
            uint32_t, coordinate.x, it_mul(uint32_t, coordinate.y, map->private.dimensions.width)),
        item);
}

void *it_map_get(it_map const *const map, it_vector2_uint16_t coordinate) {
    it_test(map != NULL);
    it_test(coordinate.x < map->private.dimensions.width);
    it_test(coordinate.y < map->private.dimensions.height);

    return it_buffer_get(&map->private.cells,
        it_add(
            uint32_t, coordinate.x, it_mul(uint32_t, coordinate.y, map->private.dimensions.width)));
}

void it_map_shrink(it_map *map) {
    it_test(map != NULL);

    it_buffer_shrink(&map->private.cells);
}

void it_map_free(it_map *map) {
    it_test(map != NULL);

    it_buffer_free(&map->private.cells);
    map->private.dimensions = (it_vector2_uint16_t) { 0 };
}

it_map_iterator it_map_iterator_init(it_map const *map) {
    it_test(map != NULL);

    return it_map_iterator_init_with_subarea(
        map, (it_vector2_uint16_t) { 0 }, map->private.dimensions);
}

it_map_iterator it_map_iterator_init_with_subarea(
    it_map const *const map, it_vector2_uint16_t origin, it_vector2_uint16_t extend) {
    it_test(map != NULL);
    it_test((origin.x < map->private.dimensions.width)
        or (origin.x == 0 and map->private.dimensions.width == 0));
    it_test((origin.y < map->private.dimensions.height)
        or (origin.y == 0 and map->private.dimensions.height == 0));

    it_vector2_uint16_t const target
        = { .x = it_clamp(
                uint16_t, it_add(uint16_t, origin.x, extend.x), 0, map->private.dimensions.width),
              .y = it_clamp(uint16_t, it_add(uint16_t, origin.y, extend.y), 0,
                  map->private.dimensions.height) };

    return (it_map_iterator) { .current = { .x = origin.x - 1, .y = origin.y },
        .private = {
            .target = target,
        } };
}

bool it_map_iterator_pump(it_map_iterator *const it) {
    it_test(it != NULL);

    if (not it->private.was_pumped) {
        it->private.was_pumped = true;
        it->current.x++;
    } else
        it->current.x = it_inc(it->current.x);

    if (it->current.x == it->private.target.x) {
        it->current.x = 0;
        it->current.y = it_inc(it->current.y);
    }

    return it->current.y < it->private.target.y;
}

#include <iso646.h>
#include <stdbool.h>
#include <stdint.h>

#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/persistent_vector.h>

it_persistent_vector it_persistent_vector_init(it_typeinfo typeinfo) {
    return (it_persistent_vector) {
        .items = it_buffer_init(typeinfo),
        .spots = it_buffer_init(it_typeinfo_init(uint32_t)),
    };
}

it_persistent_vector it_persistent_vector_from_buffer(it_buffer buffer) {
    return (it_persistent_vector) {
        .items = buffer,
        .spots = it_buffer_init(it_typeinfo_init(uint32_t)),
    };
}

bool it_persistent_vector_add(it_persistent_vector *vector, uint32_t *result) {
    it_test(vector and result);
    if (vector->spots.private.length != 0) {
        vector->spots.private.length--;
        *result = *(uint32_t *)it_buffer_get(&vector->spots, vector->spots.private.length);
    } else {
        if (not it_buffer_ensure_capacity(&vector->items, vector->items.private.length + 1))
            return false;
        vector->items.private.length++;
        *result = vector->items.private.length;
    }
    return true;
}

void *it_persistent_vector_get(it_persistent_vector const *vector, uint32_t key) {
    it_test(vector != NULL);
    it_test(key != 0);
    it_test(key - 1 < vector->items.private.length);
    return it_buffer_get(&vector->items, key - 1);
}

// todo: Mark deleted item with hash or certain memory values, so that we could check it in *_get()
// for access after deletion.
// todo: Every spot that has key higher than current item length could be dropped.
bool it_persistent_vector_delete(it_persistent_vector *vector, uint32_t key) {
    it_test(vector != NULL);
    it_test(key != 0);
    it_test(key - 1 < vector->items.private.length);

    for (uint32_t i = 0; i < vector->spots.private.length; ++i) {
        it_test(*(uint32_t *)it_buffer_get(&vector->spots, i) != key);
    }

    if (key == vector->items.private.length) {
        vector->items.private.length--;
        return true;
    } else
        return it_buffer_push(&vector->spots, &key);
}

void it_persistent_vector_free(it_persistent_vector *vector) {
    it_test(vector != NULL);

    it_buffer_free(&vector->items);
    it_buffer_free(&vector->spots);
}

it_persistent_vector_iterator it_persistent_vector_iterator_init(
    it_persistent_vector const *vector, enum it_flag flags) {
    it_test(vector != NULL);

    return (it_persistent_vector_iterator) {
        .item_walker = it_buffer_iterator_init(&vector->items, flags),
        .spots = &vector->spots,
    };
}

bool it_persistent_vector_iterator_pump(it_persistent_vector_iterator *it) {
    it_test(it and it->spots);

    while (it_buffer_iterator_pump(&it->item_walker)) {
        for (it_buffer_iterator spot_it
             = it_buffer_iterator_init(it->spots, it_flag_access_readable);
             it_buffer_iterator_pump(&spot_it);) {
            if (*it_align_cast(it->item_walker.current, uint32_t *)
                == *it_align_cast(spot_it.current, uint32_t *))
                break;
        }

        it->current = it->item_walker.current;
        return true;
    }

    it->current = NULL;
    return false;
}

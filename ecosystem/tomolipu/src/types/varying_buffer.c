#include <iso646.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/private/memory.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/private/buffer.h>
#include <tomolipu/include/types/varying_buffer.h>

#define const_padding_byte_value (uint8_t)(0)

it_static_assert(const_padding_byte_value == 0);

inline static uint32_t calculate_padding(void const *const ptr, uint16_t const alignment) {
    it_test(alignment != 0);

    if (alignment == 1)
        return 0;

    return (uint32_t)(it_sub(uintptr_t, alignment, (uintptr_t)ptr % alignment) % alignment);
}

it_varying_buffer it_varying_buffer_init(void) {
    return (
        it_varying_buffer) { .private = { .buffer = it_buffer_init(it_typeinfo_init(uint8_t)) } };
}

bool it_varying_buffer_push(it_varying_buffer *restrict varying_buffer,
    it_varying_buffer_iterator *restrict it,
    it_typeinfo item_typeinfo,
    void const *restrict item) {
    it_test(varying_buffer and it and item);
    it_test(it_is_aligned(item, item_typeinfo.alignment));
    it_test(it->private.flags bitand it_flag_access_writable);
    it_test(not(it->private.flags bitand it_flag_static));
    it_test(item_typeinfo.size != 0 and item_typeinfo.alignment != 0);

    if (not it->current) {
        // todo: Respect alignment of first typeinfo in allocation?
        if (not it_buffer_ensure_capacity(&varying_buffer->private.buffer, item_typeinfo.size))
            return false;

        uint32_t const padding = calculate_padding(
            varying_buffer->private.buffer.private.data, item_typeinfo.alignment);

        // Required as buffer allocation is ensured to only be byte aligned.
        if (not it_buffer_ensure_capacity(
                &varying_buffer->private.buffer, (uint32_t)item_typeinfo.size + padding))
            return false;

        varying_buffer->private.buffer.private.length = (uint32_t)item_typeinfo.size + padding;

        it->current = (void *)it_add(
            uintptr_t, (uintptr_t)varying_buffer->private.buffer.private.data, padding);

        it_memory_set_byte_sequence(
            varying_buffer->private.buffer.private.data, padding, const_padding_byte_value);

    } else {
        it_test(it->private.current_item_typeinfo.size != 0
            and it->private.current_item_typeinfo.alignment != 0);
        it_test(it_is_aligned(it->current, it->private.current_item_typeinfo.alignment));
        it_test(it->current >= varying_buffer->private.buffer.private.data);
        it_test(it->current < it_buffer_get_end_pointer(&varying_buffer->private.buffer));

        // todo: Usages of temporary pointers should be scoped and not accessible after resize.

        void *const after_current = (void *)it_add(
            uintptr_t, (uintptr_t)it->current, it->private.current_item_typeinfo.size);

        uint32_t const padding = calculate_padding(after_current, item_typeinfo.alignment);
        uint32_t const needed_for_new_slot = it_add(uint32_t, item_typeinfo.size, padding);

        uintptr_t const current_offset
            = (uintptr_t)it->current - (uintptr_t)varying_buffer->private.buffer.private.data;

        uint32_t const new_length = it_add(uint32_t,
            it_add(uint32_t, it_convert(uint32_t, current_offset),
                it->private.current_item_typeinfo.size),
            needed_for_new_slot);

        if (not it_buffer_ensure_capacity(&varying_buffer->private.buffer, new_length))
            return false;

        varying_buffer->private.buffer.private.length = new_length;

        // Restore relative current as it was before relocation.
        it->current = (void *)it_add(
            uintptr_t, (uintptr_t)varying_buffer->private.buffer.private.data, current_offset);

        it_memory_set_byte_sequence((void *)it_add(uintptr_t, (uintptr_t)it->current,
                                        it->private.current_item_typeinfo.size),
            padding, const_padding_byte_value);

        // Iterate to newly created item.
        it->private.bytes_left = (uint32_t)item_typeinfo.size + padding;
        it_test(it_varying_buffer_iterator_pump(it, varying_buffer, item_typeinfo));
    }

    it_test(it_is_aligned(it->current, item_typeinfo.alignment));
    it_test(it->current >= varying_buffer->private.buffer.private.data);
    it_test(it->current < it_buffer_get_end_pointer(&varying_buffer->private.buffer));

    it_memory_copy_byte_sequence(it->current, item_typeinfo.size, item, item_typeinfo.size);

    it->private.bytes_left = 0;
    it->private.current_item_typeinfo = item_typeinfo;

    return true;
}

void it_varying_buffer_free(it_varying_buffer *varying_buffer) {
    it_test(varying_buffer != NULL);

    it_buffer_free(&varying_buffer->private.buffer);
}

void it_varying_buffer_shrink(it_varying_buffer *varying_buffer) {
    it_test(varying_buffer != NULL);

    it_buffer_shrink(&varying_buffer->private.buffer);
}

it_varying_buffer_iterator it_varying_buffer_iterator_init(
    it_varying_buffer const *varying_buffer, enum it_flag flags) {
    it_test(varying_buffer != NULL);
    it_test((varying_buffer->private.buffer.private.flags bitand flags) == flags);

    return (it_varying_buffer_iterator) { .private
        = { .bytes_left = varying_buffer->private.buffer.private.length, .flags = flags } };
}

bool it_varying_buffer_iterator_pump(it_varying_buffer_iterator *it,
    it_varying_buffer const *varying_buffer,
    it_typeinfo next_item_typeinfo) {
    it_test(varying_buffer and it);
    it_test(next_item_typeinfo.size != 0 and next_item_typeinfo.alignment != 0);

    if (it->private.bytes_left == 0)
        return false;

    if (not it->current) {
        it_test(it->private.current_item_typeinfo.size == 0
            and it->private.current_item_typeinfo.alignment == 0);

        uint32_t const padding = calculate_padding(
            varying_buffer->private.buffer.private.data, next_item_typeinfo.alignment);

        it->current = (void *)it_add(
            uintptr_t, (uintptr_t)varying_buffer->private.buffer.private.data, padding);
        it->private.bytes_left = it_sub(uint32_t, it->private.bytes_left, padding);

    } else {
        it_test(it->current != NULL);
        it_test(it->private.current_item_typeinfo.size != 0
            and it->private.current_item_typeinfo.alignment != 0);
        it_test(it_is_aligned(it->current, it->private.current_item_typeinfo.alignment));

        void const *const after_current = (void *)it_add(
            uintptr_t, (uintptr_t)it->current, it->private.current_item_typeinfo.size);

        uint32_t const padding = calculate_padding(after_current, next_item_typeinfo.alignment);

        it_test(it->private.bytes_left >= ((uint32_t)next_item_typeinfo.size + padding));
        it_test(
            it_memory_is_byte_sequence_equal_to(after_current, padding, const_padding_byte_value));

        it->current = (void *)it_add(uintptr_t, (uintptr_t)after_current, padding);
        it->private.bytes_left = it_sub(uint32_t, it->private.bytes_left, padding);
    }

    it_test(it->current != NULL);
    it_test(it->current >= varying_buffer->private.buffer.private.data);
    it_test(it->current < it_buffer_get_end_pointer(&varying_buffer->private.buffer));
    it_test(it_is_aligned(it->current, next_item_typeinfo.alignment));

    it->private.current_item_typeinfo = next_item_typeinfo;
    it->private.bytes_left = it_sub(uint32_t, it->private.bytes_left, next_item_typeinfo.size);

    return true;
}

bool it_varying_buffer_iterator_is_at_end(it_varying_buffer_iterator const *it) {
    it_test(it != NULL);

    return it->private.bytes_left == 0;
}

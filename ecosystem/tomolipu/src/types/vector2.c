#include <tomolipu/include/compiler.h>
#include <tomolipu/include/testing.h>

#define IT_VECTOR2_COMPILE
#define IT_VECTOR2_ATTRIBUTES IT_API
#define it_vector2_ops_trap_  it_ensure

#include <tomolipu/include/types/private/vector2_implementation.h>
#include <tomolipu/include/types/vector2.h>

// clang-format on

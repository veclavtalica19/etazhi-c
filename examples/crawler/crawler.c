#include "ilotawa/include/types/color.h"
#include "ilotawa/include/types/image.h"
#include "ilotawa/include/types/texture.h"
#include "kalama/include/interpolate.h"
#include "tomolipu/include/reflection.h"
#include "tomolipu/include/types/buffer.h"
#include "tomolipu/include/types/map.h"
#include "tomolipu/include/types/private/fixed_implementation.h"
#include <ilotawa/include/algos/movement.h>
#include <ilotawa/include/ilotawa.h>
#include <ilotawa/include/loader.h>
#include <ilotawa/include/types/atlas.h>
#include <ilotawa/include/types/direction.h>
#include <ilotawa/include/types/font.h>
#include <ilotawa/include/types/tilemap.h>

#include <tomolipu/include/types/string.h>

#include <kalama/include/types/dice.h>
#include <kalama/include/types/value_noise.h>

static it_fixed const const_player_move_units_per_frame
    = it_fixed_const_init(it_sign_plus, 1, it_fixed_denominator / 2);

static it_atlas entity_atlas;
static it_atlas tile_atlas;
static it_tilemap tilemap;
static it_texture noise_texture;
static it_font font;
static it_dice dice;

static it_vector2_fixed player_position;

static void render(void) {
    it_vector2_fixed center = it_area_point_at_origin(it_get_window_area(), it_origin_center);
    it_tilemap_draw(&tilemap, &tile_atlas, it_vector2_fixed_sub(center, player_position));
    it_atlas_draw(&entity_atlas, center, 0, 0,
        &(it_texture_draw_options) { .scaling = it_option_from(it_fixed_from_whole(2)) });
    it_string const text = it_string_from_literal("crawler demo");
    it_font_draw(&font, (it_vector2_fixed) { 0 }, &text,
        &(it_font_draw_options) { .color = it_option_init(255, 0, 0, 255),
            .scaling = it_option_from(it_fixed_from_whole(3)) });
    it_texture_draw(&noise_texture, (it_vector2_fixed) { 0 }, 0, 0, 128, 128,
        &(it_texture_draw_options) { .scaling = it_option_from(it_fixed_from_whole(5)) });
}

static void init(void) {
    dice = it_dice_init(1);

    {
        it_texture texture = { 0 };
        it_ensure(it_texture_from_file(&texture, "res/textures/entity_atlas.gif", true));
        it_ensure(it_atlas_from_texture(&entity_atlas, texture, 17, 19));
    }

    {
        it_texture texture = { 0 };
        it_ensure(it_texture_from_file(&texture, "res/textures/floor_tiles.gif", false));
        it_ensure(it_atlas_from_texture(&tile_atlas, texture, 32, 32));
    }

    {
        it_texture texture = { 0 };
        it_ensure(it_texture_from_file(&texture, "res/textures/font_atlas.gif", true));
        it_ensure(it_font_from_bitmap(&font, texture, 7, 9));
    }

    {
        it_map permutations = it_map_init(it_typeinfo_init(it_fixed));
        it_ensure(it_map_resize(&permutations, (it_vector2_uint16_t) { .x = 128, .y = 128 }));
        it_dice_clamp_fill_map(it_dice_init(123), &permutations);

        it_value_noise rng
            = it_value_noise_from_permutations(permutations, it_interpolation_nearest);
        it_map noise = it_map_init(it_typeinfo_init(it_color));
        it_ensure(it_map_resize(&noise, (it_vector2_uint16_t) { .x = 128, .y = 128 }));

        for (it_map_iterator it = it_map_iterator_init(&noise); it_map_iterator_pump(&it);) {
            it_fixed const value = it_value_noise_at(
                &rng, it.current, it_fixed_const_init(it_sign_plus, 0, it_fixed_denominator / 8));
            it_map_set(&noise, it.current,
                &(it_color) { .red = it_fixed_get_fraction(value) / (uint32_t)256, .alpha = 255 });
        }

        it_ensure(it_texture_from_image(&noise_texture,
            (it_image) { .buffer = noise.private.cells,
                .width = 128,
                .height = 128,
                .format = it_color_format_rgba8 },
            false));

        it_value_noise_free(&rng);
    }

    it_ensure(it_tilemap_resize(&tilemap, (it_vector2_uint16_t) { .x = 64, .y = 64 }));
    for (it_tilemap_iterator it = it_tilemap_iterator_init(&tilemap);
         it_tilemap_iterator_pump(&it);) {
        it_tilemap_set(&tilemap, it.current,
            (it_tilemap_cell) {
                (uint16_t)it_dice_roll(&dice, 2), (uint16_t)it_dice_roll(&dice, 2) });
    }

    it_ensure(it_play_track("res/tracks/mod62.xm"));
}

static void update(it_action_state const *state) {
    player_position = it_movement_8way_step(
        player_position, it_direction_from_action_state(state), const_player_move_units_per_frame);
    if (state->keys[it_key_action] == it_key_state_just_released)
        it_ensure(it_play_track("res/tracks/mod62.xm"));
}

extern void it_entry(it_managed_buffer arguments);

void it_entry(it_managed_buffer arguments) {
    it_configuration configuration = it_configuration_from_arguments(arguments);
    it_option_set_unspecified(&configuration.title, "crawler");
    it_ensure(it_initialize(configuration));
    init();
    it_state state = { 0 };
    while (it_poll(&state)) {
        if (state.is_update_frame)
            update(&state.actions);
        if (state.is_render_frame)
            render();
    }
    it_deinitialize();
}

#pragma once

#include <stdint.h>

enum { it_tracker_frequency = (48000) };

enum { it_tracker_channels = (2) };

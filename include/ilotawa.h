#pragma once

#include <iso646.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <ilotawa/include/types/area.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/definitions.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/managed_buffer.h>

// todo: Make separate include for 'bundle-everything-for-me-type-of-deal'
// todo: Coordinate system in desktop, not just window surface?

enum it_key { it_key_right, it_key_down, it_key_left, it_key_up, it_key_action, it_key_count };

// todo: Add 'it_key_state_just_pressed'
// todo: Rename 'it_key_state_pressed' to 'it_key_state_holding'
enum it_key_state {
    it_key_state_released,
    it_key_state_pressed,
    it_key_state_just_released,
    it_key_state_count
};

typedef struct it_action_state {
    uint16_t pointer_x;
    uint16_t pointer_y;
    it_vector2_fixed pointer_motion;
    enum it_key_state keys[it_key_count];
} it_action_state;

// todo: Have frame counts here.
typedef struct it_state {
    bool is_update_frame;
    bool is_render_frame;
    it_action_state actions;
} it_state;

typedef struct it_configuration {
    struct {
        char const *value; // todo: Use it_string?
        bool option;
    } title;

    struct {
        uint16_t value;
        bool option;
    } window_width;

    struct {
        uint16_t value;
        bool option;
    } window_height;

    struct {
        uint32_t value;
        bool option;
    } render_rate_ms;

    struct {
        uint32_t value;
        bool option;
    } update_rate_ms;

    struct {
        bool value;
        bool option;
    } is_software_mode;

    struct {
        bool value;
        bool option;
    } use_drawing_queue;
} it_configuration;

IT_API IT_PURE it_configuration it_configuration_from_arguments(it_managed_buffer arguments);

IT_API bool it_initialize(it_configuration configuration);

IT_API void it_deinitialize(void);

// todo: Implement over iterator interface?
IT_API bool it_poll(it_state *state);

// todo: Move to separate module.
// @brief Plays given xm track, if some other was playing - stop it and play a new one.
IT_API bool it_play_track(char const *filepath);

IT_API it_area it_get_window_area(void);

#pragma once

#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_video.h>

#include <tomolipu/include/types/managed_buffer.h>

#include <ilotawa/include/ilotawa.h>

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_AudioDeviceID audio_device;

extern uint16_t global_window_width;
extern uint16_t global_window_height;

extern bool global_renderer_has_blending;

extern it_configuration global_configuration;

extern uint32_t global_current_update_tick;
extern uint32_t global_current_render_tick;

IT_INTERNAL void it_log(char const *format, ...);

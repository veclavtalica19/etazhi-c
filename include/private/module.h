#pragma once

#include <stdbool.h>

#include <tomolipu/include/compiler.h>

// todo: Report callback for retrieving testing metrics, such as memory usage.

typedef bool it_module_initializer(void);

typedef void it_module_deinitializer(void);

// @brief Cleaner functions are called when hardware usage should be reduced.
//
// @warning Beware of cycling cleaner invocations, do not try to allocate any memory within the
//          callback.
//
typedef void it_module_cleaner(void);

typedef struct it_module_descriptor {
    it_module_initializer *const initializer;
    it_module_deinitializer *const deinitializer;
    it_module_cleaner *const cleaner;
} it_module_descriptor;

// @warning: Must be called before modules are used.
//
IT_INTERNAL void it_module_prepare(void);

IT_INTERNAL bool it_module_register(it_module_descriptor module);

IT_INTERNAL bool it_module_initialize_all(void);

IT_INTERNAL void it_module_deinitialize_all(void);

IT_INTERNAL void it_module_clean_all(void);

#pragma once

#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/vector2.h>

// todo: Should not be here.
enum it_origin { it_origin_center, it_origin_upperleft, it_origin_count };

// todo: Is fixed type needed for it?
typedef struct it_area {
    it_vector2_fixed origin;
    it_vector2_fixed extend;
} it_area;

IT_API IT_PURE it_vector2_fixed it_area_point_at_origin(it_area area, enum it_origin origin);

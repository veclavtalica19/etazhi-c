#pragma once

#include <ilotawa/include/ilotawa.h>
#include <ilotawa/include/types/color.h>
#include <ilotawa/include/types/texture.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/option.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/vector2.h>

typedef struct it_atlas {
    uint16_t tile_width;
    uint16_t tile_height;

    struct {
        it_texture texture;
    } private;
} it_atlas;

IT_API bool it_atlas_from_texture(
    it_atlas *result, it_texture texture, uint16_t tile_width, uint16_t tile_height);

IT_API void it_atlas_draw(it_atlas const *atlas,
    it_vector2_fixed position,
    uint16_t tile_x,
    uint16_t tile_y,
    it_texture_draw_options const *options);

IT_API void it_atlas_free(it_atlas *atlas);

#pragma once

#include <stddef.h>
#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/reflection.h>

enum { it_color_transparent_red = (0) };

enum { it_color_transparent_green = (0) };

enum { it_color_transparent_blue = (0) };

// todo: Define alpha key'ed color as separate format, or flag.
// todo: There's no need to have such granular formats, we could work with just transparency keyed
//       rgb8 and pallet, possibly configurable.
enum it_color_format {
    it_color_format_rgba8,
    it_color_format_rgb8,
    it_color_format_bgra8,
    it_color_format_undefined
};

typedef struct it_color_rgba8 {
    uint8_t red, green, blue, alpha;
} it_color_rgba8;

typedef struct it_color_rgb8 {
    uint8_t red, green, blue;
} it_color_rgb8;

typedef struct it_color_bgra8 {
    uint8_t blue, green, red, alpha;
} it_color_bgra8;

typedef it_color_rgba8 it_color;

IT_API IT_PURE size_t it_color_format_pixel_depth(enum it_color_format format);

IT_API IT_PURE it_typeinfo it_color_format_typeinfo(enum it_color_format format);

IT_API void it_color_set(void *result, enum it_color_format format, it_color color);

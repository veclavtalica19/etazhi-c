#pragma once

#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/types/buffer.h>

#include <ilotawa/include/types/color.h>

// todo: Implement over 'it_map'.
typedef struct it_image {
    it_buffer buffer;
    uint16_t width;
    uint16_t height;
    enum it_color_format format;
} it_image;

// todo: Should receive dimension argument.
//
IT_API IT_PURE it_image it_image_init(enum it_color_format);

IT_API void it_image_free(it_image *image);

// todo: X/Y coords over plain index.
//
IT_API void it_image_set_pixel(it_image *image, uint32_t index, it_color color);

#pragma once

#include <ilotawa/include/private/drawing_queue.h>
#include <ilotawa/include/private/module.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/fixed.h>

typedef struct it_renderer_command_draw_texture {
    it_color color;
    int32_t position_x, position_y;
    it_texture_id texture_id;
    it_fixed scaling, rotation;
    uint16_t texture_x, texture_y;
    uint16_t texture_width, texture_height;
    uint8_t origin;
} it_renderer_command_draw_texture;

IT_INTERNAL void it_texture_draw_callback(void const *untyped_command);

IT_INTERNAL bool it_texture_module_initialize(void);

IT_INTERNAL void it_texture_module_deinitialize(void);

#define it_texture_get_module_descriptor()                                                         \
    (it_module_descriptor) {                                                                       \
        .initializer = it_texture_module_initialize,                                               \
        .deinitializer = it_texture_module_deinitialize                                            \
    }

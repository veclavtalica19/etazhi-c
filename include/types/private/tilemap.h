#pragma once

#include <ilotawa/include/types/tilemap.h>
#include <tomolipu/include/compiler.h>

IT_INTERNAL bool it_tilemap_module_initialize(void);

IT_INTERNAL void it_tilemap_module_deinitialize(void);

IT_INTERNAL void it_tilemap_module_clean(void);

#define it_tilemap_get_module_descriptor()                                                         \
    (it_module_descriptor) {                                                                       \
        .initializer = it_tilemap_module_initialize,                                               \
        .deinitializer = it_tilemap_module_deinitialize, .cleaner = it_tilemap_module_clean        \
    }

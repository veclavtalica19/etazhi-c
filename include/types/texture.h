#pragma once

#include <ilotawa/include/types/area.h>
#include <ilotawa/include/types/image.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/fixed.h>

typedef struct it_texture_id {
    uint32_t value;
} it_texture_id;

// todo: Should dimensions be hidden behind getters?
//
typedef struct it_texture {
    uint16_t width;
    uint16_t height;

    struct {
        it_texture_id id;
    } private;
} it_texture;

// todo: Special type for angular values?
typedef struct it_texture_draw_options {
    // @default 1.0
    // @note Use integer scaling if software mode is desirable.
    struct {
        it_fixed value;
        bool option;
    } scaling;

    // @default 0.0
    // @note Use default (0.0) for software mode as much as possible.
    struct {
        it_fixed value;
        bool option;
    } rotation;

    // @default (255, 255, 255, 255)
    // @note Use (255) for alpha if software mode is desirable, as blending is costly.
    struct {
        it_color value;
        bool option;
    } color;

    // @default it_origin_center
    struct {
        enum it_origin value;
        bool option;
    } origin;
} it_texture_draw_options;

IT_API bool it_texture_from_image(it_texture *result, it_image image, bool with_transparency);

IT_API void it_texture_draw(it_texture const *texture,
    it_vector2_fixed position,
    uint16_t origin_x,
    uint16_t origin_y,
    uint16_t extend_width,
    uint16_t extend_height,
    it_texture_draw_options const *options);

IT_API void it_texture_free(it_texture *texture);

/**
 * @brief      Tileset data resource that is managed internally by ilotawa.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

#include <stdint.h>

#include <ilotawa/include/types/atlas.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/map.h>
#include <tomolipu/include/types/vector2.h>

typedef struct it_tilemap {
    struct {
        uint32_t id;
    } private;
} it_tilemap;

typedef struct it_tilemap_cell {
    uint16_t x;
    uint16_t y;
} it_tilemap_cell;

typedef it_map_iterator it_tilemap_iterator;

IT_API it_vector2_uint16_t it_tilemap_get_dimensions(it_tilemap const *tilemap);

IT_API bool it_tilemap_resize(it_tilemap *result, it_vector2_uint16_t dimensions);

IT_API void it_tilemap_set(
    it_tilemap *tilemap, it_vector2_uint16_t coordinate, it_tilemap_cell cell);

IT_API IT_PURE it_tilemap_cell it_tilemap_get(
    it_tilemap const *tilemap, it_vector2_uint16_t coordinate);

// todo: Options.
IT_API void it_tilemap_draw(
    it_tilemap const *tilemap, it_atlas const *atlas, it_vector2_fixed position);

IT_API void it_tilemap_free(it_tilemap *tilemap);

IT_API IT_PURE it_tilemap_iterator it_tilemap_iterator_init(it_tilemap const *tilemap);

IT_API IT_PURE it_tilemap_iterator it_tilemap_iterator_init_with_subarea(
    it_tilemap const *tilemap, it_vector2_uint16_t origin, it_vector2_uint16_t extend);

// @brief Iterator driver for a 'for' loop
//
// Usage:
// @code
// it_tilemap_iterator it = it_tilemap_iterator_init(&tilemap);
// for (it_tilemap_iterator it = it_tilemap_iterator_init(&tilemap);
//      it_tilemap_iterator_pump(&it);) {
//     it_tilemap_set(&tilemap, it.current.x, it.current.y, (it_tilemap_cell){0});
// }
// @endcode
//
IT_API bool it_tilemap_iterator_pump(it_tilemap_iterator *it);

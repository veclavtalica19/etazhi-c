/* Author: Romain "Artefact2" Dalmaso <artefact2@gmail.com> */
/* Contributor: Dan Spencer <dan@atomicpotato.net> */

/* This program is free software. It comes without any warranty, to the
 * extent permitted by applicable law. You can redistribute it and/or
 * modify it under the terms of the Do What The Fuck You Want To Public
 * License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */

// todo: Reimplement over fixed point?
// todo: It's full of dangerous type conversions and places for range violations, use safe math
//       where needed.

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct context_s;
typedef struct context_s xm_context_t;

/** Create a XM context.
 *
 * @param moddata the contents of the module
 * @param moddata_length the length of the contents of the module, in bytes
 * @param rate play rate in Hz, recommended value of 48000
 *
 * @returns 0 on success
 * @returns 1 if module data is not sane
 * @returns 2 if memory allocation failed
 */
int xm_create_context_safe(
    xm_context_t **, char const *moddata, size_t moddata_length, uint32_t rate);

/** Free a XM context created by create_context(). */
void xm_free_context(xm_context_t *);

/** Play the module and put the sound samples in an output buffer.
 *
 * @param output buffer of 2*numsamples elements
 * @param numsamples number of samples to generate
 */
void xm_generate_samples(xm_context_t *, float *output, size_t numsamples);

/** Set the maximum number of times a module can loop. After the
 * specified number of loops, calls to xm_generate_samples will only
 * generate silence. You can control the current number of loops with
 * xm_get_loop_count().
 *
 * @param loopcnt maximum number of loops. Use 0 to loop
 * indefinitely. */
void xm_set_max_loop_count(xm_context_t *, uint8_t loopcnt);

/** Get the loop count of the currently playing module. This value is
 * 0 when the module is still playing, 1 when the module has looped
 * once, etc. */
uint8_t xm_get_loop_count(xm_context_t *);

/** xm_Seek to a specific position in a module.
 *
 * WARNING, WITH BIG LETTERS: xm_seeking modules is broken by design,
 * don't expect miracles.
 */
void xm_seek(xm_context_t *, uint8_t pot, uint8_t row, uint16_t tick);

/** Mute or unmute a channel.
 *
 * @note Channel numbers go from 1 to xm_get_number_of_channels(...).
 *
 * @return whether the channel was muted.
 */
bool xm_mute_channel(xm_context_t *, uint16_t, bool);

/** Mute or unmute an instrument.
 *
 * @note Instrument numbers go from 1 to
 * xm_get_number_of_instruments(...).
 *
 * @return whether the instrument was muted.
 */
bool xm_mute_instrument(xm_context_t *, uint16_t, bool);

/** Get the module name as a NUL-terminated string. */
char const *xm_get_module_name(xm_context_t *);

/** Get the tracker name as a NUL-terminated string. */
char const *xm_get_tracker_name(xm_context_t *);

/** Get the number of channels. */
uint16_t xm_get_number_of_channels(xm_context_t *);

/** Get the module length (in patterns). */
uint16_t xm_get_module_length(xm_context_t *);

/** Get the number of patterns. */
uint16_t xm_get_number_of_patterns(xm_context_t *);

/** Get the number of rows of a pattern.
 *
 * @note Pattern numbers go from 0 to
 * xm_get_number_of_patterns(...)-1.
 */
uint16_t xm_get_number_of_rows(xm_context_t *, uint16_t);

/** Get the number of instruments. */
uint16_t xm_get_number_of_instruments(xm_context_t *);

/** Get the number of samples of an instrument.
 *
 * @note Instrument numbers go from 1 to
 * xm_get_number_of_instruments(...).
 */
uint16_t xm_get_number_of_samples(xm_context_t *, uint16_t);

/** Get the internal buffer for a given sample waveform.
 *
 * This buffer can be read from or written to, at any time, but the
 * length cannot change. The buffer must be cast to (int8_t*) or
 * (int16_t*) depending on the sample type.
 *
 * @note Instrument numbers go from 1 to
 * xm_get_number_of_instruments(...).
 *
 * @note Sample numbers go from 0 to
 * get_nubmer_of_samples(...,instr)-1.
 */
void *xm_get_sample_waveform(
    xm_context_t *, uint16_t instr, uint16_t sample, size_t *length, uint8_t *bits);

/** Get the current module speed.
 *
 * @param bpm will receive the current BPM
 * @param tempo will receive the current tempo (ticks per line)
 */
void xm_get_playing_speed(xm_context_t *, uint16_t *bpm, uint16_t *tempo);

/** Get the current position in the module being played.
 *
 * @param pattern_index if not NULL, will receive the current pattern
 * index in the POT (pattern order table)
 *
 * @param pattern if not NULL, will receive the current pattern number
 *
 * @param row if not NULL, will receive the current row
 *
 * @param samples if not NULL, will receive the total number of
 * generated samples (divide by sample rate to get seconds of
 * generated audio)
 */
void xm_get_position(
    xm_context_t *, uint8_t *pattern_index, uint8_t *pattern, uint8_t *row, uint64_t *samples);

/** Get the latest time (in number of generated samples) when a
 * particular instrument was triggered in any channel.
 *
 * @note Instrument numbers go from 1 to
 * xm_get_number_of_instruments(...).
 */
uint64_t xm_get_latest_trigger_of_instrument(xm_context_t *, uint16_t);

/** Get the latest time (in number of generated samples) when a
 * particular sample was triggered in any channel.
 *
 * @note Instrument numbers go from 1 to
 * xm_get_number_of_instruments(...).
 *
 * @note Sample numbers go from 0 to
 * get_nubmer_of_samples(...,instr)-1.
 */
uint64_t xm_get_latest_trigger_of_sample(xm_context_t *, uint16_t instr, uint16_t sample);

/** Get the latest time (in number of generated samples) when any
 * instrument was triggered in a given channel.
 *
 * @note Channel numbers go from 1 to xm_get_number_of_channels(...).
 */
uint64_t xm_get_latest_trigger_of_channel(xm_context_t *, uint16_t);

/** Checks whether a channel is active (ie: is playing something).
 *
 * @note Channel numbers go from 1 to xm_get_number_of_channels(...).
 */
bool xm_is_channel_active(xm_context_t *, uint16_t);

/** Get the instrument number currently playing in a channel.
 *
 * @returns instrument number, or 0 if channel is not active.
 *
 * @note Channel numbers go from 1 to xm_get_number_of_channels(...).
 *
 * @note Instrument numbers go from 1 to
 * xm_get_number_of_instruments(...).
 */
uint16_t xm_get_instrument_of_channel(xm_context_t *, uint16_t);

/** Get the frequency of the sample currently playing in a channel.
 *
 * @returns a frequency in Hz. If the channel is not active, return
 * value is undefined.
 *
 * @note Channel numbers go from 1 to xm_get_number_of_channels(...).
 */
float xm_get_frequency_of_channel(xm_context_t *, uint16_t);

/** Get the volume of the sample currently playing in a channel. This
 * takes into account envelopes, etc.
 *
 * @returns a volume between 0 or 1. If the channel is not active,
 * return value is undefined.
 *
 * @note Channel numbers go from 1 to xm_get_number_of_channels(...).
 */
float xm_get_volume_of_channel(xm_context_t *, uint16_t);

/** Get the panning of the sample currently playing in a channel. This
 * takes into account envelopes, etc.
 *
 * @returns a panning between 0 (L) and 1 (R). If the channel is not
 * active, return value is undefined.
 *
 * @note Channel numbers go from 1 to xm_get_number_of_channels(...).
 */
float xm_get_panning_of_channel(xm_context_t *, uint16_t);

#if defined(IT_IMPLEMENT_LIBXM)

#include <inttypes.h>
#include <math.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/memory.h>

#include <ilotawa/include/private/ilotawa.h>

#if defined(IT_BUILD_DEBUG)
#define XM_IS_DEBUG 1
#define XM_DEBUG(fmt, ...)                                                                         \
    do {                                                                                           \
        it_log("%s(): " fmt "\n", __func__, __VA_ARGS__);                                          \
    } while (0)
#else
#define XM_IS_DEBUG 0
#define XM_DEBUG(...)
#endif

#if !defined(XM_LINEAR_INTERPOLATION)
#define XM_LINEAR_INTERPOLATION 0
#endif

#if !defined(XM_DEFENSIVE)
#define XM_DEFENSIVE 1
#endif

#if !defined(XM_RAMPING)
#define XM_RAMPING 0
#endif

#if !defined(XM_STRINGS)
#define XM_STRINGS 0
#endif

#if !defined(XM_BIG_ENDIAN)
#define XM_BIG_ENDIAN 0
#endif

#if XM_BIG_ENDIAN
#error "Big endian platforms are not yet supported, sorry"
/* Make sure the compiler stops, even if #error is ignored */
extern int __fail[-1];
#endif

/* ----- XM constants ----- */

#define XM_SAMPLE_NAME_LENGTH         22
#define XM_INSTRUMENT_HEADER_LENGTH   263
#define XM_INSTRUMENT_NAME_LENGTH     22
#define XM_MODULE_NAME_LENGTH         20
#define XM_TRACKER_NAME_LENGTH        20
#define XM_PATTERN_ORDER_TABLE_LENGTH 256
#define XM_NUM_NOTES                  96
#define XM_NUM_ENVELOPE_POINTS        12
#define XM_MAX_NUM_ROWS               256
#define XM_DEFAULT_PATTERN_LENGTH     64

#if XM_RAMPING
#define XM_SAMPLE_RAMPING_POINTS 0x20
#endif

/* ----- Data types ----- */

enum waveform_type_e {
    SINE_WAVEFORM = 0,
    RAMP_DOWN_WAVEFORM = 1,
    SQUARE_WAVEFORM = 2,
    RANDOM_WAVEFORM = 3,
    RAMP_UP_WAVEFORM = 4,
};
typedef enum waveform_type_e waveform_type_t;

enum loop_type_e {
    NO_LOOP,
    FORWARD_LOOP,
    PING_PONG_LOOP,
};
typedef enum loop_type_e loop_type_t;

enum frequency_type_e {
    LINEAR_FREQUENCIES,
    AMIGA_FREQUENCIES,
};
typedef enum frequency_type_e frequency_type_t;

struct envelope_point_s {
    uint16_t frame;
    uint16_t value;
};
typedef struct envelope_point_s envelope_point_t;

struct envelope_s {
    envelope_point_t points[XM_NUM_ENVELOPE_POINTS];
    uint8_t num_points;
    uint8_t sustain_point;
    uint8_t loop_start_point;
    uint8_t loop_end_point;
    bool enabled;
    bool sustain_enabled;
    bool loop_enabled;
};
typedef struct envelope_s envelope_t;

struct sample_s {
#if XM_STRINGS
    char name[XM_SAMPLE_NAME_LENGTH + 1];
#endif
    uint8_t bits; /* Either 8 or 16 */

    uint32_t length;
    uint32_t loop_start;
    uint32_t loop_length;
    uint32_t loop_end;
    float volume;
    int8_t finetune;
    loop_type_t loop_type;
    float panning;
    int8_t relative_note;
    uint64_t latest_trigger;

    union {
        int8_t *data8;
        int16_t *data16;
    };
};
typedef struct sample_s sample_t;

struct instrument_s {
#if XM_STRINGS
    char name[XM_INSTRUMENT_NAME_LENGTH + 1];
#endif
    uint16_t num_samples;
    uint8_t sample_of_notes[XM_NUM_NOTES];
    envelope_t volume_envelope;
    envelope_t panning_envelope;
    waveform_type_t vibrato_type;
    uint8_t vibrato_sweep;
    uint8_t vibrato_depth;
    uint8_t vibrato_rate;
    uint16_t volume_fadeout;
    uint64_t latest_trigger;
    bool muted;

    sample_t *samples;
};
typedef struct instrument_s instrument_t;

struct pattern_slot_s {
    uint8_t note;       /* 1-96, 97 = Key Off note */
    uint8_t instrument; /* 1-128 */
    uint8_t volume_column;
    uint8_t effect_type;
    uint8_t effect_param;
};
typedef struct pattern_slot_s pattern_slot_t;

struct pattern_s {
    uint16_t num_rows;
    pattern_slot_t *slots; /* Array of size num_rows * num_channels */
};
typedef struct pattern_s pattern_t;

struct module_s {
#if XM_STRINGS
    char name[XM_MODULE_NAME_LENGTH + 1];
    char trackername[XM_TRACKER_NAME_LENGTH + 1];
#endif
    uint16_t length;
    uint16_t restart_position;
    uint16_t num_channels;
    uint16_t num_patterns;
    uint16_t num_instruments;
    frequency_type_t frequency_type;
    uint8_t pattern_table[XM_PATTERN_ORDER_TABLE_LENGTH];

    pattern_t *patterns;
    instrument_t *instruments; /* Instrument 1 has index 0,
                                * instrument 2 has index 1, etc. */
};
typedef struct module_s module_t;

struct channel_context_s {
    float note;
    float orig_note; /* The original note before effect modifications, as read in the pattern. */
    instrument_t *instrument; /* Could be NULL */
    sample_t *sample;         /* Could be NULL */
    pattern_slot_t *current;

    float sample_position;
    float period;
    float frequency;
    float step;
    bool ping; /* For ping-pong samples: true is -->, false is <-- */

    float volume;  /* Ideally between 0 (muted) and 1 (loudest) */
    float panning; /* Between 0 (left) and 1 (right); 0.5 is centered */

    uint16_t autovibrato_ticks;

    bool sustained;
    float fadeout_volume;
    float volume_envelope_volume;
    float panning_envelope_panning;
    uint16_t volume_envelope_frame_count;
    uint16_t panning_envelope_frame_count;

    float autovibrato_note_offset;

    bool arp_in_progress;
    uint8_t arp_note_offset;
    uint8_t volume_slide_param;
    uint8_t fine_volume_slide_param;
    uint8_t global_volume_slide_param;
    uint8_t panning_slide_param;
    uint8_t portamento_up_param;
    uint8_t portamento_down_param;
    uint8_t fine_portamento_up_param;
    uint8_t fine_portamento_down_param;
    uint8_t extra_fine_portamento_up_param;
    uint8_t extra_fine_portamento_down_param;
    uint8_t tone_portamento_param;
    float tone_portamento_target_period;
    uint8_t multi_retrig_param;
    uint8_t note_delay_param;
    uint8_t pattern_loop_origin; /* Where to restart a E6y loop */
    uint8_t pattern_loop_count;  /* How many loop passes have been done */
    bool vibrato_in_progress;
    waveform_type_t vibrato_waveform;
    bool vibrato_waveform_retrigger; /* True if a new note retriggers the waveform */
    uint8_t vibrato_param;
    uint16_t vibrato_ticks; /* Position in the waveform */
    float vibrato_note_offset;
    waveform_type_t tremolo_waveform;
    bool tremolo_waveform_retrigger;
    uint8_t tremolo_param;
    uint8_t tremolo_ticks;
    float tremolo_volume;
    uint8_t tremor_param;
    bool tremor_on;

    uint64_t latest_trigger;
    bool muted;

#if XM_RAMPING
    /* These values are updated at the end of each tick, to save
     * a couple of float operations on every generated sample. */
    float target_volume[2];

    unsigned long frame_count;
    float end_of_previous_sample[XM_SAMPLE_RAMPING_POINTS];
#endif

    float actual_volume[2];
};
typedef struct channel_context_s channel_context_t;

struct context_s {
    size_t ctx_size; /* Must be first, see create_context_from_libxmize() */
    module_t module;
    uint32_t rate;

    uint16_t tempo;
    uint16_t bpm;
    float global_volume;
    float amplification;

#if XM_RAMPING
    /* How much is a channel final volume allowed to change per
     * sample; this is used to avoid abrubt volume changes which
     * manifest as "clicks" in the generated sound. */
    float volume_ramp;
#endif

    uint8_t current_table_index;
    uint8_t current_row;
    uint16_t current_tick; /* Can go below 255, with high tempo and a pattern delay */
    float remaining_samples_in_tick;
    uint64_t generated_samples;

    bool position_jump;
    bool pattern_break;
    uint8_t jump_dest;
    uint8_t jump_row;

    /* Extra ticks to be played before going to the next row -
     * Used for EEy effect */
    uint16_t extra_ticks;

    uint8_t *row_loop_count; /* Array of size XM_MAX_NUM_ROWS * module_length */
    uint8_t loop_count;
    uint8_t max_loop_count;

    channel_context_t *channels;
};

/* ----- Internal API ----- */

/** Check the module data for errors/inconsistencies.
 *
 * @returns 0 if everything looks OK. Module should be safe to load.
 */
int check_sanity_preload(char const *, size_t);

/** Check a loaded module for errors/inconsistencies.
 *
 * @returns 0 if everything looks OK.
 */
int check_sanity_postload(xm_context_t *);

/** Get the number of bytes needed to store the module data in a
 * dynamically allocated blank context.
 *
 * Things that are dynamically allocated:
 * - sample data
 * - sample structures in instruments
 * - pattern data
 * - row loop count arrays
 * - pattern structures in module
 * - instrument structures in module
 * - channel contexts
 * - context structure itself
 * @returns 0 if everything looks OK.
 */
size_t get_memory_needed_for_context(char const *, size_t);

/** Populate the context from module data.
 *
 * @returns pointer to the memory pool
 */
char *load_module(xm_context_t *, char const *, size_t, char *);

static float waveform(waveform_type_t, uint8_t);
static void autovibrato(xm_context_t *, channel_context_t *);
static void vibrato(xm_context_t *, channel_context_t *, uint8_t);
static void tremolo(xm_context_t *, channel_context_t *, uint8_t, uint16_t);
static void arpeggio(xm_context_t *, channel_context_t *, uint8_t, uint16_t);
static void tone_portamento(xm_context_t *, channel_context_t *);
static void pitch_slide(xm_context_t *, channel_context_t *, float);
static void panning_slide(channel_context_t *, uint8_t);
static void volume_slide(channel_context_t *, uint8_t);

static float envelope_lerp(envelope_point_t *, envelope_point_t *, uint16_t);
static void envelope_tick(channel_context_t *, envelope_t *, uint16_t *, float *);
static void envelopes(channel_context_t *);

static float linear_period(float);
static float linear_frequency(float);
static float amiga_period(float);
static float amiga_frequency(float);
static float period(xm_context_t *, float);
static float frequency(xm_context_t *, float, float, float);
static void update_frequency(xm_context_t *, channel_context_t *);

static void handle_note_and_instrument(xm_context_t *, channel_context_t *, pattern_slot_t *);
static void trigger_note(xm_context_t *, channel_context_t *, unsigned int flags);
static void cut_note(channel_context_t *);
static void key_off(channel_context_t *);

static void post_pattern_change(xm_context_t *);
static void row(xm_context_t *);
static void tick(xm_context_t *);

static float sample_at(sample_t *, size_t);
static float next_of_sample(channel_context_t *);
static void sample(xm_context_t *, float *, float *);

/* ----- Other oddities ----- */

#define XM_TRIGGER_KEEP_VOLUME          (1 << 0)
#define XM_TRIGGER_KEEP_PERIOD          (1 << 1)
#define XM_TRIGGER_KEEP_SAMPLE_POSITION (1 << 2)
#define XM_TRIGGER_KEEP_ENVELOPE        (1 << 3)

#define XM_AMIGA_FREQ_SCALE 1024

static const uint32_t amiga_frequencies[] = {
    1712 * XM_AMIGA_FREQ_SCALE, 1616 * XM_AMIGA_FREQ_SCALE, 1525 * XM_AMIGA_FREQ_SCALE,
    1440 * XM_AMIGA_FREQ_SCALE, /* C-2, C#2, D-2, D#2 */
    1357 * XM_AMIGA_FREQ_SCALE, 1281 * XM_AMIGA_FREQ_SCALE, 1209 * XM_AMIGA_FREQ_SCALE,
    1141 * XM_AMIGA_FREQ_SCALE, /* E-2, F-2, F#2, G-2 */
    1077 * XM_AMIGA_FREQ_SCALE, 1017 * XM_AMIGA_FREQ_SCALE, 961 * XM_AMIGA_FREQ_SCALE,
    907 * XM_AMIGA_FREQ_SCALE, /* G#2, A-2, A#2, B-2 */
    856 * XM_AMIGA_FREQ_SCALE, /* C-3 */
};

static float const multi_retrig_add[] = {
    0.f, -1.f, -2.f, -4.f, /* 0, 1, 2, 3 */
    -8.f, -16.f, 0.f, 0.f, /* 4, 5, 6, 7 */
    0.f, 1.f, 2.f, 4.f,    /* 8, 9, A, B */
    8.f, 16.f, 0.f, 0.f    /* C, D, E, F */
};

static float const multi_retrig_multiply[] = {
    1.f, 1.f, 1.f, 1.f,       /* 0, 1, 2, 3 */
    1.f, 1.f, .6666667f, .5f, /* 4, 5, 6, 7 */
    1.f, 1.f, 1.f, 1.f,       /* 8, 9, A, B */
    1.f, 1.f, 1.5f, 2.f       /* C, D, E, F */
};

#define XM_CLAMP_UP1F(vol, limit)                                                                  \
    do {                                                                                           \
        if ((vol) > (limit))                                                                       \
            (vol) = (limit);                                                                       \
    } while (0)
#define XM_CLAMP_UP(vol) XM_CLAMP_UP1F((vol), 1.f)

#define XM_CLAMP_DOWN1F(vol, limit)                                                                \
    do {                                                                                           \
        if ((vol) < (limit))                                                                       \
            (vol) = (limit);                                                                       \
    } while (0)
#define XM_CLAMP_DOWN(vol) XM_CLAMP_DOWN1F((vol), .0f)

#define XM_CLAMP2F(vol, up, down)                                                                  \
    do {                                                                                           \
        if ((vol) > (up))                                                                          \
            (vol) = (up);                                                                          \
        else if ((vol) < (down))                                                                   \
            (vol) = (down);                                                                        \
    } while (0)
#define XM_CLAMP(vol) XM_CLAMP2F((vol), 1.f, .0f)

#define XM_SLIDE_TOWARDS(val, goal, incr)                                                          \
    do {                                                                                           \
        if ((val) > (goal)) {                                                                      \
            (val) -= (incr);                                                                       \
            XM_CLAMP_DOWN1F((val), (goal));                                                        \
        } else if ((val) < (goal)) {                                                               \
            (val) += (incr);                                                                       \
            XM_CLAMP_UP1F((val), (goal));                                                          \
        }                                                                                          \
    } while (0)

#define XM_LERP(u, v, t)            ((u) + (t) * ((v) - (u)))
#define XM_INVERSE_LERP(u, v, lerp) (((lerp) - (u)) / ((v) - (u)))

#define XM_HAS_TONE_PORTAMENTO(s)                                                                  \
    ((s)->effect_type == 3 || (s)->effect_type == 5 || ((s)->volume_column >> 4) == 0xF)
#define XM_HAS_ARPEGGIO(s) ((s)->effect_type == 0 && (s)->effect_param != 0)
#define XM_HAS_VIBRATO(s)                                                                          \
    ((s)->effect_type == 4 || (s)->effect_type == 6 || ((s)->volume_column >> 4) == 0xB)
#define XM_NOTE_IS_VALID(n) ((n) > 0 && (n) < 97)

/* ----- Function definitions ----- */

static float waveform(waveform_type_t waveform, uint8_t step) {
    static unsigned int next_rand = 24492;
    step %= 0x40;

    switch (waveform) {

    case SINE_WAVEFORM:
        /* Why not use a table? For saving space, and because there's
         * very very little actual performance gain. */
        return -sinf(2.f * 3.141592f * (float)step / (float)0x40);

    case RAMP_DOWN_WAVEFORM:
        /* Ramp down: 1.0f when step = 0; -1.0f when step = 0x40 */
        return (float)(0x20 - step) / 0x20;

    case SQUARE_WAVEFORM:
        /* Square with a 50% duty */
        return (step >= 0x20) ? 1.f : -1.f;

    case RANDOM_WAVEFORM:
        /* Use the POSIX.1-2001 example, just to be deterministic
         * across different machines */
        next_rand = next_rand * 1103515245 + 12345;
        return (float)((next_rand >> 16) & 0x7FFF) / (float)0x4000 - 1.f;

    case RAMP_UP_WAVEFORM:
        /* Ramp up: -1.f when step = 0; 1.f when step = 0x40 */
        return (float)(step - 0x20) / 0x20;

    default:
        break;
    }

    return .0f;
}

static void autovibrato(xm_context_t *ctx, channel_context_t *ch) {
    if (ch->instrument == NULL || ch->instrument->vibrato_depth == 0) {
        if ((bool)ch->autovibrato_note_offset) {
            ch->autovibrato_note_offset = 0.f;
            update_frequency(ctx, ch);
        }
        return;
    }
    instrument_t *instr = ch->instrument;
    float sweep = 1.f;

    if (ch->autovibrato_ticks < instr->vibrato_sweep) {
        /* No idea if this is correct, but it sounds close enough… */
        sweep = XM_LERP(0.f, 1.f, (float)ch->autovibrato_ticks / (float)instr->vibrato_sweep);
    }

    unsigned int step = ((ch->autovibrato_ticks++) * instr->vibrato_rate) >> 2;
    ch->autovibrato_note_offset = .25f * waveform(instr->vibrato_type, (uint8_t)step)
        * (float)instr->vibrato_depth / (float)0xF * sweep;
    update_frequency(ctx, ch);
}

static void vibrato(xm_context_t *ctx, channel_context_t *ch, uint8_t param) {
    ch->vibrato_ticks += (param >> 4);
    ch->vibrato_note_offset = -2.f * waveform(ch->vibrato_waveform, (uint8_t)ch->vibrato_ticks)
        * (float)(param & 0x0F) / (float)0xF;
    update_frequency(ctx, ch);
}

static void tremolo(xm_context_t *ctx, channel_context_t *ch, uint8_t param, uint16_t pos) {
    (void)ctx;
    unsigned int step = pos * (param >> 4);
    /* Not so sure about this, it sounds correct by ear compared with
     * MilkyTracker, but it could come from other bugs */
    ch->tremolo_volume
        = -1.f * waveform(ch->tremolo_waveform, (uint8_t)step) * (float)(param & 0x0F) / (float)0xF;
}

static void arpeggio(xm_context_t *ctx, channel_context_t *ch, uint8_t param, uint16_t tick) {
    switch (tick % 3) {
    case 0:
        ch->arp_in_progress = false;
        ch->arp_note_offset = 0;
        break;
    case 2:
        ch->arp_in_progress = true;
        ch->arp_note_offset = param >> 4;
        break;
    case 1:
        ch->arp_in_progress = true;
        ch->arp_note_offset = param & 0x0F;
        break;
    }

    update_frequency(ctx, ch);
}

static void tone_portamento(xm_context_t *ctx, channel_context_t *ch) {
    /* 3xx called without a note, wait until we get an actual
     * target note. */
    if (ch->tone_portamento_target_period == 0.f)
        return;

    if (fabsf(ch->period - ch->tone_portamento_target_period) < FLT_EPSILON) {
        XM_SLIDE_TOWARDS(ch->period, ch->tone_portamento_target_period,
            (ctx->module.frequency_type == LINEAR_FREQUENCIES ? 4.f : 1.f)
                * ch->tone_portamento_param);
        update_frequency(ctx, ch);
    }
}

static void pitch_slide(xm_context_t *ctx, channel_context_t *ch, float period_offset) {
    /* Don't ask about the 4.f coefficient. I found mention of it
     * nowhere. Found by ear™. */
    if (ctx->module.frequency_type == LINEAR_FREQUENCIES) {
        period_offset *= 4.f;
    }

    ch->period += period_offset;
    XM_CLAMP_DOWN(ch->period);
    /* XXX: upper bound of period ? */

    update_frequency(ctx, ch);
}

static void panning_slide(channel_context_t *ch, uint8_t rawval) {
    float f;

    if ((rawval & 0xF0) && (rawval & 0x0F)) {
        /* Illegal state */
        return;
    }

    if (rawval & 0xF0) {
        /* Slide right */
        f = (float)(rawval >> 4) / (float)0xFF;
        ch->panning += f;
        XM_CLAMP_UP(ch->panning);
    } else {
        /* Slide left */
        f = (float)(rawval & 0x0F) / (float)0xFF;
        ch->panning -= f;
        XM_CLAMP_DOWN(ch->panning);
    }
}

static void volume_slide(channel_context_t *ch, uint8_t rawval) {
    float f;

    if ((rawval & 0xF0) && (rawval & 0x0F)) {
        /* Illegal state */
        return;
    }

    if (rawval & 0xF0) {
        /* Slide up */
        f = (float)(rawval >> 4) / (float)0x40;
        ch->volume += f;
        XM_CLAMP_UP(ch->volume);
    } else {
        /* Slide down */
        f = (float)(rawval & 0x0F) / (float)0x40;
        ch->volume -= f;
        XM_CLAMP_DOWN(ch->volume);
    }
}

static float envelope_lerp(envelope_point_t *a, envelope_point_t *b, uint16_t pos) {
    /* Linear interpolation between two envelope points */
    if (pos <= a->frame)
        return a->value;
    else if (pos >= b->frame)
        return b->value;
    else {
        float p = (float)(pos - a->frame) / (float)(b->frame - a->frame);
        return a->value * (1 - p) + b->value * p;
    }
}

static void post_pattern_change(xm_context_t *ctx) {
    /* Loop if necessary */
    if (ctx->current_table_index >= ctx->module.length) {
        ctx->current_table_index = (uint8_t)ctx->module.restart_position;
    }
}

static float linear_period(float note) { return 7680.f - note * 64.f; }

static float linear_frequency(float period) {
    return 8363.f * powf(2.f, (4608.f - period) / 768.f);
}

static float amiga_period(float note) {
    unsigned int intnote = (unsigned int)note;
    uint8_t a = intnote % 12;
    int8_t octave = (int8_t)(note / 12.f - 2);
    int32_t p1 = (int32_t)amiga_frequencies[a], p2 = (int32_t)amiga_frequencies[a + 1];

    if (octave > 0) {
        p1 >>= octave;
        p2 >>= octave;
    } else if (octave < 0) {
        p1 <<= (-octave);
        p2 <<= (-octave);
    }

    return XM_LERP((float)p1, (float)p2, note - (float)intnote) / XM_AMIGA_FREQ_SCALE;
}

static float amiga_frequency(float period) {
    if (period == .0f)
        return .0f;

    /* This is the PAL value. No reason to choose this one over the
     * NTSC value. */
    return 7093789.2f / (period * 2.f);
}

static float period(xm_context_t *ctx, float note) {
    switch (ctx->module.frequency_type) {
    case LINEAR_FREQUENCIES:
        return linear_period(note);
    case AMIGA_FREQUENCIES:
        return amiga_period(note);
    }
    return .0f;
}

static float frequency(xm_context_t *ctx, float period, float note_offset, float period_offset) {
    uint8_t a;
    int8_t octave;
    float note;
    int32_t p1 = 0, p2 = 0;

    switch (ctx->module.frequency_type) {

    case LINEAR_FREQUENCIES:
        return linear_frequency(period - 64.f * note_offset - 16.f * period_offset);

    case AMIGA_FREQUENCIES:
        if (note_offset == 0.f) {
            /* A chance to escape from insanity */
            return amiga_frequency(period + 16.f * period_offset);
        }

        /* FIXME: this is very crappy at best */
        a = octave = 0;

        /* Find the octave of the current period */
        period *= XM_AMIGA_FREQ_SCALE;
        if (period > (float)amiga_frequencies[0]) {
            --octave;
            while (period > (float)(amiga_frequencies[0] << (-octave)))
                --octave;
        } else if (period < (float)amiga_frequencies[12]) {
            ++octave;
            while (period < (float)(amiga_frequencies[12] >> octave))
                ++octave;
        }

        /* Find the smallest note closest to the current period */
        for (uint8_t i = 0; i < 12; ++i) {
            p1 = (int32_t)amiga_frequencies[i];
            p2 = (int32_t)amiga_frequencies[i + 1];

            if (octave > 0) {
                p1 >>= octave;
                p2 >>= octave;
            } else if (octave < 0) {
                p1 <<= (-octave);
                p2 <<= (-octave);
            }

            if ((float)p2 <= period && period <= (float)p1) {
                a = i;
                break;
            }
        }

        if (XM_IS_DEBUG && ((float)p1 < period || (float)p2 > period)) {
            XM_DEBUG("%" PRId32 " <= %f <= %" PRId32 " should hold but doesn't, this is a bug", p2,
                (double)period, p1);
        }

        note = 12.f * (octave + 2) + a + XM_INVERSE_LERP((float)p1, (float)p2, period);

        return amiga_frequency(amiga_period(note + note_offset) + 16.f * period_offset);
    }

    return .0f;
}

static void update_frequency(xm_context_t *ctx, channel_context_t *ch) {
    ch->frequency = frequency(ctx, ch->period, ch->arp_note_offset,
        ch->vibrato_note_offset + ch->autovibrato_note_offset);
    ch->step = ch->frequency / (float)ctx->rate;
}

static void handle_note_and_instrument(
    xm_context_t *ctx, channel_context_t *ch, pattern_slot_t *s) {
    if (s->instrument > 0) {
        if (XM_HAS_TONE_PORTAMENTO(ch->current) && ch->instrument != NULL && ch->sample != NULL) {
            /* Tone portamento in effect, unclear stuff happens */
            trigger_note(ctx, ch, XM_TRIGGER_KEEP_PERIOD | XM_TRIGGER_KEEP_SAMPLE_POSITION);
        } else if (s->note == 0 && ch->sample != NULL) {
            /* Ghost instrument, trigger note */
            /* Sample position is kept, but envelopes are reset */
            trigger_note(ctx, ch, XM_TRIGGER_KEEP_SAMPLE_POSITION);
        } else if (s->instrument > ctx->module.num_instruments) {
            /* Invalid instrument, Cut current note */
            cut_note(ch);
            ch->instrument = NULL;
            ch->sample = NULL;
        } else {
            ch->instrument = ctx->module.instruments + (s->instrument - 1);
        }
    }

    if (XM_NOTE_IS_VALID(s->note)) {
        /* Yes, the real note number is s->note -1. Try finding
         * THAT in any of the specs! :-) */

        instrument_t *instr = ch->instrument;

        if (XM_HAS_TONE_PORTAMENTO(ch->current) && instr != NULL && ch->sample != NULL) {
            /* Tone portamento in effect */
            ch->note = s->note + ch->sample->relative_note + ch->sample->finetune / 128.f - 1.f;
            ch->tone_portamento_target_period = period(ctx, ch->note);
        } else if (instr == NULL || ch->instrument->num_samples == 0) {
            /* Bad instrument */
            cut_note(ch);
        } else {
            if (instr->sample_of_notes[s->note - 1] < instr->num_samples) {
#if XM_RAMPING
                for (unsigned int z = 0; z < XM_SAMPLE_RAMPING_POINTS; ++z) {
                    ch->end_of_previous_sample[z] = next_of_sample(ch);
                }
                ch->frame_count = 0;
#endif
                ch->sample = instr->samples + instr->sample_of_notes[s->note - 1];
                ch->orig_note = ch->note
                    = s->note + ch->sample->relative_note + ch->sample->finetune / 128.f - 1.f;
                if (s->instrument > 0) {
                    trigger_note(ctx, ch, 0);
                } else {
                    /* Ghost note: keep old volume */
                    trigger_note(ctx, ch, XM_TRIGGER_KEEP_VOLUME);
                }
            } else {
                /* Bad sample */
                cut_note(ch);
            }
        }
    } else if (s->note == 97) {
        /* Key Off */
        key_off(ch);
    }

    switch (s->volume_column >> 4) {

    case 0x5:
        if (s->volume_column > 0x50)
            break;
        IT_FALLTHROUGH;
    case 0x1:
        IT_FALLTHROUGH;
    case 0x2:
        IT_FALLTHROUGH;
    case 0x3:
        IT_FALLTHROUGH;
    case 0x4:
        /* Set volume */
        ch->volume = (float)(s->volume_column - 0x10) / (float)0x40;
        break;

    case 0x8: /* Fine volume slide down */
        volume_slide(ch, s->volume_column & 0x0F);
        break;

    case 0x9: /* Fine volume slide up */
        volume_slide(ch, (uint8_t)(s->volume_column << 4));
        break;

    case 0xA: /* Set vibrato speed */
        ch->vibrato_param
            = (uint8_t)((ch->vibrato_param & 0x0F) | ((s->volume_column & 0x0F) << 4));
        break;

    case 0xC: /* Set panning */
        ch->panning
            = (float)(((s->volume_column & 0x0F) << 4) | (s->volume_column & 0x0F)) / (float)0xFF;
        break;

    case 0xF: /* Tone portamento */
        if (s->volume_column & 0x0F) {
            ch->tone_portamento_param
                = (uint8_t)((s->volume_column & 0x0F) << 4) | (s->volume_column & 0x0F);
        }
        break;

    default:
        break;
    }

    switch (s->effect_type) {

    case 1: /* 1xx: Portamento up */
        if (s->effect_param > 0) {
            ch->portamento_up_param = s->effect_param;
        }
        break;

    case 2: /* 2xx: Portamento down */
        if (s->effect_param > 0) {
            ch->portamento_down_param = s->effect_param;
        }
        break;

    case 3: /* 3xx: Tone portamento */
        if (s->effect_param > 0) {
            ch->tone_portamento_param = s->effect_param;
        }
        break;

    case 4: /* 4xy: Vibrato */
        if (s->effect_param & 0x0F) {
            /* Set vibrato depth */
            ch->vibrato_param = (ch->vibrato_param & 0xF0) | (s->effect_param & 0x0F);
        }
        if (s->effect_param >> 4) {
            /* Set vibrato speed */
            ch->vibrato_param = (s->effect_param & 0xF0) | (ch->vibrato_param & 0x0F);
        }
        break;

    case 5: /* 5xy: Tone portamento + Volume slide */
        if (s->effect_param > 0) {
            ch->volume_slide_param = s->effect_param;
        }
        break;

    case 6: /* 6xy: Vibrato + Volume slide */
        if (s->effect_param > 0) {
            ch->volume_slide_param = s->effect_param;
        }
        break;

    case 7: /* 7xy: Tremolo */
        if (s->effect_param & 0x0F) {
            /* Set tremolo depth */
            ch->tremolo_param = (ch->tremolo_param & 0xF0) | (s->effect_param & 0x0F);
        }
        if (s->effect_param >> 4) {
            /* Set tremolo speed */
            ch->tremolo_param = (s->effect_param & 0xF0) | (ch->tremolo_param & 0x0F);
        }
        break;

    case 8: /* 8xx: Set panning */
        ch->panning = (float)s->effect_param / (float)0xFF;
        break;

    case 9: /* 9xx: Sample offset */
        if (ch->sample != NULL && XM_NOTE_IS_VALID(s->note)) {
            uint32_t final_offset = (uint32_t)(s->effect_param << (ch->sample->bits == 16 ? 7 : 8));
            if (final_offset >= ch->sample->length) {
                /* Pretend the sample dosen't loop and is done playing */
                ch->sample_position = -1;
                break;
            }
            ch->sample_position = (float)final_offset;
        }
        break;

    case 0xA: /* Axy: Volume slide */
        if (s->effect_param > 0) {
            ch->volume_slide_param = s->effect_param;
        }
        break;

    case 0xB: /* Bxx: Position jump */
        if (s->effect_param < ctx->module.length) {
            ctx->position_jump = true;
            ctx->jump_dest = s->effect_param;
            ctx->jump_row = 0;
        }
        break;

    case 0xC: /* Cxx: Set volume */
        ch->volume = (float)((s->effect_param > 0x40) ? 0x40 : s->effect_param) / (float)0x40;
        break;

    case 0xD: /* Dxx: Pattern break */
        /* Jump after playing this line */
        ctx->pattern_break = true;
        ctx->jump_row = (s->effect_param >> 4) * 10 + (s->effect_param & 0x0F);
        break;

    case 0xE: /* EXy: Extended command */
        switch (s->effect_param >> 4) {

        case 1: /* E1y: Fine portamento up */
            if (s->effect_param & 0x0F) {
                ch->fine_portamento_up_param = s->effect_param & 0x0F;
            }
            pitch_slide(ctx, ch, -ch->fine_portamento_up_param);
            break;

        case 2: /* E2y: Fine portamento down */
            if (s->effect_param & 0x0F) {
                ch->fine_portamento_down_param = s->effect_param & 0x0F;
            }
            pitch_slide(ctx, ch, ch->fine_portamento_down_param);
            break;

        case 4: /* E4y: Set vibrato control */
            ch->vibrato_waveform = (waveform_type_t)(s->effect_param & 3);
            ch->vibrato_waveform_retrigger = !((s->effect_param >> 2) & 1);
            break;

        case 5: /* E5y: Set finetune */
            if (XM_NOTE_IS_VALID(ch->current->note) && ch->sample != NULL) {
                ch->note = ch->current->note + ch->sample->relative_note
                    + (float)(((s->effect_param & 0x0F) - 8) << 4) / 128.f - 1.f;
                ch->period = period(ctx, ch->note);
                update_frequency(ctx, ch);
            }
            break;

        case 6: /* E6y: Pattern loop */
            if (s->effect_param & 0x0F) {
                if ((s->effect_param & 0x0F) == ch->pattern_loop_count) {
                    /* Loop is over */
                    ch->pattern_loop_count = 0;
                    break;
                }

                /* Jump to the beginning of the loop */
                ch->pattern_loop_count++;
                ctx->position_jump = true;
                ctx->jump_row = ch->pattern_loop_origin;
                ctx->jump_dest = ctx->current_table_index;
            } else {
                /* Set loop start point */
                ch->pattern_loop_origin = ctx->current_row;
                /* Replicate FT2 E60 bug */
                ctx->jump_row = ch->pattern_loop_origin;
            }
            break;

        case 7: /* E7y: Set tremolo control */
            ch->tremolo_waveform = (waveform_type_t)(s->effect_param & 3);
            ch->tremolo_waveform_retrigger = !((s->effect_param >> 2) & 1);
            break;

        case 0xA: /* EAy: Fine volume slide up */
            if (s->effect_param & 0x0F) {
                ch->fine_volume_slide_param = s->effect_param & 0x0F;
            }
            volume_slide(ch, (uint8_t)(ch->fine_volume_slide_param << 4));
            break;

        case 0xB: /* EBy: Fine volume slide down */
            if (s->effect_param & 0x0F) {
                ch->fine_volume_slide_param = s->effect_param & 0x0F;
            }
            volume_slide(ch, ch->fine_volume_slide_param);
            break;

        case 0xD: /* EDy: Note delay */
            /* XXX: figure this out better. EDx triggers
             * the note even when there no note and no
             * instrument. But ED0 acts like like a ghost
             * note, EDx (x ≠ 0) does not. */
            if (s->note == 0 && s->instrument == 0) {
                unsigned int flags = XM_TRIGGER_KEEP_VOLUME;

                if (ch->current->effect_param & 0x0F) {
                    ch->note = ch->orig_note;
                    trigger_note(ctx, ch, flags);
                } else {
                    trigger_note(
                        ctx, ch, flags | XM_TRIGGER_KEEP_PERIOD | XM_TRIGGER_KEEP_SAMPLE_POSITION);
                }
            }
            break;

        case 0xE: /* EEy: Pattern delay */
            ctx->extra_ticks = (ch->current->effect_param & 0x0F) * ctx->tempo;
            break;

        default:
            break;
        }
        break;

    case 0xF: /* Fxx: Set tempo/BPM */
        if (s->effect_param > 0) {
            if (s->effect_param <= 0x1F) {
                ctx->tempo = s->effect_param;
            } else {
                ctx->bpm = s->effect_param;
            }
        }
        break;

    case 16: /* Gxx: Set global volume */
        ctx->global_volume
            = (float)((s->effect_param > 0x40) ? 0x40 : s->effect_param) / (float)0x40;
        break;

    case 17: /* Hxy: Global volume slide */
        if (s->effect_param > 0) {
            ch->global_volume_slide_param = s->effect_param;
        }
        break;

    case 21: /* Lxx: Set envelope position */
        ch->volume_envelope_frame_count = s->effect_param;
        ch->panning_envelope_frame_count = s->effect_param;
        break;

    case 25: /* Pxy: Panning slide */
        if (s->effect_param > 0) {
            ch->panning_slide_param = s->effect_param;
        }
        break;

    case 27: /* Rxy: Multi retrig note */
        if (s->effect_param > 0) {
            if ((s->effect_param >> 4) == 0) {
                /* Keep previous x value */
                ch->multi_retrig_param = (ch->multi_retrig_param & 0xF0) | (s->effect_param & 0x0F);
            } else {
                ch->multi_retrig_param = s->effect_param;
            }
        }
        break;

    case 29: /* Txy: Tremor */
        if (s->effect_param > 0) {
            /* Tremor x and y params do not appear to be separately
             * kept in memory, unlike Rxy */
            ch->tremor_param = s->effect_param;
        }
        break;

    case 33: /* Xxy: Extra stuff */
        switch (s->effect_param >> 4) {

        case 1: /* X1y: Extra fine portamento up */
            if (s->effect_param & 0x0F) {
                ch->extra_fine_portamento_up_param = s->effect_param & 0x0F;
            }
            pitch_slide(ctx, ch, -1.0f * ch->extra_fine_portamento_up_param);
            break;

        case 2: /* X2y: Extra fine portamento down */
            if (s->effect_param & 0x0F) {
                ch->extra_fine_portamento_down_param = s->effect_param & 0x0F;
            }
            pitch_slide(ctx, ch, ch->extra_fine_portamento_down_param);
            break;

        default:
            break;
        }
        break;

    default:
        break;
    }
}

static void trigger_note(xm_context_t *ctx, channel_context_t *ch, unsigned int flags) {
    if (!(flags & XM_TRIGGER_KEEP_SAMPLE_POSITION)) {
        ch->sample_position = 0.f;
        ch->ping = true;
    }

    if (ch->sample != NULL) {
        if (!(flags & XM_TRIGGER_KEEP_VOLUME)) {
            ch->volume = ch->sample->volume;
        }

        ch->panning = ch->sample->panning;
    }

    if (!(flags & XM_TRIGGER_KEEP_ENVELOPE)) {
        ch->sustained = true;
        ch->fadeout_volume = ch->volume_envelope_volume = 1.0f;
        ch->panning_envelope_panning = .5f;
        ch->volume_envelope_frame_count = ch->panning_envelope_frame_count = 0;
    }
    ch->vibrato_note_offset = 0.f;
    ch->tremolo_volume = 0.f;
    ch->tremor_on = false;

    ch->autovibrato_ticks = 0;

    if (ch->vibrato_waveform_retrigger) {
        ch->vibrato_ticks = 0; /* XXX: should the waveform itself also
                                * be reset to sine? */
    }
    if (ch->tremolo_waveform_retrigger) {
        ch->tremolo_ticks = 0;
    }

    if (!(flags & XM_TRIGGER_KEEP_PERIOD)) {
        ch->period = period(ctx, ch->note);
        update_frequency(ctx, ch);
    }

    ch->latest_trigger = ctx->generated_samples;
    if (ch->instrument != NULL) {
        ch->instrument->latest_trigger = ctx->generated_samples;
    }
    if (ch->sample != NULL) {
        ch->sample->latest_trigger = ctx->generated_samples;
    }
}

static void cut_note(channel_context_t *ch) {
    /* NB: this is not the same as Key Off */
    ch->volume = .0f;
}

static void key_off(channel_context_t *ch) {
    /* Key Off */
    ch->sustained = false;

    /* If no volume envelope is used, also cut the note */
    if (ch->instrument == NULL || !ch->instrument->volume_envelope.enabled) {
        cut_note(ch);
    }
}

static void row(xm_context_t *ctx) {
    static pattern_slot_t empty_slot = { 0 };

    if (ctx->position_jump) {
        ctx->current_table_index = ctx->jump_dest;
        ctx->current_row = ctx->jump_row;
        ctx->position_jump = false;
        ctx->pattern_break = false;
        ctx->jump_row = 0;
        post_pattern_change(ctx);
    } else if (ctx->pattern_break) {
        ctx->current_table_index++;
        ctx->current_row = ctx->jump_row;
        ctx->pattern_break = false;
        ctx->jump_row = 0;
        post_pattern_change(ctx);
    }

    uint8_t pat_idx = ctx->module.pattern_table[ctx->current_table_index];
    pattern_t *cur = (pat_idx < ctx->module.num_patterns ? ctx->module.patterns + pat_idx : NULL);
    bool in_a_loop = false;

    /* Read notes… */
    for (uint8_t i = 0; i < ctx->module.num_channels; ++i) {
        pattern_slot_t *s
            = (cur ? cur->slots + ctx->current_row * ctx->module.num_channels + i : &empty_slot);
        channel_context_t *ch = ctx->channels + i;

        ch->current = s;

        if (s->effect_type != 0xE || s->effect_param >> 4 != 0xD) {
            handle_note_and_instrument(ctx, ch, s);
        } else {
            ch->note_delay_param = s->effect_param & 0x0F;
        }

        if (!in_a_loop && ch->pattern_loop_count > 0) {
            in_a_loop = true;
        }
    }

    if (!in_a_loop) {
        /* No E6y loop is in effect (or we are in the first pass) */
        ctx->loop_count = (ctx->row_loop_count[XM_MAX_NUM_ROWS * ctx->current_table_index
            + ctx->current_row]++);
    }

    ctx->current_row++; /* Since this is an uint8, this line can
                         * increment from 255 to 0, in which case it
                         * is still necessary to go the next
                         * pattern. */
    if (!ctx->position_jump && !ctx->pattern_break
        && (ctx->current_row >= (cur ? cur->num_rows : XM_DEFAULT_PATTERN_LENGTH)
            || ctx->current_row == 0)) {
        ctx->current_table_index++;
        ctx->current_row = ctx->jump_row; /* This will be 0 most of
                                           * the time, except when E60
                                           * is used */
        ctx->jump_row = 0;
        post_pattern_change(ctx);
    }
}

static void envelope_tick(
    channel_context_t *ch, envelope_t *env, uint16_t *counter, float *outval) {
    if (env->num_points < 2) {
        /* Don't really know what to do… */
        if (env->num_points == 1) {
            /* XXX I am pulling this out of my ass */
            *outval = (float)env->points[0].value / (float)0x40;
            if (*outval > 1) {
                *outval = 1;
            }
        }

        return;
    } else {
        uint8_t j;

        if (env->loop_enabled) {
            uint16_t loop_start = env->points[env->loop_start_point].frame;
            uint16_t loop_end = env->points[env->loop_end_point].frame;
            uint16_t loop_length = loop_end - loop_start;

            if (*counter >= loop_end) {
                *counter -= loop_length;
            }
        }

        for (j = 0; j < (env->num_points - 2); ++j) {
            if (env->points[j].frame <= *counter && env->points[j + 1].frame >= *counter) {
                break;
            }
        }

        *outval = envelope_lerp(env->points + j, env->points + j + 1, *counter) / (float)0x40;

        /* Make sure it is safe to increment frame count */
        if (!ch->sustained || !env->sustain_enabled
            || *counter != env->points[env->sustain_point].frame) {
            (*counter)++;
        }
    }
}

static void envelopes(channel_context_t *ch) {
    if (ch->instrument != NULL) {
        if (ch->instrument->volume_envelope.enabled) {
            if (!ch->sustained) {
                ch->fadeout_volume -= (float)ch->instrument->volume_fadeout / 32768.f;
                XM_CLAMP_DOWN(ch->fadeout_volume);
            }

            envelope_tick(ch, &(ch->instrument->volume_envelope),
                &(ch->volume_envelope_frame_count), &(ch->volume_envelope_volume));
        }

        if (ch->instrument->panning_envelope.enabled) {
            envelope_tick(ch, &(ch->instrument->panning_envelope),
                &(ch->panning_envelope_frame_count), &(ch->panning_envelope_panning));
        }
    }
}

static void tick(xm_context_t *ctx) {
    if (ctx->current_tick == 0) {
        row(ctx);
    }

    for (uint8_t i = 0; i < ctx->module.num_channels; ++i) {
        channel_context_t *ch = ctx->channels + i;

        envelopes(ch);
        autovibrato(ctx, ch);

        if (ch->arp_in_progress && !XM_HAS_ARPEGGIO(ch->current)) {
            ch->arp_in_progress = false;
            ch->arp_note_offset = 0;
            update_frequency(ctx, ch);
        }
        if (ch->vibrato_in_progress && !XM_HAS_VIBRATO(ch->current)) {
            ch->vibrato_in_progress = false;
            ch->vibrato_note_offset = 0.f;
            update_frequency(ctx, ch);
        }

        switch (ch->current->volume_column >> 4) {

        case 0x6: /* Volume slide down */
            if (ctx->current_tick == 0)
                break;
            volume_slide(ch, ch->current->volume_column & 0x0F);
            break;

        case 0x7: /* Volume slide up */
            if (ctx->current_tick == 0)
                break;
            volume_slide(ch, (uint8_t)(ch->current->volume_column << 4));
            break;

        case 0xB: /* Vibrato */
            if (ctx->current_tick == 0)
                break;
            ch->vibrato_in_progress = false;
            vibrato(ctx, ch, ch->vibrato_param);
            break;

        case 0xD: /* Panning slide left */
            if (ctx->current_tick == 0)
                break;
            panning_slide(ch, (uint8_t)(ch->current->volume_column & 0x0F));
            break;

        case 0xE: /* Panning slide right */
            if (ctx->current_tick == 0)
                break;
            panning_slide(ch, (uint8_t)(ch->current->volume_column << 4));
            break;

        case 0xF: /* Tone portamento */
            if (ctx->current_tick == 0)
                break;
            tone_portamento(ctx, ch);
            break;

        default:
            break;
        }

        switch (ch->current->effect_type) {

        case 0: /* 0xy: Arpeggio */
            if (ch->current->effect_param > 0) {
                char arp_offset = ctx->tempo % 3;
                switch (arp_offset) {
                case 2: /* 0 -> x -> 0 -> y -> x -> … */
                    if (ctx->current_tick == 1) {
                        ch->arp_in_progress = true;
                        ch->arp_note_offset = ch->current->effect_param >> 4;
                        update_frequency(ctx, ch);
                        break;
                    }
                    /* No break here, this is intended */
                    IT_FALLTHROUGH;
                case 1: /* 0 -> 0 -> y -> x -> … */
                    if (ctx->current_tick == 0) {
                        ch->arp_in_progress = false;
                        ch->arp_note_offset = 0;
                        update_frequency(ctx, ch);
                        break;
                    }
                    /* No break here, this is intended */
                    IT_FALLTHROUGH;
                case 0: /* 0 -> y -> x -> … */
                    arpeggio(ctx, ch, ch->current->effect_param,
                        (uint16_t)(ctx->current_tick - arp_offset));
                    break;

                default:
                    break;
                }
            }
            break;

        case 1: /* 1xx: Portamento up */
            if (ctx->current_tick == 0)
                break;
            pitch_slide(ctx, ch, -ch->portamento_up_param);
            break;

        case 2: /* 2xx: Portamento down */
            if (ctx->current_tick == 0)
                break;
            pitch_slide(ctx, ch, ch->portamento_down_param);
            break;

        case 3: /* 3xx: Tone portamento */
            if (ctx->current_tick == 0)
                break;
            tone_portamento(ctx, ch);
            break;

        case 4: /* 4xy: Vibrato */
            if (ctx->current_tick == 0)
                break;
            ch->vibrato_in_progress = true;
            vibrato(ctx, ch, ch->vibrato_param);
            break;

        case 5: /* 5xy: Tone portamento + Volume slide */
            if (ctx->current_tick == 0)
                break;
            tone_portamento(ctx, ch);
            volume_slide(ch, ch->volume_slide_param);
            break;

        case 6: /* 6xy: Vibrato + Volume slide */
            if (ctx->current_tick == 0)
                break;
            ch->vibrato_in_progress = true;
            vibrato(ctx, ch, ch->vibrato_param);
            volume_slide(ch, ch->volume_slide_param);
            break;

        case 7: /* 7xy: Tremolo */
            if (ctx->current_tick == 0)
                break;
            tremolo(ctx, ch, ch->tremolo_param, ch->tremolo_ticks++);
            break;

        case 0xA: /* Axy: Volume slide */
            if (ctx->current_tick == 0)
                break;
            volume_slide(ch, ch->volume_slide_param);
            break;

        case 0xE: /* EXy: Extended command */
            switch (ch->current->effect_param >> 4) {

            case 0x9: /* E9y: Retrigger note */
                if (ctx->current_tick != 0 && ch->current->effect_param & 0x0F) {
                    if (!(ctx->current_tick % (ch->current->effect_param & 0x0F))) {
                        trigger_note(ctx, ch, XM_TRIGGER_KEEP_VOLUME);
                        envelopes(ch);
                    }
                }
                break;

            case 0xC: /* ECy: Note cut */
                if ((ch->current->effect_param & 0x0F) == ctx->current_tick) {
                    cut_note(ch);
                }
                break;

            case 0xD: /* EDy: Note delay */
                if (ch->note_delay_param == ctx->current_tick) {
                    handle_note_and_instrument(ctx, ch, ch->current);
                    envelopes(ch);
                }
                break;

            default:
                break;
            }
            break;

        case 17: /* Hxy: Global volume slide */
            if (ctx->current_tick == 0)
                break;
            if ((ch->global_volume_slide_param & 0xF0) && (ch->global_volume_slide_param & 0x0F)) {
                /* Illegal state */
                break;
            }
            if (ch->global_volume_slide_param & 0xF0) {
                /* Global slide up */
                float f = (float)(ch->global_volume_slide_param >> 4) / (float)0x40;
                ctx->global_volume += f;
                XM_CLAMP_UP(ctx->global_volume);
            } else {
                /* Global slide down */
                float f = (float)(ch->global_volume_slide_param & 0x0F) / (float)0x40;
                ctx->global_volume -= f;
                XM_CLAMP_DOWN(ctx->global_volume);
            }
            break;

        case 20: /* Kxx: Key off */
            /* Most documentations will tell you the parameter has no
             * use. Don't be fooled. */
            if (ctx->current_tick == ch->current->effect_param) {
                key_off(ch);
            }
            break;

        case 25: /* Pxy: Panning slide */
            if (ctx->current_tick == 0)
                break;
            panning_slide(ch, ch->panning_slide_param);
            break;

        case 27: /* Rxy: Multi retrig note */
            if (ctx->current_tick == 0)
                break;
            if (((ch->multi_retrig_param) & 0x0F) == 0)
                break;
            if ((ctx->current_tick % (ch->multi_retrig_param & 0x0F)) == 0) {
                trigger_note(ctx, ch, XM_TRIGGER_KEEP_VOLUME | XM_TRIGGER_KEEP_ENVELOPE);

                /* Rxy doesn't affect volume if there's a command in the volume
                   column, or if the instrument has a volume envelope. */
                if (!ch->current->volume_column && !ch->instrument->volume_envelope.enabled) {
                    float v = ch->volume * multi_retrig_multiply[ch->multi_retrig_param >> 4]
                        + multi_retrig_add[ch->multi_retrig_param >> 4] / (float)0x40;
                    XM_CLAMP(v);
                    ch->volume = v;
                }
            }
            break;

        case 29: /* Txy: Tremor */
            if (ctx->current_tick == 0)
                break;
            ch->tremor_on = ((ctx->current_tick - 1)
                    % ((ch->tremor_param >> 4) + (ch->tremor_param & 0x0F) + 2)
                > (ch->tremor_param >> 4));
            break;

        default:
            break;
        }

        float panning, volume;

        panning = ch->panning
            + (ch->panning_envelope_panning - .5f) * (.5f - fabsf(ch->panning - .5f)) * 2.0f;

        if (ch->tremor_on) {
            volume = .0f;
        } else {
            volume = ch->volume + ch->tremolo_volume;
            XM_CLAMP(volume);
            volume *= ch->fadeout_volume * ch->volume_envelope_volume;
        }

#if XM_RAMPING
        /* See https://modarchive.org/forums/index.php?topic=3517.0
         * and https://github.com/Artefact2/libxm/pull/16 */
        ch->target_volume[0] = volume * sqrtf(1.f - panning);
        ch->target_volume[1] = volume * sqrtf(panning);
#else
        ch->actual_volume[0] = volume * sqrtf(1.f - panning);
        ch->actual_volume[1] = volume * sqrtf(panning);
#endif
    }

    ctx->current_tick++;
    if (ctx->current_tick >= ctx->tempo + ctx->extra_ticks) {
        ctx->current_tick = 0;
        ctx->extra_ticks = 0;
    }

    /* FT2 manual says number of ticks / second = BPM * 0.4 */
    ctx->remaining_samples_in_tick += (float)ctx->rate / ((float)ctx->bpm * 0.4f);
}

static float sample_at(sample_t *sample, size_t k) {
    return sample->bits == 8 ? (sample->data8[k] / 128.f) : (sample->data16[k] / 32768.f);
}

static float next_of_sample(channel_context_t *ch) {
    if (ch->instrument == NULL || ch->sample == NULL || ch->sample_position < 0) {
#if XM_RAMPING
        if (ch->frame_count < XM_SAMPLE_RAMPING_POINTS) {
            return XM_LERP(ch->end_of_previous_sample[ch->frame_count], .0f,
                (float)ch->frame_count / (float)XM_SAMPLE_RAMPING_POINTS);
        }
#endif
        return .0f;
    }
    if (ch->sample->length == 0) {
        return .0f;
    }

    float u, v, t;
    uint32_t a, b;
    a = (uint32_t)ch->sample_position; /* This cast is fine,
                                        * sample_position will not
                                        * go above integer
                                        * ranges */
    if (XM_LINEAR_INTERPOLATION) {
        b = a + 1;
        t = ch->sample_position - (float)a; /* Cheaper than fmodf(., 1.f) */
    }
    u = sample_at(ch->sample, a);

    switch (ch->sample->loop_type) {

    case NO_LOOP:
        if (XM_LINEAR_INTERPOLATION) {
            v = (b < ch->sample->length) ? sample_at(ch->sample, b) : .0f;
        }
        ch->sample_position += (float)ch->step;
        if (ch->sample_position >= (float)ch->sample->length) {
            ch->sample_position = -1;
        }
        break;

    case FORWARD_LOOP:
        if (XM_LINEAR_INTERPOLATION) {
            v = sample_at(ch->sample, (b == ch->sample->loop_end) ? ch->sample->loop_start : b);
        }
        ch->sample_position += ch->step;
        while (ch->sample_position >= (float)ch->sample->loop_end) {
            ch->sample_position -= (float)ch->sample->loop_length;
        }
        break;

    case PING_PONG_LOOP:
        if (ch->ping) {
            ch->sample_position += ch->step;
        } else {
            ch->sample_position -= ch->step;
        }
        /* XXX: this may not work for very tight ping-pong loops
         * (ie switches direction more than once per sample */
        if (ch->ping) {
            if (XM_LINEAR_INTERPOLATION) {
                v = sample_at(ch->sample, (b >= ch->sample->loop_end) ? a : b);
            }
            if (ch->sample_position >= (float)ch->sample->loop_end) {
                ch->ping = false;
                ch->sample_position = (float)(ch->sample->loop_end << 1) - ch->sample_position;
            }
            /* sanity checking */
            if (ch->sample_position >= (float)ch->sample->length) {
                ch->ping = false;
                ch->sample_position -= (float)ch->sample->length - 1;
            }
        } else {
            if (XM_LINEAR_INTERPOLATION) {
                v = u;
                u = sample_at(
                    ch->sample, (b == 1 || b - 2 <= ch->sample->loop_start) ? a : (b - 2));
            }
            if (ch->sample_position <= (float)ch->sample->loop_start) {
                ch->ping = true;
                ch->sample_position = (float)(ch->sample->loop_start << 1) - ch->sample_position;
            }
            /* sanity checking */
            if (ch->sample_position <= .0f) {
                ch->ping = true;
                ch->sample_position = .0f;
            }
        }
        break;

    default:
        v = .0f;
        break;
    }

    float endval = (XM_LINEAR_INTERPOLATION ? XM_LERP(u, v, t) : u);

#if XM_RAMPING
    if (ch->frame_count < XM_SAMPLE_RAMPING_POINTS) {
        /* Smoothly transition between old and new sample. */
        return XM_LERP(ch->end_of_previous_sample[ch->frame_count], endval,
            (float)ch->frame_count / (float)XM_SAMPLE_RAMPING_POINTS);
    }
#endif

    return endval;
}

static void sample(xm_context_t *ctx, float *left, float *right) {
    if (ctx->remaining_samples_in_tick <= 0) {
        tick(ctx);
    }
    ctx->remaining_samples_in_tick--;

    *left = 0.f;
    *right = 0.f;

    if (ctx->max_loop_count > 0 && ctx->loop_count >= ctx->max_loop_count) {
        return;
    }

    for (uint8_t i = 0; i < ctx->module.num_channels; ++i) {
        channel_context_t *ch = ctx->channels + i;

        if (ch->instrument == NULL || ch->sample == NULL || ch->sample_position < 0) {
            continue;
        }

        float const fval = next_of_sample(ch);

        if (!ch->muted && !ch->instrument->muted) {
            *left += fval * ch->actual_volume[0];
            *right += fval * ch->actual_volume[1];
        }

#if XM_RAMPING
        ch->frame_count++;
        XM_SLIDE_TOWARDS(ch->actual_volume[0], ch->target_volume[0], ctx->volume_ramp);
        XM_SLIDE_TOWARDS(ch->actual_volume[1], ch->target_volume[1], ctx->volume_ramp);
#endif
    }

    float const fgvol = ctx->global_volume * ctx->amplification;
    *left *= fgvol;
    *right *= fgvol;

    if (XM_IS_DEBUG) {
        if (fabsf(*left) > 1 || fabsf(*right) > 1) {
            XM_DEBUG("clipping frame: %f %f, this is a bad module or a libxm bug", (double)*left,
                (double)*right);
        }
    }
}

void xm_generate_samples(xm_context_t *ctx, float *output, size_t numsamples) {
    ctx->generated_samples += numsamples;

    for (size_t i = 0; i < numsamples; i++) {
        sample(ctx, output + (2 * i), output + (2 * i + 1));
    }
}

/* .xm files are little-endian. */

/* Bounded reader macros.
 * If we attempt to read the buffer out-of-bounds, pretend that the buffer is
 * infinitely padded with zeroes.
 */
#define XM_READ_U8_BOUND(offset, bound)                                                            \
    (((offset) < (bound)) ? (*(uint8_t const *)(moddata + (offset))) : 0)
#define XM_READ_U16_BOUND(offset, bound)                                                           \
    ((uint16_t)XM_READ_U8_BOUND(offset, bound)                                                     \
        | ((uint16_t)XM_READ_U8_BOUND((offset) + 1, bound) << 8))
#define XM_READ_U32_BOUND(offset, bound)                                                           \
    ((uint32_t)XM_READ_U16_BOUND(offset, bound)                                                    \
        | ((uint32_t)XM_READ_U16_BOUND((offset) + 2, bound) << 16))
#define XM_READ_MEMCPY_BOUND(ptr, offset, length, bound)                                           \
    memcpy_pad(ptr, length, moddata, bound, offset)

#define XM_READ_U8(offset)               XM_READ_U8_BOUND(offset, moddata_length)
#define XM_READ_U16(offset)              XM_READ_U16_BOUND(offset, moddata_length)
#define XM_READ_U32(offset)              XM_READ_U32_BOUND(offset, moddata_length)
#define READ_MEMCPY(ptr, offset, length) XM_READ_MEMCPY_BOUND(ptr, offset, length, moddata_length)

#define XM_MIN(a, b) ((a) < (b) ? (a) : (b))

static inline void memcpy_pad(
    void *dst, size_t dst_len, void const *src, size_t src_len, size_t offset) {
    uint8_t *dst_c = (uint8_t *)dst;
    uint8_t const *src_c = (uint8_t const *)src;

    /* how many bytes can be copied without overrunning `src` */
    size_t copy_bytes = (src_len >= offset) ? (src_len - offset) : 0;
    copy_bytes = copy_bytes > dst_len ? dst_len : copy_bytes;

    it_memory_copy_byte_sequence(dst_c, copy_bytes, src_c + offset, copy_bytes);
    /* padded bytes */
    it_memory_set_byte_sequence(dst_c + copy_bytes, dst_len - copy_bytes, 0);
}

int check_sanity_preload(char const *module, size_t module_length) {
    if (module_length < 60) {
        return 4;
    }

    if (!it_memory_are_byte_sequences_identical("Extended Module: ", 17, module, 17)) {
        return 1;
    }

    if (module[37] != 0x1A) {
        return 2;
    }

    if (module[59] != 0x01 || module[58] != 0x04) {
        /* Not XM 1.04 */
        return 3;
    }

    return 0;
}

int check_sanity_postload(xm_context_t *ctx) {
    (void)ctx;
    /* @todo: plenty of stuff to do here… */
    return 0;
}

size_t get_memory_needed_for_context(char const *moddata, size_t moddata_length) {
    size_t memory_needed = 0;
    size_t offset = 60; /* Skip the first header */
    uint16_t num_channels;
    uint16_t num_patterns;
    uint16_t num_instruments;

    /* Read the module header */

    num_channels = (uint16_t)XM_READ_U16(offset + 8);
    num_patterns = (uint16_t)XM_READ_U16(offset + 10);
    memory_needed += num_patterns * sizeof(pattern_t);

    num_instruments = (uint16_t)XM_READ_U16(offset + 12);
    memory_needed += num_instruments * sizeof(instrument_t);

    memory_needed += (size_t)XM_MAX_NUM_ROWS * (uint16_t)XM_READ_U16(offset + 4)
        * sizeof(uint8_t); /* Module length */

    /* Header size */
    offset += XM_READ_U32(offset);

    /* Read pattern headers */
    for (uint16_t i = 0; i < num_patterns; ++i) {
        uint16_t num_rows;

        num_rows = (uint16_t)XM_READ_U16(offset + 5);
        memory_needed += num_rows * num_channels * sizeof(pattern_slot_t);

        /* Pattern header length + packed pattern data size */
        offset += (uint32_t)XM_READ_U32(offset) + (uint16_t)XM_READ_U16(offset + 7);
    }

    /* Read instrument headers */
    for (uint16_t i = 0; i < num_instruments; ++i) {
        uint16_t num_samples;
        uint32_t sample_size_aggregate = 0;

        num_samples = (uint16_t)XM_READ_U16(offset + 27);
        memory_needed += num_samples * sizeof(sample_t);

        /* Instrument header size */
        uint32_t ins_header_size = (uint32_t)XM_READ_U32(offset);
        if (ins_header_size == 0 || ins_header_size > XM_INSTRUMENT_HEADER_LENGTH)
            ins_header_size = XM_INSTRUMENT_HEADER_LENGTH;
        offset += ins_header_size;

        for (uint16_t j = 0; j < num_samples; ++j) {
            uint32_t sample_size;

            sample_size = (uint32_t)XM_READ_U32(offset);
            sample_size_aggregate += sample_size;
            memory_needed += sample_size;
            offset += 40; /* See comment in load_module() */
        }

        offset += sample_size_aggregate;
    }

    memory_needed += num_channels * sizeof(channel_context_t);
    memory_needed += sizeof(xm_context_t);

    return memory_needed;
}

char *load_module(xm_context_t *ctx, char const *moddata, size_t moddata_length, char *mempool) {
    size_t offset = 0;
    module_t *mod = &(ctx->module);

    /* Read XM header */
#if XM_STRINGS
    READ_MEMCPY(mod->name, offset + 17, XM_MODULE_NAME_LENGTH);
    READ_MEMCPY(mod->trackername, offset + 38, XM_TRACKER_NAME_LENGTH);
#endif
    offset += 60;

    /* Read module header */
    uint32_t header_size = (uint32_t)XM_READ_U32(offset);

    mod->length = (uint16_t)XM_READ_U16(offset + 4);
    mod->restart_position = (uint16_t)XM_READ_U16(offset + 6);
    mod->num_channels = (uint16_t)XM_READ_U16(offset + 8);
    mod->num_patterns = (uint16_t)XM_READ_U16(offset + 10);
    mod->num_instruments = (uint16_t)XM_READ_U16(offset + 12);

    mod->patterns = it_align_cast(mempool, pattern_t *);
    mempool += mod->num_patterns * sizeof(pattern_t);

    mod->instruments = it_align_cast(mempool, instrument_t *);
    mempool += mod->num_instruments * sizeof(instrument_t);

    uint16_t flags = (uint16_t)XM_READ_U32(offset + 14);
    mod->frequency_type = (flags & (1 << 0)) ? LINEAR_FREQUENCIES : AMIGA_FREQUENCIES;

    ctx->tempo = (uint16_t)XM_READ_U16(offset + 16);
    ctx->bpm = (uint16_t)XM_READ_U16(offset + 18);

    READ_MEMCPY(mod->pattern_table, offset + 20, XM_PATTERN_ORDER_TABLE_LENGTH);
    offset += header_size;

    /* Read patterns */
    for (uint16_t i = 0; i < mod->num_patterns; ++i) {
        uint16_t packed_patterndata_size = (uint16_t)XM_READ_U16(offset + 7);
        pattern_t *pat = mod->patterns + i;

        pat->num_rows = (uint16_t)XM_READ_U16(offset + 5);

        pat->slots = (pattern_slot_t *)mempool;
        mempool += mod->num_channels * pat->num_rows * sizeof(pattern_slot_t);

        /* Pattern header length */
        offset += (uint32_t)XM_READ_U32(offset);

        if (packed_patterndata_size == 0) {
            /* No pattern data is present */
            it_memory_set_byte_sequence(
                pat->slots, sizeof(pattern_slot_t) * pat->num_rows * mod->num_channels, 0);
        } else {
            /* This isn't your typical for loop */
            for (uint16_t j = 0, k = 0; j < packed_patterndata_size; ++k) {
                uint8_t note = XM_READ_U8(offset + j);
                pattern_slot_t *slot = pat->slots + k;

                if (note & (1 << 7)) {
                    /* MSB is set, this is a compressed packet */
                    ++j;

                    if (note & (1 << 0)) {
                        /* Note follows */
                        slot->note = XM_READ_U8(offset + j);
                        ++j;
                    } else {
                        slot->note = 0;
                    }

                    if (note & (1 << 1)) {
                        /* Instrument follows */
                        slot->instrument = XM_READ_U8(offset + j);
                        ++j;
                    } else {
                        slot->instrument = 0;
                    }

                    if (note & (1 << 2)) {
                        /* Volume column follows */
                        slot->volume_column = XM_READ_U8(offset + j);
                        ++j;
                    } else {
                        slot->volume_column = 0;
                    }

                    if (note & (1 << 3)) {
                        /* Effect follows */
                        slot->effect_type = XM_READ_U8(offset + j);
                        ++j;
                    } else {
                        slot->effect_type = 0;
                    }

                    if (note & (1 << 4)) {
                        /* Effect parameter follows */
                        slot->effect_param = XM_READ_U8(offset + j);
                        ++j;
                    } else {
                        slot->effect_param = 0;
                    }
                } else {
                    /* Uncompressed packet */
                    slot->note = note;
                    slot->instrument = XM_READ_U8(offset + j + 1);
                    slot->volume_column = XM_READ_U8(offset + j + 2);
                    slot->effect_type = XM_READ_U8(offset + j + 3);
                    slot->effect_param = XM_READ_U8(offset + j + 4);
                    j += 5;
                }
            }
        }

        offset += packed_patterndata_size;
    }

    /* Read instruments */
    for (uint16_t i = 0; i < ctx->module.num_instruments; ++i) {
        instrument_t *instr = mod->instruments + i;

        /* Original FT2 would load instruments with a direct read into the
           instrument data structure that was previously zeroed. This means
           that if the declared length was less than XM_INSTRUMENT_HEADER_LENGTH,
           all excess data would be zeroed. This is used by the XM compressor
           BoobieSqueezer. To implement this, bound all reads to the header size. */
        uint32_t ins_header_size = XM_READ_U32(offset);
        if (ins_header_size == 0 || ins_header_size > XM_INSTRUMENT_HEADER_LENGTH)
            ins_header_size = XM_INSTRUMENT_HEADER_LENGTH;

#if XM_STRINGS
        XM_READ_MEMCPY_BOUND(
            instr->name, offset + 4, XM_INSTRUMENT_NAME_LENGTH, offset + ins_header_size);
        instr->name[XM_INSTRUMENT_NAME_LENGTH] = 0;
#endif
        instr->num_samples = (uint16_t)XM_READ_U16_BOUND(offset + 27, offset + ins_header_size);

        if (instr->num_samples > 0) {
            /* Read extra header properties */
            XM_READ_MEMCPY_BOUND(
                instr->sample_of_notes, offset + 33, XM_NUM_NOTES, offset + ins_header_size);

            instr->volume_envelope.num_points
                = XM_READ_U8_BOUND(offset + 225, offset + ins_header_size);
            if (instr->volume_envelope.num_points > XM_NUM_ENVELOPE_POINTS)
                instr->volume_envelope.num_points = XM_NUM_ENVELOPE_POINTS;

            instr->panning_envelope.num_points
                = XM_READ_U8_BOUND(offset + 226, offset + ins_header_size);
            if (instr->panning_envelope.num_points > XM_NUM_ENVELOPE_POINTS)
                instr->panning_envelope.num_points = XM_NUM_ENVELOPE_POINTS;

            for (uint8_t j = 0; j < instr->volume_envelope.num_points; ++j) {
                instr->volume_envelope.points[j].frame
                    = (uint16_t)XM_READ_U16_BOUND(offset + 129 + 4 * j, offset + ins_header_size);
                instr->volume_envelope.points[j].value = (uint16_t)XM_READ_U16_BOUND(
                    offset + 129 + 4 * j + 2, offset + ins_header_size);
            }

            for (uint8_t j = 0; j < instr->panning_envelope.num_points; ++j) {
                instr->panning_envelope.points[j].frame
                    = (uint16_t)XM_READ_U16_BOUND(offset + 177 + 4 * j, offset + ins_header_size);
                instr->panning_envelope.points[j].value = (uint16_t)XM_READ_U16_BOUND(
                    offset + 177 + 4 * j + 2, offset + ins_header_size);
            }

            instr->volume_envelope.sustain_point
                = XM_READ_U8_BOUND(offset + 227, offset + ins_header_size);
            instr->volume_envelope.loop_start_point
                = XM_READ_U8_BOUND(offset + 228, offset + ins_header_size);
            instr->volume_envelope.loop_end_point
                = XM_READ_U8_BOUND(offset + 229, offset + ins_header_size);

            instr->panning_envelope.sustain_point
                = XM_READ_U8_BOUND(offset + 230, offset + ins_header_size);
            instr->panning_envelope.loop_start_point
                = XM_READ_U8_BOUND(offset + 231, offset + ins_header_size);
            instr->panning_envelope.loop_end_point
                = XM_READ_U8_BOUND(offset + 232, offset + ins_header_size);

            // Fix broken modules with loop points outside of defined points
            if (instr->volume_envelope.num_points > 0) {
                instr->volume_envelope.loop_start_point = XM_MIN(
                    instr->volume_envelope.loop_start_point, instr->volume_envelope.num_points - 1);
                instr->volume_envelope.loop_end_point = XM_MIN(
                    instr->volume_envelope.loop_end_point, instr->volume_envelope.num_points - 1);
            }
            if (instr->panning_envelope.num_points > 0) {
                instr->panning_envelope.loop_start_point
                    = XM_MIN(instr->panning_envelope.loop_start_point,
                        instr->panning_envelope.num_points - 1);
                instr->panning_envelope.loop_end_point = XM_MIN(
                    instr->panning_envelope.loop_end_point, instr->panning_envelope.num_points - 1);
            }

            uint8_t inst_flags = XM_READ_U8_BOUND(offset + 233, offset + ins_header_size);
            instr->volume_envelope.enabled = inst_flags & (1 << 0);
            instr->volume_envelope.sustain_enabled = inst_flags & (1 << 1);
            instr->volume_envelope.loop_enabled = inst_flags & (1 << 2);

            inst_flags = XM_READ_U8_BOUND(offset + 234, offset + ins_header_size);
            instr->panning_envelope.enabled = inst_flags & (1 << 0);
            instr->panning_envelope.sustain_enabled = inst_flags & (1 << 1);
            instr->panning_envelope.loop_enabled = inst_flags & (1 << 2);

            instr->vibrato_type
                = (waveform_type_t)(XM_READ_U8_BOUND(offset + 235, offset + ins_header_size));
            if (instr->vibrato_type == 2) {
                instr->vibrato_type = (waveform_type_t)(1);
            } else if (instr->vibrato_type == 1) {
                instr->vibrato_type = (waveform_type_t)(2);
            }
            instr->vibrato_sweep = XM_READ_U8_BOUND(offset + 236, offset + ins_header_size);
            instr->vibrato_depth = XM_READ_U8_BOUND(offset + 237, offset + ins_header_size);
            instr->vibrato_rate = XM_READ_U8_BOUND(offset + 238, offset + ins_header_size);
            instr->volume_fadeout
                = (uint16_t)XM_READ_U16_BOUND(offset + 239, offset + ins_header_size);

            instr->samples = it_align_cast(mempool, sample_t *);
            mempool += instr->num_samples * sizeof(sample_t);
        } else {
            instr->samples = NULL;
        }

        /* Instrument header size */
        offset += ins_header_size;

        for (uint16_t j = 0; j < instr->num_samples; ++j) {
            /* Read sample header */
            sample_t *sample = instr->samples + j;

            sample->length = (uint32_t)XM_READ_U32(offset);
            sample->loop_start = (uint32_t)XM_READ_U32(offset + 4);
            sample->loop_length = (uint32_t)XM_READ_U32(offset + 8);
            sample->loop_end = sample->loop_start + sample->loop_length;
            sample->volume = (float)XM_READ_U8(offset + 12) / (float)0x40;
            sample->finetune = (int8_t)XM_READ_U8(offset + 13);

            /* Fix invalid loop definitions */
            if (sample->loop_start > sample->length)
                sample->loop_start = sample->length;
            if (sample->loop_end > sample->length)
                sample->loop_end = sample->length;
            sample->loop_length = sample->loop_end - sample->loop_start;

            uint8_t sample_flags = XM_READ_U8(offset + 14);
            if ((sample_flags & 3) == 0 || sample->loop_length == 0) {
                sample->loop_type = NO_LOOP;
            } else if ((sample_flags & 3) == 1) {
                sample->loop_type = FORWARD_LOOP;
            } else {
                sample->loop_type = PING_PONG_LOOP;
            }

            sample->bits = (sample_flags & (1 << 4)) ? 16 : 8;

            sample->panning = (float)XM_READ_U8(offset + 15) / (float)0xFF;
            sample->relative_note = (int8_t)XM_READ_U8(offset + 16);
#if XM_STRINGS
            READ_MEMCPY(sample->name, offset + 18, XM_SAMPLE_NAME_LENGTH);
            sample->name[XM_SAMPLE_NAME_LENGTH] = 0;
#endif
            sample->data8 = (int8_t *)mempool;
            mempool += sample->length;

            if (sample->bits == 16) {
                sample->loop_start >>= 1;
                sample->loop_length >>= 1;
                sample->loop_end >>= 1;
                sample->length >>= 1;
            }

            /* Notice that, even if there's a "sample header size" in the
               instrument header, that value seems ignored, and might even
               be wrong in some corrupted modules. */
            offset += 40;
        }

        for (uint16_t j = 0; j < instr->num_samples; ++j) {
            /* Read sample data */
            sample_t *sample = instr->samples + j;
            uint32_t length = sample->length;

            if (sample->bits == 16) {
                int16_t v = 0;
                for (uint32_t k = 0; k < length; ++k) {
                    v = v + (int16_t)XM_READ_U16(offset + (k << 1));
                    sample->data16[k] = v;
                }
                offset += sample->length << 1;
            } else {
                int8_t v = 0;
                for (uint32_t k = 0; k < length; ++k) {
                    v = v + (int8_t)XM_READ_U8(offset + k);
                    sample->data8[k] = v;
                }
                offset += sample->length;
            }
        }
    }

    return mempool;
}

#define XM_OFFSET(ptr)                                                                             \
    do {                                                                                           \
        (ptr) = (void *)((intptr_t)(ptr) + (intptr_t)(*ctxp));                                     \
    } while (0)

#define XM_CHECK_CHANNEL(ctx, c)                                                                   \
    do {                                                                                           \
        if (XM_IS_DEBUG && ((c) == 0 || (c) > (ctx)->module.num_channels))                         \
            XM_DEBUG("invalid channel %d", (c));                                                   \
    } while (0)

#define XM_CHECK_INSTRUMENT(ctx, i)                                                                \
    do {                                                                                           \
        if (XM_IS_DEBUG && ((i) == 0 || (i) > (ctx)->module.num_instruments))                      \
            XM_DEBUG("invalid instrument %d", (i));                                                \
    } while (0)

#define XM_CHECK_SAMPLE(ctx, i, s)                                                                 \
    do {                                                                                           \
        XM_CHECK_INSTRUMENT((ctx), (i));                                                           \
        if (XM_IS_DEBUG && ((s) > (ctx)->module.instruments[(i)].num_samples))                     \
            XM_DEBUG("invalid sample %d for instrument %d", (s), (i));                             \
    } while (0)

int xm_create_context_safe(
    xm_context_t **ctxp, char const *moddata, size_t moddata_length, uint32_t rate) {
    size_t bytes_needed;
    char *mempool;
    xm_context_t *ctx;

    if (XM_DEFENSIVE) {
        int ret;
        if ((ret = check_sanity_preload(moddata, moddata_length))) {
            XM_DEBUG("check_sanity_preload() returned %i, module is not safe to load", ret);
            return 1;
        }
    }

    bytes_needed = get_memory_needed_for_context(moddata, moddata_length);
    mempool = (char *)malloc(bytes_needed);
    if (mempool == NULL && bytes_needed > 0) {
        /* malloc() failed, trouble ahead */
        XM_DEBUG("call to malloc() failed, returned %p", (void *)mempool);
        return 2;
    }

    /* Initialize most of the fields to 0, 0.f, NULL or false depending on type */
    it_memory_set_byte_sequence(mempool, bytes_needed, 0);

    ctx = (*ctxp = it_align_cast(mempool, xm_context_t *));
    ctx->ctx_size = bytes_needed; /* Keep original requested size for xmconvert */
    mempool += sizeof(xm_context_t);

    ctx->rate = rate;
    mempool = load_module(ctx, moddata, moddata_length, mempool);

    ctx->channels = it_align_cast(mempool, channel_context_t *);
    mempool += ctx->module.num_channels * sizeof(channel_context_t);

    ctx->global_volume = 1.f;
    ctx->amplification
        = .25f; /* XXX: some bad modules may still clip. Find out something better. */

#if XM_RAMPING
    ctx->volume_ramp = (1.f / 128.f);
#endif

    for (uint8_t i = 0; i < ctx->module.num_channels; ++i) {
        channel_context_t *ch = ctx->channels + i;

        ch->ping = true;
        ch->vibrato_waveform = SINE_WAVEFORM;
        ch->vibrato_waveform_retrigger = true;
        ch->tremolo_waveform = SINE_WAVEFORM;
        ch->tremolo_waveform_retrigger = true;

        ch->volume = ch->volume_envelope_volume = ch->fadeout_volume = 1.0f;
        ch->panning = ch->panning_envelope_panning = .5f;
        ch->actual_volume[0] = .0f;
        ch->actual_volume[1] = .0f;
    }

    ctx->row_loop_count = (uint8_t *)mempool;
    mempool += ctx->module.length * XM_MAX_NUM_ROWS * sizeof(uint8_t);

    if (XM_DEFENSIVE) {
        int ret;
        if ((ret = check_sanity_postload(ctx))) {
            XM_DEBUG("check_sanity_postload() returned %i, module is not safe to play", ret);
            xm_free_context(ctx);
            return 1;
        }
    }

    return 0;
}

void xm_free_context(xm_context_t *context) { free(context); }

void xm_set_max_loop_count(xm_context_t *context, uint8_t loopcnt) {
    context->max_loop_count = loopcnt;
}

uint8_t xm_get_loop_count(xm_context_t *context) { return context->loop_count; }

void xm_seek(xm_context_t *ctx, uint8_t pot, uint8_t row, uint16_t tick) {
    ctx->current_table_index = pot;
    ctx->current_row = row;
    ctx->current_tick = tick;
    ctx->remaining_samples_in_tick = 0;
}

bool xm_mute_channel(xm_context_t *ctx, uint16_t channel, bool mute) {
    XM_CHECK_CHANNEL(ctx, channel);
    bool old = ctx->channels[channel - 1].muted;
    ctx->channels[channel - 1].muted = mute;
    return old;
}

bool xm_mute_instrument(xm_context_t *ctx, uint16_t instr, bool mute) {
    XM_CHECK_INSTRUMENT(ctx, instr);
    bool old = ctx->module.instruments[instr - 1].muted;
    ctx->module.instruments[instr - 1].muted = mute;
    return old;
}

#if XM_STRINGS
char const *xm_get_module_name(xm_context_t *ctx) { return ctx->module.name; }

char const *xm_get_tracker_name(xm_context_t *ctx) { return ctx->module.trackername; }
#else
char const *xm_get_module_name(xm_context_t *ctx) {
    (void)ctx;
    return NULL;
}

char const *xm_get_tracker_name(xm_context_t *ctx) {
    (void)ctx;
    return NULL;
}
#endif

uint16_t xm_get_number_of_channels(xm_context_t *ctx) { return ctx->module.num_channels; }

uint16_t xm_get_module_length(xm_context_t *ctx) { return ctx->module.length; }

uint16_t xm_get_number_of_patterns(xm_context_t *ctx) { return ctx->module.num_patterns; }

uint16_t xm_get_number_of_rows(xm_context_t *ctx, uint16_t pattern) {
    if (pattern < ctx->module.num_patterns)
        return ctx->module.patterns[pattern].num_rows;
    return XM_DEFAULT_PATTERN_LENGTH;
}

uint16_t xm_get_number_of_instruments(xm_context_t *ctx) { return ctx->module.num_instruments; }

uint16_t xm_get_number_of_samples(xm_context_t *ctx, uint16_t instrument) {
    XM_CHECK_INSTRUMENT(ctx, instrument);
    return ctx->module.instruments[instrument - 1].num_samples;
}

void *xm_get_sample_waveform(
    xm_context_t *ctx, uint16_t i, uint16_t s, size_t *size, uint8_t *bits) {
    XM_CHECK_SAMPLE(ctx, i, s);
    *size = ctx->module.instruments[i - 1].samples[s].length;
    *bits = ctx->module.instruments[i - 1].samples[s].bits;
    return ctx->module.instruments[i - 1].samples[s].data8;
}

void xm_get_playing_speed(xm_context_t *ctx, uint16_t *bpm, uint16_t *tempo) {
    if (bpm)
        *bpm = ctx->bpm;
    if (tempo)
        *tempo = ctx->tempo;
}

void xm_get_position(
    xm_context_t *ctx, uint8_t *pattern_index, uint8_t *pattern, uint8_t *row, uint64_t *samples) {
    if (pattern_index)
        *pattern_index = ctx->current_table_index;
    if (pattern)
        *pattern = ctx->module.pattern_table[ctx->current_table_index];
    if (row)
        *row = ctx->current_row;
    if (samples)
        *samples = ctx->generated_samples;
}

uint64_t xm_get_latest_trigger_of_instrument(xm_context_t *ctx, uint16_t instr) {
    XM_CHECK_INSTRUMENT(ctx, instr);
    return ctx->module.instruments[instr - 1].latest_trigger;
}

uint64_t xm_get_latest_trigger_of_sample(xm_context_t *ctx, uint16_t instr, uint16_t sample) {
    XM_CHECK_SAMPLE(ctx, instr, sample);
    return ctx->module.instruments[instr - 1].samples[sample].latest_trigger;
}

uint64_t xm_get_latest_trigger_of_channel(xm_context_t *ctx, uint16_t chn) {
    XM_CHECK_CHANNEL(ctx, chn);
    return ctx->channels[chn - 1].latest_trigger;
}

bool xm_is_channel_active(xm_context_t *ctx, uint16_t chn) {
    XM_CHECK_CHANNEL(ctx, chn);
    channel_context_t *ch = ctx->channels + (chn - 1);
    return ch->instrument != NULL && ch->sample != NULL && ch->sample_position >= 0;
}

float xm_get_frequency_of_channel(xm_context_t *ctx, uint16_t chn) {
    XM_CHECK_CHANNEL(ctx, chn);
    return ctx->channels[chn - 1].frequency;
}

float xm_get_volume_of_channel(xm_context_t *ctx, uint16_t chn) {
    XM_CHECK_CHANNEL(ctx, chn);
    return ctx->channels[chn - 1].volume * ctx->global_volume;
}

float xm_get_panning_of_channel(xm_context_t *ctx, uint16_t chn) {
    XM_CHECK_CHANNEL(ctx, chn);
    return ctx->channels[chn - 1].panning;
}

uint16_t xm_get_instrument_of_channel(xm_context_t *ctx, uint16_t chn) {
    XM_CHECK_CHANNEL(ctx, chn);
    channel_context_t *ch = ctx->channels + (chn - 1);
    if (ch->instrument == NULL)
        return 0;
    return (uint16_t)(1 + (ch->instrument - ctx->module.instruments));
}

#endif // #if defined(IT_IMPLEMENT_LIBXM)

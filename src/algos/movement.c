#include <ilotawa/include/algos/movement.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/direction.h>

#define IT_INTEGER_OPS_INLINED
#define IT_VECTOR2_INLINED
#define IT_FIXED_INLINED
#include <tomolipu/include/integer_ops.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/vector2.h>

it_vector2_fixed it_movement_8way_step(
    it_vector2_fixed const position, enum it_direction const direction, it_fixed const distance) {

    return it_vector2_fixed_add(position,
        it_vector2_fixed_mul(it_direction_to_vector(direction),
            (it_vector2_fixed) { .x = distance, .y = distance }));
}

#include <iso646.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define IT_INTEGER_OPS_INLINED
#include <ilotawa/include/private/drawing_queue.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/private/texture.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/memory.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/varying_buffer.h>

// todo: For some reason redrawing on startup happens multiple times, even if conditions are
//       identical.

struct command_info {
    it_typeinfo const typeinfo;
    it_drawing_queue_callback *const callback;
};

// todo: Prefer initialization in module initializer? That way it's easier to handle re-entry.
//       This would also reduce the amount of macro usage and internal details exposed.

static it_buffer command_info_buffer;

static it_varying_buffer command_queue;
static it_varying_buffer_iterator command_queue_iterator;

static it_varying_buffer deferred_command_queue;
static it_varying_buffer_iterator deferred_command_queue_iterator;

static it_varying_buffer cached_deferred_command_queue;

static bool redrawing;

it_drawing_queue_command_id it_drawing_queue_register_command(
    it_typeinfo data_typeinfo, it_drawing_queue_callback *callback) {
    it_test(callback != NULL);

    it_ensure(it_buffer_push(&command_info_buffer,
        &(struct command_info) { .typeinfo = data_typeinfo, .callback = callback }));

    return it_convert(it_drawing_queue_command_id, command_info_buffer.private.length);
}

// todo: Return to proper state on allocation error.
bool it_drawing_queue_push_command(it_drawing_queue_command_id command, void const *data) {
    it_test(data != NULL);
    it_test(command != 0);
    it_test(command <= command_info_buffer.private.length);

    it_drawing_queue_command_id const command_index = command - 1;

    struct command_info const *info = it_buffer_get(&command_info_buffer, command_index);

    if (not global_configuration.use_drawing_queue.value)
        info->callback(data);

    if (it_varying_buffer_iterator_is_at_end(&command_queue_iterator))
        redrawing = true;

    if (redrawing) {
        if (not it_varying_buffer_push(&command_queue, &command_queue_iterator,
                it_typeinfo_init(it_drawing_queue_command_id), &command_index))
            return false;
        if (not it_varying_buffer_push(
                &command_queue, &command_queue_iterator, info->typeinfo, data))
            return false;
    } else {
        it_test(it_varying_buffer_iterator_pump(&command_queue_iterator, &command_queue,
            it_typeinfo_init(it_drawing_queue_command_id)));
        if (*(it_drawing_queue_command_id *)command_queue_iterator.current != command_index) {
            *(it_drawing_queue_command_id *)command_queue_iterator.current = command_index;
            if (not it_varying_buffer_push(
                    &command_queue, &command_queue_iterator, info->typeinfo, data))
                return false;
            redrawing = true;
        } else {
            it_test(it_varying_buffer_iterator_pump(
                &command_queue_iterator, &command_queue, info->typeinfo));
            if (not it_memory_are_byte_sequences_identical(command_queue_iterator.current,
                    info->typeinfo.size, data, info->typeinfo.size)) {
                it_memory_copy_byte_sequence(
                    command_queue_iterator.current, info->typeinfo.size, data, info->typeinfo.size);
                redrawing = true;
            }
        }
    }

    return true;
}

bool it_drawing_queue_push_deferred_command(it_drawing_queue_command_id command, void const *data) {
    it_test(data != NULL);
    it_test(command != 0);
    it_test(command <= command_info_buffer.private.length);

    it_drawing_queue_command_id const command_index = command - 1;

    struct command_info const *info = it_buffer_get(&command_info_buffer, command_index);

    if (not global_configuration.use_drawing_queue.value)
        info->callback(data);

    if (not it_varying_buffer_push(&deferred_command_queue, &deferred_command_queue_iterator,
            it_typeinfo_init(it_drawing_queue_command_id), &command_index))
        return false;

    // todo: Revert.
    if (not it_varying_buffer_push(
            &deferred_command_queue, &deferred_command_queue_iterator, info->typeinfo, data))
        return false;

    return true;
}

static void flush_queue(it_varying_buffer const *queue) {
    for (it_varying_buffer_iterator it
         = it_varying_buffer_iterator_init(queue, it_flag_access_readable);
         it_varying_buffer_iterator_pump(
             &it, queue, it_typeinfo_init(it_drawing_queue_command_id));) {
        uint32_t const id = *(it_drawing_queue_command_id *)it.current;
        it_test(id < command_info_buffer.private.length);

        struct command_info const *info
            = (struct command_info *)it_buffer_get(&command_info_buffer, id);
        it_test(info->callback != NULL);

        it_test(it_varying_buffer_iterator_pump(&it, queue, info->typeinfo));
        info->callback(it.current);
    }
}

IT_INTERNAL void it_drawing_queue_invalidate(void) { flush_queue(&command_queue); }

bool it_drawing_queue_flush(void) {
    if (not global_configuration.use_drawing_queue.value)
        return true;

    if (not it_varying_buffer_iterator_is_at_end(&command_queue_iterator) or redrawing) {
        flush_queue(&command_queue);
        flush_queue(&cached_deferred_command_queue);

        it_varying_buffer const swap = cached_deferred_command_queue;
        cached_deferred_command_queue = deferred_command_queue;
        deferred_command_queue = swap;
    }

    command_queue_iterator = it_varying_buffer_iterator_init(
        &command_queue, it_flag_access_readable bitor it_flag_access_writable);

    deferred_command_queue_iterator = it_varying_buffer_iterator_init(
        &deferred_command_queue, it_flag_access_readable bitor it_flag_access_writable);

    bool was_redrawn = redrawing;

    redrawing = false;

    return was_redrawn;
}

bool it_drawing_queue_module_initialize(void) {
    command_info_buffer = it_buffer_init(it_typeinfo_init(struct command_info));
    command_queue = it_varying_buffer_init();
    command_queue_iterator = it_varying_buffer_iterator_init(
        &command_queue, it_flag_access_readable bitor it_flag_access_writable);
    deferred_command_queue = it_varying_buffer_init();
    deferred_command_queue_iterator = it_varying_buffer_iterator_init(
        &deferred_command_queue, it_flag_access_readable bitor it_flag_access_writable);
    cached_deferred_command_queue = it_varying_buffer_init();

    return true;
}

void it_drawing_queue_module_deinitialize(void) {
    it_buffer_free(&command_info_buffer);
    it_varying_buffer_free(&command_queue);
    it_varying_buffer_free(&deferred_command_queue);
    it_varying_buffer_free(&cached_deferred_command_queue);
}

void it_drawing_queue_module_clean(void) {
    it_buffer_shrink(&command_info_buffer);
    it_varying_buffer_shrink(&command_queue);
    it_varying_buffer_shrink(&deferred_command_queue);
    it_varying_buffer_shrink(&cached_deferred_command_queue);
}

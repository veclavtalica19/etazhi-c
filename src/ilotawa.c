#include <iso646.h>
#include <stdarg.h>
#include <stdint.h>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

#define IT_IMPLEMENT_LIBXM
#include <ilotawa/src/adopted/libxm.h>

#include <ilotawa/include/definitions.h>
#include <ilotawa/include/ilotawa.h>
#include <ilotawa/include/loader.h>
#include <ilotawa/include/private/drawing_queue.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/private/module.h>
#include <ilotawa/include/types/atlas.h>
#include <ilotawa/include/types/image.h>
#include <ilotawa/include/types/private/texture.h>
#include <ilotawa/include/types/private/tilemap.h>

#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/memory.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/managed_buffer.h>
#include <tomolipu/include/types/string.h>
#include <tomolipu/include/types/vector2.h>

// todo: Separate SDL2/backend specific implementations.

// todo: Prefix with 'global_'
SDL_Window *window;
SDL_Renderer *renderer;
SDL_AudioDeviceID audio_device;

// todo: Should not be needed?
// Current window dimensions, might differ from ones in global_configuration
uint16_t global_window_width;
uint16_t global_window_height;

bool global_renderer_has_blending = true;

it_configuration global_configuration;

uint32_t global_current_update_tick;
uint32_t global_current_render_tick;

static xm_context_t *tracker_context;
static struct it_buffer current_track;
static it_color clear_color; // todo: Move to configuration.

static bool is_running = true;

// todo: Do not clear if composition is the same as for the last frame. It would require creation of
//       rendering command queue.
static void render_start(void) {
    if (SDL_SetRenderDrawColor(
            renderer, clear_color.red, clear_color.green, clear_color.blue, clear_color.alpha)
        != 0)
        it_log("error specifying renderer color: %s", SDL_GetError());
    if (SDL_RenderClear(renderer) != 0)
        it_log("error clearing the render backbuffer");
}

// todo: Check that render was started.
static void render_finish(void) {
    if (it_drawing_queue_flush())
        SDL_RenderPresent(renderer);
}

static void update_action_state(it_action_state actions[static 1]) {
    for (size_t i = 0; i < it_key_count; ++i)
        if (actions->keys[i] == it_key_state_just_released)
            actions->keys[i] = it_key_state_released;

    // todo:
    actions->pointer_motion = (it_vector2_fixed) { 0 };

    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_QUIT:
            is_running = false;
            break;
        case SDL_KEYUP:
        case SDL_KEYDOWN:
            if (event.key.repeat)
                break;
            switch (event.key.keysym.scancode) {
            case SDL_SCANCODE_LEFT:
            case SDL_SCANCODE_A:
                switch (event.key.state) {
                case SDL_PRESSED:
                    actions->keys[it_key_left] = it_key_state_pressed;
                    break;
                case SDL_RELEASED:
                    actions->keys[it_key_left] = it_key_state_just_released;
                    break;
                }
                break;
            case SDL_SCANCODE_RIGHT:
            case SDL_SCANCODE_D:
                switch (event.key.state) {
                case SDL_PRESSED:
                    actions->keys[it_key_right] = it_key_state_pressed;
                    break;
                case SDL_RELEASED:
                    actions->keys[it_key_right] = it_key_state_just_released;
                    break;
                }
                break;
            case SDL_SCANCODE_UP:
            case SDL_SCANCODE_W:
                switch (event.key.state) {
                case SDL_PRESSED:
                    actions->keys[it_key_up] = it_key_state_pressed;
                    break;
                case SDL_RELEASED:
                    actions->keys[it_key_up] = it_key_state_just_released;
                    break;
                }
                break;
            case SDL_SCANCODE_DOWN:
            case SDL_SCANCODE_S:
                switch (event.key.state) {
                case SDL_PRESSED:
                    actions->keys[it_key_down] = it_key_state_pressed;
                    break;
                case SDL_RELEASED:
                    actions->keys[it_key_down] = it_key_state_just_released;
                    break;
                }
                break;
            case SDL_SCANCODE_SPACE:
                switch (event.key.state) {
                case SDL_PRESSED:
                    actions->keys[it_key_action] = it_key_state_pressed;
                    break;
                case SDL_RELEASED:
                    actions->keys[it_key_action] = it_key_state_just_released;
                    break;
                }
                break;
            default:
                (void)0;
            }
            break;
        case SDL_MOUSEMOTION:
            actions->pointer_x = it_convert(uint16_t, event.motion.x);
            actions->pointer_y = it_convert(uint16_t, event.motion.y);
            actions->pointer_motion = (it_vector2_fixed) { .x
                = it_fixed_from_whole(it_convert(int16_t, event.motion.xrel)),
                .y = it_fixed_from_whole(it_convert(int16_t, event.motion.yrel)) };
            break;
        }
    }
}

// todo: Handle time overrun.
// todo: Test SDL_Delay for minimal reliable time. SDL1 docs say to not rely on times less than
//       10ms, we might use polling instead in such cases.
// todo: Implement generically over vector of frames?
bool it_poll(it_state *state) {
    static bool is_first_poll = true;

    static uint32_t update_time;
    static uint32_t render_time;

    if (not is_running)
        return false;

    uint32_t const current_time = SDL_GetTicks();

    if (is_first_poll) {
        update_time = current_time;
        render_time = current_time;

        is_first_poll = false;
    }

    // Test for overflow of timer.
    it_ensure(current_time >= update_time);
    it_ensure(current_time >= render_time);

    uint32_t const update_rate_ms = global_configuration.update_rate_ms.value;
    uint32_t const render_rate_ms = global_configuration.render_rate_ms.value;

    uint32_t delayed_time = current_time;

    // Sleep until frame processing is required.
    if ((update_rate_ms != 0) and (render_rate_ms != 0)) {
        uint32_t const ms_till_update = (current_time - update_time) >= update_rate_ms
            ? 0
            : update_rate_ms - (current_time - update_time);

        uint32_t const ms_till_render = (current_time - render_time) >= render_rate_ms
            ? 0
            : render_rate_ms - (current_time - render_time);

        uint32_t const ms_to_wait = ms_till_update > ms_till_render ? ms_till_render
                                                                    : ms_till_update;
        if (ms_to_wait != 0)
            SDL_Delay(ms_to_wait);

        delayed_time += ms_to_wait;

        it_test(delayed_time >= current_time);
    }

    if (state->is_render_frame)
        render_finish();

    state->is_update_frame = false;
    state->is_render_frame = false;

    if ((delayed_time - update_time) > update_rate_ms) {
        update_action_state(&state->actions);
        update_time += update_rate_ms;
        state->is_update_frame = true;
        global_current_update_tick++;
    }

    if ((delayed_time - render_time) > render_rate_ms) {
        render_start();
        render_time += render_rate_ms;
        state->is_render_frame = true;
        global_current_render_tick++;
    }

    return true;
}

// todo: Window dimensions are not as relevant as actual screen size, aka drawable area.
//       It should be used and enforced instead.
static bool init_sdl(void) {
    uint16_t const desired_client_width = global_configuration.window_width.value;
    uint16_t const desired_client_height = global_configuration.window_height.value;

    window = SDL_CreateWindow(global_configuration.title.value, SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, desired_client_width, desired_client_height, 0);
    if (not window)
        return false;
    // todo: Do we need to try again for getting a software mode one?
    renderer = SDL_CreateRenderer(window, -1,
        global_configuration.is_software_mode.value ? SDL_RENDERER_SOFTWARE
                                                    : SDL_RENDERER_ACCELERATED);
    if (not renderer) {
        SDL_DestroyWindow(window);
        window = NULL;
        it_log("error creating renderer: %s", SDL_GetError());
        return false;
    }

    // Force drawable dimension to be of given size, without including DE specific margins.
    SDL_SetWindowSize(window, desired_client_width, desired_client_height);

    int client_width, client_height;
    SDL_GetWindowSize(window, &client_width, &client_height);
    if (client_width != desired_client_width or client_height != desired_client_height)
        it_log("client drawing area is not the same as required by configuration. current: (%u, "
               "%u), desired: (%u, %u)",
            client_width, client_height, desired_client_width, desired_client_height);

    it_log("using %s rendering",
        global_configuration.is_software_mode.value ? "software" : "hardware accelerated");
    if (SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND) != 0) {
        it_log("error enabling blending: %s", SDL_GetError());
        global_renderer_has_blending = false;
    }

    int width, height;
    SDL_GetWindowSize(window, &width, &height);
    // todo: Safe conversion
    global_window_width = (uint16_t)width;
    global_window_height = (uint16_t)height;

    // todo: Expose in configuration.
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");

    return true;
}

static void audio_callback(void *userdata, uint8_t *stream, int len) {
    (void)userdata;
    it_test(it_is_aligned(stream, it_alignof(float)));
    xm_generate_samples(tracker_context, it_align_cast(stream, float *),
        (size_t)len / sizeof(float) / it_tracker_channels);
}

// todo: Feed obtained frequency to tracker context.
static bool init_audio_device(void) {
    SDL_AudioSpec desired = { 0 }, obtained;
    desired.freq = it_tracker_frequency;
    desired.format = AUDIO_F32;
    desired.channels = it_tracker_channels;
    desired.samples = 4096;
    desired.callback = audio_callback;
    audio_device
        = SDL_OpenAudioDevice(NULL, 0, &desired, &obtained, SDL_AUDIO_ALLOW_SAMPLES_CHANGE);
    if (audio_device == 0)
        return false;
    return true;
}

// todo: We might want to delay the actual loading to later?
// todo: Preserve current audio if something fails to load a new one.
bool it_play_track(char const *filepath) {
    SDL_PauseAudioDevice(audio_device, 1);

    if (tracker_context) {
        xm_free_context(tracker_context);
        tracker_context = NULL;
    } else
        current_track = it_buffer_init(it_typeinfo_init(uint8_t));

    if (not it_buffer_from_file(&current_track, filepath))
        return false;

    int status = xm_create_context_safe(&tracker_context, (char const *)current_track.private.data,
                     current_track.private.length * current_track.private.typeinfo.size,
                     it_tracker_frequency)
        == 0;
    if (not status)
        return false;

    SDL_PauseAudioDevice(audio_device, 0);

    return true;
}

static bool flag_present(
    it_managed_buffer command_line_arguments[const static 1], it_string argument) {
    for (it_buffer_iterator it
         = it_buffer_iterator_init(&command_line_arguments->buffer, it_flag_access_readable);
         it_buffer_iterator_pump(&it);) {
        if (it_string_check_equality(it_align_cast(it.current, it_string const *), &argument)) {
            it_string_free(&argument);
            return true;
        }
    }
    it_string_free(&argument);
    return false;
}

// todo: Logging interface should handle that.
static void log_format_memory(char format[const static 1], size_t bytes) {
    if (bytes >= 1048576)
        it_log(format, bytes / 1048576, "MiB");
    else if (bytes >= 1024)
        it_log(format, bytes / 1024, "KiB");
    else
        it_log(format, bytes, "bytes");
}

static void dump_memory_stats(void) {
    it_memory_stats stats = it_memory_get_stats();

    it_log("memory stats:");
    it_log("=============");
    log_format_memory("memory in use: %zu %s", stats.bytes_used);
    log_format_memory("max memory used: %zu %s", stats.max_bytes_used);
    it_log("number of allocations: %zu", stats.n_allocations);
    it_log("number of reallocations: %zu", stats.n_reallocations);
    it_log("number of frees: %zu", stats.n_frees);
}

it_configuration it_configuration_from_arguments(it_managed_buffer arguments) {
    it_configuration result = { 0 };

    bool is_software_mode
        = flag_present(&arguments, it_string_from_literal("--force-software-renderer"))
        or flag_present(&arguments, it_string_from_literal("-fsr"));

    bool direct_draw = flag_present(&arguments, it_string_from_literal("--direct-draw"))
        or flag_present(&arguments, it_string_from_literal("-dd"));

    if (is_software_mode)
        it_option_set(&result.is_software_mode, true);

    if (direct_draw) {
        it_log("using direct drawing");
        it_option_set(&result.use_drawing_queue, false);
    }

    it_managed_buffer_free(&arguments);

    return result;
}

static bool populate_configuration(it_configuration configuration[static 1]) {
    it_option_set_unspecified(&configuration->title, "ilotawa");
    it_option_set_unspecified(&configuration->is_software_mode, false);
    it_option_set_unspecified(&configuration->render_rate_ms, 20);
    it_option_set_unspecified(&configuration->update_rate_ms, 10);
    it_option_set_unspecified(&configuration->window_width, 640);
    it_option_set_unspecified(&configuration->window_height, 480);
    it_option_set_unspecified(&configuration->use_drawing_queue, true);

    global_configuration = *configuration;

    return true;
}

// todo: Register cleanup signal.
bool it_initialize(it_configuration configuration) {
    if (not populate_configuration(&configuration))
        return false;

    SDL_SetMainReady();

    if (SDL_Init(SDL_INIT_VIDEO bitor SDL_INIT_AUDIO) != 0) {
        it_log("error initializing video and audio submodules: %s", SDL_GetError());
        return false;
    }

    if (not init_sdl())
        goto error;

    if (not init_audio_device())
        goto error;

    it_module_prepare();

    if (not it_module_register(it_drawing_queue_get_module_descriptor()))
        goto error;
    if (not it_module_register(it_texture_get_module_descriptor()))
        goto error;
    if (not it_module_register(it_tilemap_get_module_descriptor()))
        goto error;

    if (not it_module_initialize_all())
        goto error;

    return true;

error:
    it_deinitialize();

    return false;
}

void it_deinitialize(void) {
    it_module_deinitialize_all();

    if (audio_device != 0) {
        SDL_CloseAudioDevice(audio_device);
        audio_device = 0;
    }

    if (tracker_context) {
        xm_free_context(tracker_context);
        tracker_context = NULL;
    }

    if (current_track.private.data)
        it_buffer_free(&current_track);

    if (renderer) {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }

    if (window) {
        SDL_DestroyWindow(window);
        window = NULL;
    }

    dump_memory_stats();

    SDL_Quit();
}

void it_log(char const *format, ...) {
    va_list list;
    va_start(list, format);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO, format, list);
    va_end(list);
}

it_area it_get_window_area(void) {
    return (it_area) { .origin = { 0 },
        .extend = { .width = it_fixed_from_whole(it_convert(int16_t, global_window_width)),
            .height = it_fixed_from_whole(it_convert(int16_t, global_window_height)) } };
}

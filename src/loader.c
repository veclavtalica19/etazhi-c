#include <stdint.h>

#include <SDL2/SDL_rwops.h>

#include <ilotawa/src/adopted/gif_load.h>

#define IT_INTEGER_OPS_INLINED
#include <ilotawa/include/loader.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/color.h>
#include <ilotawa/include/types/image.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>

// todo: Caching of resources.
//       Problem with that is that we can't know for sure whether mutations are expected for
//       returned buffers.
//       Ideally everything should be shared, unless mutations are in place, aka COW.
//       For that to work refcounted cell type is required.

bool it_buffer_from_file(it_buffer *result, char const *filepath) {
    SDL_RWops *io = SDL_RWFromFile(filepath, "rb");
    if (io == NULL)
        return false;

    int64_t offset = SDL_RWseek(io, 0, RW_SEEK_END);
    if (offset <= 0) {
        // todo: we should handle such case by loading in chunks
        SDL_RWclose(io);
        return false;
    }

    if (SDL_RWseek(io, 0, RW_SEEK_SET) == -1) {
        SDL_RWclose(io);
        return false;
    }

    if (not it_buffer_ensure_capacity(result, (uint32_t)offset)) {
        SDL_RWclose(io);
        return false;
    }

    if (SDL_RWread(io, result->private.data, sizeof(char), (uint32_t)offset) == 0) {
        if (SDL_GetError()) { // todo: kinda whack, there should be better way
            SDL_RWclose(io);
            it_buffer_free(result);
            return false;
        }
    }

    SDL_RWclose(io);

    result->private.length = (uint32_t)offset;

    return true;
}

struct it_gifdecode_package {
    it_image *image;
    bool success;
};

static void it_gifdecode_loader_callback(void *usrdata, struct GIF_WHDR *handle) {
    struct it_gifdecode_package *package_ptr = (struct it_gifdecode_package *)usrdata;
    struct it_gifdecode_package package = *package_ptr;

    // Ignore everything except the first frame
    if (handle->nfrm != 1)
        return;

    it_image *image = package.image;

    it_ensure(handle->frxd >= 0 and handle->fryd >= 0);
    it_ensure(handle->frxd <= UINT16_MAX and handle->fryd <= UINT16_MAX);

    image->width = (uint16_t)handle->frxd;
    image->height = (uint16_t)handle->fryd;

    uint32_t size = it_mul(uint32_t, (uint16_t)handle->frxd, (uint16_t)handle->fryd);

    if (not it_buffer_ensure_capacity(&image->buffer, size)) {
        package_ptr->success = false;
        return;
    }

    image->buffer.private.length = size;

    uint8_t *needle = handle->bptr;

    for (size_t i = 0; i < size; ++i, ++needle) {
        uint8_t palette = *needle;
        it_image_set_pixel(image, it_convert(uint32_t, i),
            (it_color) { .red = handle->cpal[palette].R,
                .green = handle->cpal[palette].G,
                .blue = handle->cpal[palette].B,
                .alpha = palette == handle->tran ? 0 : 255 });
    }

    package_ptr->success = true;
}

bool it_image_from_buffer(it_image *result, it_buffer buffer) {
    struct it_gifdecode_package package = { .image = result };
    long status = GIF_Load(buffer.private.data,
        (long)it_mul(uint32_t, buffer.private.length, buffer.private.typeinfo.size),
        it_gifdecode_loader_callback, NULL, &package, 0L);

    if (status <= 0 or !package.success) {
        it_image_free(result);
        it_buffer_free(&buffer);
        return false;
    }

    it_buffer_free(&buffer);

    return true;
}

bool it_texture_from_file(it_texture *result, char const *filepath, bool with_transparency) {
    it_test(result and filepath);

    // todo: This is rather useless.
    enum it_color_format format = global_configuration.is_software_mode.value
        ? it_color_format_rgba8
        : it_color_format_bgra8;

    it_buffer buffer = it_buffer_init(it_color_format_typeinfo(format));
    if (not it_buffer_from_file(&buffer, filepath))
        return false;

    it_image image = it_image_init(format);
    if (not it_image_from_buffer(&image, buffer)) {
        it_buffer_free(&buffer);
        return false;
    }

    if (not it_texture_from_image(result, image, with_transparency)) {
        it_image_free(&image);
        return false;
    }

    return true;
}

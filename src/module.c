#include <stdbool.h>
#include <stdint.h>

#include <ilotawa/include/private/module.h>
#include <tomolipu/include/types/buffer.h>

static it_buffer module_descriptor_buffer;

void it_module_prepare(void) {
    module_descriptor_buffer = it_buffer_init(it_typeinfo_init(it_module_descriptor));
}

bool it_module_register(it_module_descriptor module) {
    return it_buffer_push(&module_descriptor_buffer, &module);
}

bool it_module_initialize_all(void) {
    uint32_t was_initialized = 0;

    for (it_buffer_iterator it
         = it_buffer_iterator_init(&module_descriptor_buffer, it_flag_access_readable);
         it_buffer_iterator_pump(&it);) {
        it_module_descriptor const *const descriptor = it.current;
        if (descriptor->initializer) {
            if (not descriptor->initializer())
                break;
        }
        was_initialized++;
    }

    // Deinitialize what was initialized if something went wrong.
    if (was_initialized != module_descriptor_buffer.private.length) {
        for (it_buffer_iterator it
             = it_buffer_iterator_init(&module_descriptor_buffer, it_flag_access_readable);
             was_initialized != 0 and it_buffer_iterator_pump(&it);) {
            it_module_descriptor const *const descriptor = it.current;
            if (descriptor->deinitializer)
                descriptor->deinitializer();
            was_initialized--;
        }

        return false;
    }

    return true;
}

void it_module_deinitialize_all(void) {
    for (it_buffer_iterator it
         = it_buffer_iterator_init(&module_descriptor_buffer, it_flag_access_readable);
         it_buffer_iterator_pump(&it);) {
        it_module_descriptor const *const descriptor = it.current;
        if (descriptor->deinitializer)
            descriptor->deinitializer();
    }

    it_buffer_free(&module_descriptor_buffer);
}

void it_module_clean_all(void) {
    for (it_buffer_iterator it
         = it_buffer_iterator_init(&module_descriptor_buffer, it_flag_access_readable);
         it_buffer_iterator_pump(&it);) {
        it_module_descriptor const *const descriptor = it.current;
        if (descriptor->cleaner)
            descriptor->cleaner();
    }
}

#define IT_INTEGER_OPS_INLINED
#define IT_VECTOR2_INLINED
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/atlas.h>
#include <ilotawa/include/types/image.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/vector2.h>

bool it_atlas_from_texture(
    it_atlas *result, it_texture texture, uint16_t tile_width, uint16_t tile_height) {
    it_test(result and texture.private.id.value != 0);
    it_test(tile_width != 0 and tile_height != 0);

    *result = (it_atlas) {
        .tile_width = tile_width, .tile_height = tile_height, .private = { .texture = texture }
    };

    return true;
}

// todo: Check whether atlas coords are inbound
void it_atlas_draw(it_atlas const *atlas,
    it_vector2_fixed position,
    uint16_t tile_x,
    uint16_t tile_y,
    it_texture_draw_options const *options) {
    it_test(atlas != NULL);
    it_test(tile_x < it_div(uint16_t, atlas->private.texture.width, atlas->tile_width));
    it_test(tile_y < it_div(uint16_t, atlas->private.texture.height, atlas->tile_height));

    it_texture_draw(&atlas->private.texture, position, it_mul(uint16_t, atlas->tile_width, tile_x),
        it_mul(uint16_t, atlas->tile_height, tile_y), atlas->tile_width, atlas->tile_height,
        options);
}

void it_atlas_free(it_atlas *atlas) {
    it_test(atlas != NULL);
    it_texture_free(&atlas->private.texture);
    atlas->tile_width = 0;
    atlas->tile_height = 0;
}


#include <ilotawa/include/ilotawa.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/color.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/testing.h>

void it_color_set(void *result, enum it_color_format format, it_color color) {
    it_test(result != NULL);

    switch (format) {
    case it_color_format_rgba8:
        *(it_color_rgba8 *)result = (it_color_rgba8) {
            .red = color.red, .green = color.green, .blue = color.blue, .alpha = color.alpha
        };
        break;
    case it_color_format_rgb8:
        if (color.alpha != 255) {
            *(it_color_rgb8 *)result = (it_color_rgb8) { .red = it_color_transparent_red,
                .green = it_color_transparent_green,
                .blue = it_color_transparent_blue };
        } else
            *(it_color_rgb8 *)result
                = (it_color_rgb8) { .red = color.red, .green = color.green, .blue = color.blue };
        break;
    case it_color_format_bgra8:
        *(it_color_bgra8 *)result = (it_color_bgra8) {
            .red = color.red, .green = color.green, .blue = color.blue, .alpha = color.alpha
        };
        break;
    default:
        it_ensure(false);
        it_unreachable();
    }
}

static size_t it_color_format_pixel_size(enum it_color_format format) {
    switch (format) {
    case it_color_format_rgb8:
        return sizeof(uint8_t) * 3;
    case it_color_format_rgba8:
    case it_color_format_bgra8:
        return sizeof(uint8_t) * 4;
    default:
        it_ensure(false);
        it_unreachable();
    }

    return 0;
}

size_t it_color_format_pixel_depth(enum it_color_format format) {
    switch (format) {
    case it_color_format_rgb8:
        return 3;
    case it_color_format_rgba8:
    case it_color_format_bgra8:
        return 4;
    default:
        it_ensure(false);
        it_unreachable();
    }

    return 0;
}

it_typeinfo it_color_format_typeinfo(enum it_color_format format) {
    switch (format) {
    case it_color_format_rgb8:
    case it_color_format_rgba8:
    case it_color_format_bgra8:
        return (it_typeinfo) { .size = (uint16_t)it_color_format_pixel_size(format),
            .alignment = alignof(uint8_t) };
    default:
        it_ensure(false);
        it_unreachable();
    }

    return (it_typeinfo) { 0 };
}

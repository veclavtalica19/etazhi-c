#include <iso646.h>

#include <ilotawa/include/types/direction.h>
#include <tomolipu/include/types/fixed.h>

// 46341 ~= it_fixed_denominator / fixedal part of root of 2
//
static const it_vector2_fixed unit_vectors[it_direction_count] = {
    // None
    { 0 },
    // Right
    { .x = it_fixed_const_init(it_sign_plus, 1, 0) },
    // Bottom-Right
    { .x = it_fixed_const_init(it_sign_plus, 0, 46341),
        .y = it_fixed_const_init(it_sign_plus, 0, 46341) },
    // Bottom
    { .y = it_fixed_const_init(it_sign_plus, 1, 0) },
    // Bottom-Left
    { .x = it_fixed_const_init(it_sign_minus, 0, 46341),
        .y = it_fixed_const_init(it_sign_plus, 0, 46341) },
    // Left
    { .x = it_fixed_const_init(it_sign_minus, 1, 0) },
    // Left-Up
    { .x = it_fixed_const_init(it_sign_minus, 0, 46341),
        .y = it_fixed_const_init(it_sign_minus, 0, 46341) },
    // Up
    { .y = it_fixed_const_init(it_sign_minus, 1, 0) },
    // Right-Up
    { .x = it_fixed_const_init(it_sign_plus, 0, 46341),
        .y = it_fixed_const_init(it_sign_minus, 0, 46341) }
};

enum it_direction it_direction_from_action_state(it_action_state const *const state) {
    it_test(state != NULL);

    if (state->keys[it_key_right] and not state->keys[it_key_left]) {
        if (state->keys[it_key_down] and not state->keys[it_key_up])
            return it_direction_right_down;
        else if (state->keys[it_key_up] and not state->keys[it_key_down])
            return it_direction_right_up;
        else
            return it_direction_right;
    }

    else if (state->keys[it_key_left] and not state->keys[it_key_right]) {
        if (state->keys[it_key_down] and not state->keys[it_key_up])
            return it_direction_left_down;
        else if (state->keys[it_key_up] and not state->keys[it_key_down])
            return it_direction_left_up;
        else
            return it_direction_left;
    }

    else if (state->keys[it_key_up] and not state->keys[it_key_down])
        return it_direction_up;

    else if (state->keys[it_key_down] and not state->keys[it_key_up])
        return it_direction_down;

    return it_direction_none;
}

it_vector2_fixed it_direction_to_vector(enum it_direction const direction) {
    return unit_vectors[direction];
}

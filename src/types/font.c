#include <stdint.h>

#define IT_INTEGER_OPS_INLINED
#define IT_FIXED_INLINED
#include <ilotawa/include/ilotawa.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/atlas.h>
#include <ilotawa/include/types/font.h>
#include <ilotawa/include/types/texture.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/string.h>

bool it_font_from_bitmap(
    it_font *result, it_texture texture, uint16_t tile_width, uint16_t tile_height) {

    it_atlas atlas;
    if (not it_atlas_from_texture(&atlas, texture, tile_width, tile_height)) {
        it_texture_free(&texture);
        return false;
    }

    *result = (it_font) {
        .kind = it_font_kind_bitmap,
        .bitmap = { atlas },
    };

    return true;
}

// todo: Implement limit handling.
void it_font_draw(it_font const *font,
    it_vector2_fixed position,
    it_string const *string,
    it_font_draw_options const *options) {
    it_test(font != NULL and string != NULL);

    const it_texture_draw_options draw_options = {
        .color = it_option_move(options->color),
        .scaling = it_option_move(options->scaling),
    };

    if (font->kind == it_font_kind_bitmap) {
        // todo: Use safe modulo.
        it_test(font->bitmap.atlas.private.texture.width % font->bitmap.atlas.tile_width == 0);

        uint16_t const tiles_per_horizontal_line = it_div(
            uint16_t, font->bitmap.atlas.private.texture.width, font->bitmap.atlas.tile_width);

        uint16_t const tiles_per_vertical_line = it_div(
            uint16_t, font->bitmap.atlas.private.texture.height, font->bitmap.atlas.tile_height);

        it_fixed const unit_x_diff
            = it_fixed_mul(it_fixed_const_init(it_sign_plus, font->bitmap.atlas.tile_width, 0),
                it_option_value_or(draw_options.scaling, it_fixed_const_init(it_sign_plus, 1, 0)));

        for (it_string_iterator it = it_string_iterator_init(string);
             it_string_iterator_pump(&it);) {
            // todo: Handle more control characters.
            if (it.current < 32)
                continue;

            uint16_t const tile_x = (it.current - 32) % tiles_per_horizontal_line;
            uint16_t const tile_y = it_convert(
                uint16_t, it_div(uint32_t, it.current - 32, tiles_per_horizontal_line));

            if (tile_y > tiles_per_vertical_line)
                continue;

            it_atlas_draw(&font->bitmap.atlas, position, tile_x, tile_y, &draw_options);

            position.x = it_fixed_add(position.x, unit_x_diff);
        }
    } else
        it_ensure(false);
}

IT_API void it_font_free(it_font *font) {
    it_test(font != NULL);

    if (font->kind == it_font_kind_bitmap)
        it_atlas_free(&font->bitmap.atlas);
    else
        it_ensure(false);
}

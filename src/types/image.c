#include <tomolipu/include/compiler.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/testing.h>

#include <ilotawa/include/ilotawa.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/color.h>
#include <ilotawa/include/types/image.h>

it_image it_image_init(enum it_color_format format) {
    return (
        it_image) { .buffer = it_buffer_init(it_color_format_typeinfo(format)), .format = format };
}

void it_image_free(it_image *image) {
    it_test(image != NULL);
    it_buffer_free(&image->buffer);
    image->width = 0;
    image->height = 0;
    image->format = it_color_format_undefined;
}

void it_image_set_pixel(it_image *image, uint32_t index, it_color color) {
    it_test(image != NULL);
    it_color_set(it_buffer_get(&image->buffer, index), image->format, color);
}

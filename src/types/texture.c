#include <stdbool.h>
#include <stdint.h>

#include <SDL2/SDL_blendmode.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_render.h>

#include <ilotawa/include/private/drawing_queue.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/area.h>
#include <ilotawa/include/types/private/texture.h>
#include <ilotawa/include/types/texture.h>

#define IT_INTEGER_OPS_INLINED
#define IT_FIXED_INLINED
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/interfaces/option.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/fixed.h>
#include <tomolipu/include/types/persistent_vector.h>

it_static_assert(it_origin_count < UINT8_MAX);

typedef struct it_texture_implementation {
    SDL_Texture *texture;
    bool with_transparency;
} it_texture_implementation;

static it_persistent_vector texture_implementation_vector;

static it_drawing_queue_command_id draw_texture_command_id;

bool it_texture_from_image(it_texture *result, it_image image, bool with_transparency) {
    it_test(result != NULL);
    it_test(result->private.id.value == 0);
    it_test(image.width != 0 and image.height != 0);
    it_test(image.buffer.private.flags bitand it_flag_access_readable);

    uint32_t id;
    if (not it_persistent_vector_add(&texture_implementation_vector, &id))
        return false;

    it_texture_implementation *implementation
        = it_persistent_vector_get(&texture_implementation_vector, id);

    const uint32_t sdl_format = global_configuration.is_software_mode.value
        ? SDL_PIXELFORMAT_RGBA32
        : SDL_PIXELFORMAT_BGRA32;

    SDL_Texture *texture = SDL_CreateTexture(
        renderer, sdl_format, SDL_TEXTUREACCESS_STATIC, image.width, image.height);

    if (not texture) {
        it_ensure(it_persistent_vector_delete(&texture_implementation_vector, id));
        it_log("error creating texture, SDL error: %s", SDL_GetError());
        return false;
    }

    // todo: This requires data in image and texture to be in the same format, which might be not
    //       the case.
    //
    if (SDL_UpdateTexture(texture, NULL, image.buffer.private.data,
            it_mul(int, image.width, it_color_format_typeinfo(image.format).size))
        != 0) {
        it_ensure(it_persistent_vector_delete(&texture_implementation_vector, id));
        SDL_DestroyTexture(texture);
        it_log("error uploading texture, SDL error: %s", SDL_GetError());
        return false;
    }

    int width, height;
    if (SDL_QueryTexture(texture, NULL, NULL, &width, &height) != 0) {
        it_ensure(it_persistent_vector_delete(&texture_implementation_vector, id));
        SDL_DestroyTexture(texture);
        it_log("error getting texture dimensions for texture, SDL error: %s", SDL_GetError());
        return false;
    }

    if (SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND) != 0)
        it_log("error setting blend mode for texture, expect graphical artifacts. "
               "SDL error: %s",
            SDL_GetError());

    it_image_free(&image);

    *implementation = (it_texture_implementation) { .texture = texture,
        .with_transparency = with_transparency };

    *result = (it_texture) { .private.id.value = id,
        .width = it_convert(uint16_t, width),
        .height = it_convert(uint16_t, height) };

    return true;
}

void it_texture_draw_callback(void const *untyped_command) {
    it_renderer_command_draw_texture const *const command = untyped_command;

    it_texture_implementation *implementation
        = it_persistent_vector_get(&texture_implementation_vector, command->texture_id.value);

    // todo: Cache last texture color and alpha state and only change it when needed?
    if (SDL_SetTextureColorMod(
            implementation->texture, command->color.red, command->color.green, command->color.blue)
        != 0)
        it_log("error specifying texture color modulation: %s", SDL_GetError());

    if (SDL_SetTextureAlphaMod(implementation->texture, command->color.alpha) != 0)
        it_log("error specifying texture alpha modulation: %s", SDL_GetError());

    SDL_Rect const source = {
        .x = it_convert(int, command->texture_x),
        .y = it_convert(int, command->texture_y),
        .w = it_convert(int, command->texture_width),
        .h = it_convert(int, command->texture_height),
    };

    SDL_Rect destination;

    switch (command->origin) {
    case it_origin_center:
        destination = (SDL_Rect) {
            .x = it_convert(int, it_sub(int32_t, command->position_x, command->texture_width / 2)),
            .y = it_convert(int, it_sub(int32_t, command->position_y, command->texture_height / 2)),
            .w = it_convert(int,
                it_fixed_get_whole(
                    it_fixed_mul(it_fixed_const_init(it_sign_plus, command->texture_width, 0),
                        command->scaling))),
            .h = it_convert(int,
                it_fixed_get_whole(
                    it_fixed_mul(it_fixed_const_init(it_sign_plus, command->texture_height, 0),
                        command->scaling))),
        };
        break;
    case it_origin_upperleft:
        destination = (SDL_Rect) {
            .x = it_convert(int, command->position_x),
            .y = it_convert(int, command->position_y),
            .w = it_convert(int,
                it_fixed_get_whole(
                    it_fixed_mul(it_fixed_const_init(it_sign_plus, command->texture_width, 0),
                        command->scaling))),
            .h = it_convert(int,
                it_fixed_get_whole(
                    it_fixed_mul(it_fixed_const_init(it_sign_plus, command->texture_height, 0),
                        command->scaling))),
        };
        break;
    default:
        it_test(false);
        it_unreachable();
    }

    if (it_fixed_is_zero(command->rotation)) {
        if (SDL_RenderCopy(renderer, implementation->texture, &source, &destination) != 0)
            it_log("error rendering texture: %s", SDL_GetError());
    } else if (SDL_RenderCopyEx(renderer, implementation->texture, &source, &destination,
                   it_fixed_to_double(command->rotation) * 360.0, NULL, SDL_FLIP_NONE)
        != 0)
        it_log("error rendering texture: %s", SDL_GetError());
}

void it_texture_draw(it_texture const *texture,
    it_vector2_fixed position,
    uint16_t origin_x,
    uint16_t origin_y,
    uint16_t extend_width,
    uint16_t extend_height,
    it_texture_draw_options const *options) {
    it_test(texture != NULL);

    if (not options)
        options = &(it_texture_draw_options) { 0 };

    it_fixed const rotation
        = it_option_value_or(options->rotation, it_fixed_init(it_sign_plus, 0, 0));
    it_fixed const scaling
        = it_option_value_or(options->scaling, it_fixed_init(it_sign_plus, 1, 0));
    it_color const color = it_option_value_or(options->color, ((it_color) { 255, 255, 255, 255 }));
    enum it_origin origin = it_option_value_or(options->origin, it_origin_center);

    it_ensure(it_drawing_queue_push_command(draw_texture_command_id,
        &(it_renderer_command_draw_texture) { .color = color,
            .position_x = it_fixed_get_whole(position.x),
            .position_y = it_fixed_get_whole(position.y),
            .texture_id = texture->private.id,
            .texture_x = origin_x,
            .texture_y = origin_y,
            .texture_width = extend_width,
            .texture_height = extend_height,
            .scaling = scaling,
            .rotation = rotation,
            .origin = it_convert(uint8_t, origin) }));
}

static void it_texture_implementation_free(it_texture_implementation implementation[static 1]) {
    SDL_DestroyTexture(implementation->texture);
    implementation->texture = NULL;
}

// todo: Don't actually free texture resource immediately, it could be reused for new texture.
void it_texture_free(it_texture *texture) {
    it_test(texture != NULL);

    it_drawing_queue_invalidate();
    it_texture_implementation_free(
        it_persistent_vector_get(&texture_implementation_vector, texture->private.id.value));
    it_ensure(
        it_persistent_vector_delete(&texture_implementation_vector, texture->private.id.value));
    texture->width = 0;
    texture->height = 0;
    texture->private.id.value = 0;
}

bool it_texture_module_initialize(void) {
    texture_implementation_vector
        = it_persistent_vector_init(it_typeinfo_init(it_texture_implementation));

    draw_texture_command_id = it_drawing_queue_register_command(
        it_typeinfo_init(it_renderer_command_draw_texture), &it_texture_draw_callback);

    return true;
}

void it_texture_module_deinitialize(void) {
    for (it_persistent_vector_iterator it = it_persistent_vector_iterator_init(
             &texture_implementation_vector, it_flag_access_readable bitor it_flag_access_writable);
         it_persistent_vector_iterator_pump(&it);) {
        it_texture_implementation_free(it.current);
    }

    it_persistent_vector_free(&texture_implementation_vector);
}

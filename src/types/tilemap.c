#include <stdint.h>

#include <ilotawa/include/ilotawa.h>
#include <ilotawa/include/private/drawing_queue.h>
#include <ilotawa/include/private/ilotawa.h>
#include <ilotawa/include/types/private/texture.h>
#include <ilotawa/include/types/private/tilemap.h>
#include <ilotawa/include/types/tilemap.h>

#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/persistent_vector.h>

// todo: Refcount and ability to copy.

// This way of handling things has two benefits:
//  - Back reference to all resource is stored in ilotawa and module memory reduction can be
//    performed.
//  - Freeing on deinitialization is trivial and user dont have to manage it.
//
typedef struct it_tilemap_resource {
    it_map map;
    uint32_t last_modification_tick;
    it_vector2_uint16_t dimensions;
} it_tilemap_resource;

typedef struct it_renderer_command_draw_tilemap {
    uint32_t id;
    it_texture_id texture_id;
    uint32_t last_modification_tick;
    it_vector2_int32_t position;
    it_vector2_uint16_t origin;
    it_vector2_uint16_t extend;
    it_vector2_uint16_t tile_dimensions;
} it_renderer_command_draw_tilemap;

static it_persistent_vector tilemap_resource_vector;

static it_drawing_queue_command_id draw_tilemap_command_id;

IT_API it_vector2_uint16_t it_tilemap_get_dimensions(it_tilemap const *const tilemap) {
    it_test(tilemap != NULL);
    it_test(tilemap->private.id != 0);

    it_tilemap_resource *const resource
        = it_persistent_vector_get(&tilemap_resource_vector, tilemap->private.id);

    return it_map_get_dimensions(&resource->map);
}

IT_API bool it_tilemap_resize(it_tilemap *const result, it_vector2_uint16_t dimensions) {
    it_test(result != NULL);

    if (result->private.id == 0) {
        if (not it_persistent_vector_add(&tilemap_resource_vector, &result->private.id))
            return false;

        it_tilemap_resource *const resource
            = it_persistent_vector_get(&tilemap_resource_vector, result->private.id);

        resource->map = it_map_init(it_typeinfo_init(it_tilemap_cell));
    }

    it_tilemap_resource *const resource
        = it_persistent_vector_get(&tilemap_resource_vector, result->private.id);

    resource->last_modification_tick = global_current_render_tick;

    return it_map_resize(&resource->map, dimensions);
}

IT_API void it_tilemap_set(
    it_tilemap *const tilemap, it_vector2_uint16_t coordinate, it_tilemap_cell cell) {
    it_test(tilemap != NULL);
    it_test(tilemap->private.id != 0);

    it_tilemap_resource *const resource
        = it_persistent_vector_get(&tilemap_resource_vector, tilemap->private.id);

    resource->last_modification_tick = global_current_render_tick;

    it_map_set(&resource->map, coordinate, &cell);
}

IT_API it_tilemap_cell it_tilemap_get(
    it_tilemap const *const tilemap, it_vector2_uint16_t coordinate) {
    it_test(tilemap != NULL);
    it_test(tilemap->private.id != 0);

    it_tilemap_resource const *const resource
        = it_persistent_vector_get(&tilemap_resource_vector, tilemap->private.id);

    return *(it_tilemap_cell const *)it_map_get(&resource->map, coordinate);
}

void it_tilemap_draw(it_tilemap const *tilemap, it_atlas const *atlas, it_vector2_fixed position) {
    it_test(tilemap and atlas);
    it_test(tilemap->private.id != 0);

    it_tilemap_resource const *const resource
        = it_persistent_vector_get(&tilemap_resource_vector, tilemap->private.id);

    int32_t const x = it_fixed_get_whole(position.x);
    int32_t const y = it_fixed_get_whole(position.y);

    it_vector2_uint16_t tilemap_dims = it_tilemap_get_dimensions(tilemap);

    uint16_t const starting_tile_x = it_convert(uint16_t,
        it_clamp(int32_t, it_div(int32_t, -x, atlas->tile_width), 0, it_dec(tilemap_dims.width)));
    uint16_t const starting_tile_y = it_convert(uint16_t,
        it_clamp(int32_t, it_div(int32_t, -y, atlas->tile_height), 0, it_dec(tilemap_dims.height)));

    uint16_t const tiles_per_horizontal_line
        = it_inc(it_div(uint16_t, global_window_width, atlas->tile_width));
    uint16_t const tiles_per_vertical_line
        = it_inc(it_div(uint16_t, global_window_height, atlas->tile_height));

    it_ensure(it_drawing_queue_push_command(draw_tilemap_command_id,
        &(it_renderer_command_draw_tilemap) { .id = tilemap->private.id,
            .texture_id = atlas->private.texture.private.id,
            .last_modification_tick = resource->last_modification_tick,
            .position = (it_vector2_int32_t) { .x = x, .y = y },
            .origin = (it_vector2_uint16_t) { .x = starting_tile_x, .y = starting_tile_y },
            .extend = (it_vector2_uint16_t) { .x
                = it_clamp(uint16_t, tiles_per_horizontal_line, 0, tilemap_dims.width),
                .y = it_clamp(uint16_t, tiles_per_vertical_line, 0, tilemap_dims.height) },
            .tile_dimensions = (it_vector2_uint16_t) {
                .width = atlas->tile_width, .height = atlas->tile_height } }));
}

static void it_tilemap_draw_callback(void const *untyped_command) {
    it_renderer_command_draw_tilemap const *command = untyped_command;

    it_tilemap_resource const *const resource
        = it_persistent_vector_get(&tilemap_resource_vector, command->id);

    it_map_iterator it
        = it_map_iterator_init_with_subarea(&resource->map, command->origin, command->extend);

    while (it_map_iterator_pump(&it)) {
        it_tilemap_cell const *const cell = it_map_get(&resource->map, it.current);

        it_texture_draw_callback(&(it_renderer_command_draw_texture) {
            .position_x = it_add(int32_t, command->position.x,
                it_mul(int32_t, it.current.x, command->tile_dimensions.width)),
            .position_y = it_add(int32_t, command->position.y,
                it_mul(int32_t, it.current.y, command->tile_dimensions.height)),
            .texture_id = command->texture_id,
            .texture_x = it_mul(uint16_t, command->tile_dimensions.width, cell->x),
            .texture_y = it_mul(uint16_t, command->tile_dimensions.height, cell->y),
            .texture_width = command->tile_dimensions.width,
            .texture_height = command->tile_dimensions.height,
            .scaling = it_fixed_const_init(it_sign_plus, 1, 0),
            .color = (it_color) { 255, 255, 255, 255 },
            .origin = it_origin_upperleft });
    }
}

static void it_tilemap_resource_free(it_tilemap_resource resource[static 1]) {
    it_map_free(&resource->map);
}

void it_tilemap_free(it_tilemap *const tilemap) {
    it_test(tilemap != NULL);

    if (tilemap->private.id == 0)
        return;

    it_tilemap_resource_free(
        it_persistent_vector_get(&tilemap_resource_vector, tilemap->private.id));

    it_ensure(it_persistent_vector_delete(&tilemap_resource_vector, tilemap->private.id));

    tilemap->private.id = 0;
}

it_tilemap_iterator it_tilemap_iterator_init(it_tilemap const *const tilemap) {
    it_test(tilemap != NULL);
    it_test(tilemap->private.id != 0);

    it_tilemap_resource const *const resource
        = it_persistent_vector_get(&tilemap_resource_vector, tilemap->private.id);

    return it_map_iterator_init(&resource->map);
}

IT_API it_tilemap_iterator it_tilemap_iterator_init_with_subarea(
    it_tilemap const *const tilemap, it_vector2_uint16_t origin, it_vector2_uint16_t extend) {
    it_test(tilemap != NULL);
    it_test(tilemap->private.id != 0);

    it_tilemap_resource const *const resource
        = it_persistent_vector_get(&tilemap_resource_vector, tilemap->private.id);

    return it_map_iterator_init_with_subarea(&resource->map, origin, extend);
}

bool it_tilemap_iterator_pump(it_tilemap_iterator *const it) {
    it_test(it != NULL);

    return it_map_iterator_pump(it);
}

bool it_tilemap_module_initialize(void) {
    tilemap_resource_vector = it_persistent_vector_init(it_typeinfo_init(it_tilemap_resource));

    draw_tilemap_command_id = it_drawing_queue_register_command(
        it_typeinfo_init(it_renderer_command_draw_tilemap), &it_tilemap_draw_callback);

    return true;
}

void it_tilemap_module_deinitialize(void) {
    for (it_persistent_vector_iterator it = it_persistent_vector_iterator_init(
             &tilemap_resource_vector, it_flag_access_readable bitor it_flag_access_writable);
         it_persistent_vector_iterator_pump(&it);) {
        it_tilemap_resource_free(it.current);
    }

    it_persistent_vector_free(&tilemap_resource_vector);
}

void it_tilemap_module_clean(void) {
    for (it_persistent_vector_iterator it = it_persistent_vector_iterator_init(
             &tilemap_resource_vector, it_flag_access_readable bitor it_flag_access_writable);
         it_persistent_vector_iterator_pump(&it);) {
        it_map_shrink(&((it_tilemap_resource *)it.current)->map);
    }
}

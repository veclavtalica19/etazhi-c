#include <stddef.h>
#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/managed_buffer.h>
#include <tomolipu/include/types/varying_buffer.h>

#include "utest.h"

UTEST(varying_buffer, static_array_iteration) {
    enum { kind_uint8_t, kind_uint16_t };

    unsigned char buffer_data[6];
    it_buffer buffer = it_buffer_from_sequence(buffer_data, 6, it_typeinfo_init(unsigned char),
        it_flag_access_writable bitor it_flag_access_readable);

    unsigned char *cursor = buffer.private.data;

    cursor[0] = kind_uint8_t;
    *(uint8_t *)&cursor[1] = 100;
    cursor[2] = kind_uint16_t;
    *it_align_cast(&cursor[4], uint16_t *) = 65000;

    it_varying_buffer varying = { .private = { .buffer = buffer } };
    it_varying_buffer_iterator it
        = it_varying_buffer_iterator_init(&varying, it_flag_access_readable);

    {
        EXPECT_TRUE(
            it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(unsigned char)));
        EXPECT_EQ(*(unsigned char *)it.current, kind_uint8_t);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(uint8_t)));
        EXPECT_EQ(*(uint8_t *)it.current, 100);

        EXPECT_TRUE(
            it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(unsigned char)));
        EXPECT_EQ(*(unsigned char *)it.current, kind_uint16_t);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(uint16_t)));
        EXPECT_EQ(*(uint16_t *)it.current, 65000);

        EXPECT_TRUE(it_varying_buffer_iterator_is_at_end(&it));
    }

    it_varying_buffer_free(&varying);
}

UTEST(varying_buffer, iteration_with_push) {
    enum { kind_uint8_t, kind_uint16_t };

    it_varying_buffer varying = it_varying_buffer_init();

    {
        it_varying_buffer_iterator it = it_varying_buffer_iterator_init(
            &varying, it_flag_access_writable bitor it_flag_access_readable);

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(unsigned char), &(unsigned char) { kind_uint8_t }));

        EXPECT_TRUE(
            it_varying_buffer_push(&varying, &it, it_typeinfo_init(uint8_t), &(uint8_t) { 100 }));

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(unsigned char), &(unsigned char) { kind_uint16_t }));

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(uint16_t), &(uint16_t) { 65000 }));
    }

    it_varying_buffer_iterator it
        = it_varying_buffer_iterator_init(&varying, it_flag_access_readable);

    {
        EXPECT_TRUE(
            it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(unsigned char)));
        EXPECT_EQ(*(unsigned char *)it.current, kind_uint8_t);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(uint8_t)));
        EXPECT_EQ(*(uint8_t *)it.current, 100);

        EXPECT_TRUE(
            it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(unsigned char)));
        EXPECT_EQ(*(unsigned char *)it.current, kind_uint16_t);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(uint16_t)));
        EXPECT_EQ(*(uint16_t *)it.current, 65000);

        EXPECT_TRUE(it_varying_buffer_iterator_is_at_end(&it));
    }

    it_varying_buffer_free(&varying);
}

UTEST(varying_buffer, structs_of_same_alignment) {
    enum kind { kind_a, kind_b };

    struct a {
        int a, b, c, d;
    };

    struct b {
        int a, b, c, d, e, f, g;
    };

    it_varying_buffer varying = it_varying_buffer_init();

    {
        it_varying_buffer_iterator it = it_varying_buffer_iterator_init(
            &varying, it_flag_access_writable bitor it_flag_access_readable);

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(enum kind), &(enum kind) { kind_a }));

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(struct a), &(struct a) { .a = 2, .d = 5 }));

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(enum kind), &(enum kind) { kind_b }));

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(struct b), &(struct b) { .a = 1, .g = 12 }));
    }

    it_varying_buffer_iterator it
        = it_varying_buffer_iterator_init(&varying, it_flag_access_readable);

    {
        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(enum kind)));
        EXPECT_EQ(*(enum kind *)it.current, kind_a);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(struct a)));

        struct a const *a = it.current;
        EXPECT_EQ(a->a, 2);
        EXPECT_EQ(a->d, 5);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(enum kind)));
        EXPECT_EQ(*(enum kind *)it.current, kind_b);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(struct b)));

        struct b const *b = it.current;
        EXPECT_EQ(b->a, 1);
        EXPECT_EQ(b->g, 12);

        EXPECT_TRUE(it_varying_buffer_iterator_is_at_end(&it));
    }

    it_varying_buffer_free(&varying);
}

UTEST(varying_buffer, structs_of_varying_alignment) {
    enum kind { kind_a, kind_b };

    struct a {
        char a, b, c, d, x;
    };

    struct b {
        int a, b, c, d, e, f, g;
    };

    it_varying_buffer varying = it_varying_buffer_init();

    {
        it_varying_buffer_iterator it = it_varying_buffer_iterator_init(
            &varying, it_flag_access_writable bitor it_flag_access_readable);

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(enum kind), &(enum kind) { kind_a }));

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(struct a), &(struct a) { .a = 2, .d = 5 }));

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(enum kind), &(enum kind) { kind_b }));

        EXPECT_TRUE(it_varying_buffer_push(
            &varying, &it, it_typeinfo_init(struct b), &(struct b) { .a = 1, .g = 12 }));
    }

    it_varying_buffer_iterator it
        = it_varying_buffer_iterator_init(&varying, it_flag_access_readable);

    {
        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(enum kind)));
        EXPECT_EQ(*(enum kind *)it.current, kind_a);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(struct a)));

        struct a const *a = it.current;
        EXPECT_EQ(a->a, 2);
        EXPECT_EQ(a->d, 5);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(enum kind)));
        EXPECT_EQ(*(enum kind *)it.current, kind_b);

        EXPECT_TRUE(it_varying_buffer_iterator_pump(&it, &varying, it_typeinfo_init(struct b)));

        struct b const *b = it.current;
        EXPECT_EQ(b->a, 1);
        EXPECT_EQ(b->g, 12);

        EXPECT_TRUE(it_varying_buffer_iterator_is_at_end(&it));
    }

    it_varying_buffer_free(&varying);
}
